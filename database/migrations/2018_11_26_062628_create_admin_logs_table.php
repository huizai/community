                                                    <?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $prefix = \Config::get('database.connections.mysql.prefix');
        $sql = <<<SQL
        create table {$prefix}admin_logs
        (
          id int unsigned not null auto_increment
            primary key ,
          admin_uid int not null, 
          action varchar(255) not null,
          value varchar(255) not null,
          remark varchar(255) not null,
          created_at timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          deleted_at timestamp null default null
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
