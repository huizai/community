<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrowdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $prefix = \Config::get('database.connections.mysql.prefix');
        $sql = <<<SQL
        create table {$prefix}crowd
        (
          id int(10) unsigned not null auto_increment
            primary key,
          uid int unsigned not null,
          name varchar(200) not null,
          introduce varchar(255) default null comment '简介',
          headimgurl varchar(255) default null,
          classify_id int not null,
          tag_ids varchar(255) not null,
          grade int default 1 comment '等级',
          member int default 0 comment '成员数量',
          goods int default 0 comment '商品数量',
          is_free smallint default 0 comment '是否收费;1收费，0不收费',
          is_private smallint default 0 comment '是否私密',
          lat varchar(20) default null comment '经度',
          lon varchar(20) default null comment '纬度',
          created_at timestamp null default null,
          updated_at timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          deleted_at timestamp null default null
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
