<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCertTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $prefix = \Config::get('database.connections.mysql.prefix');
        $sql = <<<SQL
        create table {$prefix}user_cert
        (
          id int unsigned not null auto_increment
            primary key ,
          user_id int unsigned not null,
          real_name varchar(255) not null,
          mobile varchar(11) not null,
          id_card varchar(255) not null,
          id_card_img_left varchar(255) not null,
          id_card_img_right varchar(255) not null,
          created_at timestamp null default null,
          updated_at timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          deleted_at timestamp null default null
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
