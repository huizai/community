<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrowdClassifyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $prefix = \Config::get('database.connections.mysql.prefix');
        $sql = <<<SQL
        create table {$prefix}crowd_classify
        (
          id int(10) unsigned not null auto_increment
            primary key,
          pid int default 0,
          name varchar(20) not null ,
          sort int default 0,
          updated_at timestamp default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          deleted_at timestamp null default null
        );
SQL;
        if (!DB::statement($sql)) {
            throw new \Exception("failed on execute SQL: [{$sql}]");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new \Exception('downgrade is forbidden');
    }
}
