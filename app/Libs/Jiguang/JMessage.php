<?php
namespace App\Libs\Jiguang;

use JMessage\IM\Friend;
use JMessage\IM\Group;
use JMessage\JMessage as JGMessage;
use JMessage\IM\Resource;
use JMessage\IM\User;
use zgldh\QiniuStorage\QiniuStorage;

class JMessage
{
    private $appKey = '';
    private $appSecret = '';

    public function __construct()
    {
        $this->appKey = env('JPUSH_APP_KEY');
        $this->appSecret = env('JPUSH_APP_SECRET');
    }
    public function getMessager(){
        return new JGMessage($this->appKey, $this->appSecret);
    }

    public function getUserInfo($username){
        $jMessager = $this->getMessager();

        $jmUser = new User($jMessager);

        $response = $jmUser->show($username);

        \Log::debug($response);
    }

    public function friendAdd($user, $friends){
        $jMessager = $this->getMessager();
        $friend = new Friend($jMessager);

        $response = $friend->add($user, $friends);
        \Log::debug($response);
        if(isset($response['body']['error'])){
            if($response['body']['error']['code'] == 899070){
                return true;
            }
            \Log::debug($response);
            return false;
        }

        return true;
    }

    public function friendDel($user, $friends){
        $jMessager = $this->getMessager();
        $friend = new Friend($jMessager);

        $response = $friend->remove($user, $friends);

        if(isset($response['body']['error'])){
            \Log::debug($response);
            return false;
        }

        return true;
    }

    public function userRegister($username){
        $jMessager = $this->getMessager();

        $jmUser = new User($jMessager);

        $response = $jmUser->register($username, md5($username));

        if(isset($response['body']['error'])){
            \Log::debug($response);
            return false;
        }

        return $username;
    }

    public function userUpdate($username, array $option){
        $jMessager = $this->getMessager();

        $jmUser = new User($jMessager);
        $response = $jmUser->update($username, $option);
        if(isset($response['body'][0]['error'])){
            \Log::debug($response);
            return false;
        }

        return true;
    }

    public function reUpload($type, $path){
        $jMessager = $this->getMessager();
        $resource = new Resource($jMessager);

        $response = $resource->upload($type, $path);

        \Log::debug($response);
        if(isset($response['body']['error'])){
            \Log::debug($response);
            return false;
        }

        return $response['body']['media_id'];
    }

    public function addGroup($owner, $name, $desc, array $members = [], $avatarPath = '', $flag = 1){
        $jMessager = $this->getMessager();
        $group = new Group($jMessager);

        \Log::debug($avatarPath);
        $pathinfo = pathinfo($avatarPath);
        $extension = isset($pathinfo['extension'])?$pathinfo['extension']:'png';
        file_put_contents("/tmp/$owner.{$extension}",file_get_contents($avatarPath));
        $upload = $this->reUpload('image', "/tmp/$owner.png");
        $response = $group->create($owner, $name, $desc, $members, $upload);

        if(isset($response['body']['error'])){
            return false;
        }

        return $response['body']['gid'];
    }

    public function addGroupMember($gid, array $members){
        $jMessager = $this->getMessager();
        $group = new Group($jMessager);
        $response = $group->addMembers($gid, $members);

        if(isset($response['body']['error'])){
            \Log::debug($response);
            return false;
        }

        return true;
    }

    public function outGroupMember($gid, array $members){
        $jMessager = $this->getMessager();
        $group = new Group($jMessager);
        $response = $group->removeMembers($gid, $members);
        if(isset($response['body']['error'])){
            \Log::debug($response);
            return false;
        }

        return true;
    }

    public function addGroupNodisturb($username, array $group){
        $jMessager = $this->getMessager();

        $jmUser = new User($jMessager);
        $response = $jmUser->addGroupNodisturb($username, $group);

        \Log::debug(1111);
        \Log::debug($response);

        if(isset($response['body'][0]['error'])){
            \Log::debug($response);
            return false;
        }

        return true;
    }

    public function removeGroupNodisturb($username, array $group){
        $jMessager = $this->getMessager();

        $jmUser = new User($jMessager);
        $response = $jmUser->removeGroupNodisturb($username, $group);
        if(isset($response['body'][0]['error'])){
            \Log::debug($response);
            return false;
        }

        return true;
    }

}