<?php
namespace App\Libs\Jiguang;

use JMessage\JMessage;
use JPush\Client;

class Jpush
{
    private $appKey = '';
    private $appSecret = '';

    public function __construct()
    {
        $this->appKey = env('JPUSH_APP_KEY');
        $this->appSecret = env('JPUSH_APP_SECRET');
    }

    public function getPusher(){
        return new Client($this->appKey, $this->appSecret);
    }

    public function getMessager(){
        return new JMessage($this->appKey, $this->appSecret);
    }

    public function pushAliasToAllPlatform($alias=[], $content='', $extras=[]){
        $pusher = $this->getPusher();

        $androidNotification = [
            'title' => '',
            'extras' => $extras,
            'large_icon' => 'http://pjbhucv1v.bkt.clouddn.com/logo.png',
        ];

        $iosNotification = [
            'extras' => $extras
        ];

        $data = $pusher->setPlatform('all')
            ->addAlias($alias)
            ->androidNotification($content,$androidNotification)
            ->iosNotification($content, $iosNotification)
            ->send();

        return true;
    }

    public function pushInformation(){
        $pusher = $this->getPusher();
        $androidNotification = [
            'title' => '',
            'extras' => ['type' => 0],
            'large_icon' => 'http://pjbhucv1v.bkt.clouddn.com/logo.png',
        ];

        $iosNotification = [
            'extras' => ['type' => 0]
        ];
        $pusher->push()
            ->setPlatform('all')
            ->addAllAudience()
            ->androidNotification('有一条新的系统消息',$androidNotification)
            ->iosNotification('有一条新的系统消息', $iosNotification)
            ->send();
    }
}