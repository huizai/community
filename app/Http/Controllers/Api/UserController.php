<?php

namespace App\Http\Controllers\Api;

use App\Blocks\CrowdBlock;
use App\Blocks\TopicBlock;
use App\Blocks\UserBlock;
use App\Http\Controllers\Controller;
use App\Http\Requests\Cert;
use App\Http\Requests\Register;
use App\Http\Requests\SmsCode;
use App\Libs\Jiguang\JMessage;
use App\Libs\ResponseMessage;
use App\Models\UserCert;
use App\Models\UserFriends;
use App\Models\Users;
use App\User;
use EasyWeChat\Factory;
use http\Env\Response;
use Illuminate\Http\Request;
use zgldh\QiniuStorage\QiniuStorage;

class UserController extends Controller
{
    /**
     * @api {post} /api/0.0.1/forget/password 忘记密码
     * @apiName forgetPassword
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 忘记密码
     * @apiParam {String} mobile 手机号
     * @apiParam {String} validate_code 验证码
     * @apiParam {String} password 密码
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function forgetPassword(Request $request)
    {
        $mobile = $request->get('mobile');
        $validateCode = $request->get('validate_code');
        $password = $request->get('password');

        if ($validateCode != \Cache::get($mobile)) {
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }

        if(Users::where('mobile', $mobile)->update([
                'password' => md5($password)
            ]) === false){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }
    /**
     * @api {post} /api/0.0.1/login/from/mobile 手机号短信登录
     * @apiName mobileLogin
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 用户通过手机号和验证码进行登录
     * @apiParam {String} mobile 手机号
     * @apiParam {String} validate_code 验证码
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'id' : 1,                               //用户ID
     *          'mobile' : '18800001111',               //用户手机号
     *          'nick_name' : '张三',                    //用户昵称
     *          'headimgurl' : 'http://xxxx.png',       //用户头像
     *          'auth_token' : xxxxxxx                  //判断是否登录的凭证,
     *          'user_cert_status' : null               //是否实名；null未实名;0已提交;1通过;2未通过
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function mobileLogin(Request $request)
    {
        $mobile = $request->get('mobile');
        $validateCode = $request->get('validate_code');

        if (!$validateCode){
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }
        if ($validateCode != \Cache::get($mobile)) {
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }

        $userBlock = new UserBlock();
        $user = $userBlock->userInfoByMobile($mobile);

        //用户不存在,就添加用户信息
        if (!$user) {
            $saveData = [
                'mobile' => $mobile,
                'created_at' => date('Y-m-d H:i:s', time()),
                'jg_im_username' => 'quanji_'.$mobile
            ];

            $userBlock = new UserBlock();
            $userId = $userBlock->addUser($saveData);

            if ($userId) {
                $saveData['id'] = $userId;
                $saveData['is_new'] = 1;
                $saveData['jg_im_userpassword'] = md5($saveData['jg_im_username']);
                $sessionKey = md5($userId);
                $saveData['auth_token'] = $sessionKey;
                \Cache::put($sessionKey, $saveData, \Config::get('session.lifetime'));

                return response()->json(ResponseMessage::getInstance()->success($saveData)->response());
            } else {
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }

        if(empty($user->nick_name)) {
            $user->is_new = 1;
        }

        $user->jg_im_userpassword = md5($user->jg_im_username);

        $sessionKey = md5($user->id . md5($user->nick_name));
        \Cache::put($sessionKey, $user, \Config::get('session.lifetime'));
        $user->auth_token = $sessionKey;
        unset($user->password);
        unset($user->deleted_at);

        return response()->json(ResponseMessage::getInstance()->success($user)->response());
    }
    /**
     * @api {post} /api/0.0.1/login 账号密码登录
     * @apiName login
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 用户通过手机号和密码进行登录
     * @apiParam {String} mobile 手机号
     * @apiParam {String} password 密码
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'id' : 1,                               //用户ID
     *          'mobile' : '18800001111',               //用户手机号
     *          'nick_name' : '张三',                    //用户昵称
     *          'headimgurl' : 'http://xxxx.png',       //用户头像
     *          'auth_token' : xxxxxxx                  //判断是否登录的凭证,
     *          'user_cert_status' : null               //是否实名；null未实名;0已提交;1通过;2未通过
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function login(Request $request)
    {
        $mobile = $request->get('mobile');
        $password = $request->get('password');

        $userBlock = new UserBlock();
        $user = $userBlock->userInfoByMobile($mobile);

        //用户不存在
        if (!$user) {
            return response()->json(ResponseMessage::getInstance()->failed('NO_USER')->response());
        }

        //密码不正确
        if ($user->password != md5($password)) {
            return response()->json(ResponseMessage::getInstance()->failed('PASSWORD_ERROR')->response());
        }

        $user->jg_im_userpassword = md5($user->jg_im_username);

        $sessionKey = md5($user->id . md5($user->nick_name));
        \Cache::put($sessionKey, $user, \Config::get('session.lifetime'));
        $user->auth_token = $sessionKey;
        unset($user->password);
        unset($user->deleted_at);

        return response()->json(ResponseMessage::getInstance()->success($user)->response());

    }

    /**
     * @api {post} /api/0.0.1/login/from/wx 微信登录
     * @apiName wxLogin
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 用户微信登录
     * @apiParam {String} code app通过sdk获取的code
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'id' : 1,                               //用户ID
     *          'mobile' : '18800001111',               //用户手机号
     *          'nick_name' : '张三',                    //用户昵称
     *          'headimgurl' : 'http://xxxx.png',       //用户头像
     *          'auth_token' : xxxxxxx                  //判断是否登录的凭证,
     *          'user_cert_status' : null               //是否实名；null未实名;0已提交;1通过;2未通过
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function wxLogin(Request $request)
    {
        $wechat = Factory::officialAccount([
            'app_id' => env('CH_APP_KEY'),
            'secret' => env('CH_APP_SECRET'),
        ]);

        $wxUser = $wechat->oauth->user();
        $userBlock = new UserBlock();
        $user = $userBlock->getUserByWxOpenid($wxUser->getId());
        if (!$user) {
            $wxuser = $wxUser->getOriginal();

            $saveUserData = [
                'wechat_id' => 0,
                'subscribe_time' => isset($wxuser['subscribe_time']) ? date('Y-m-d H:i:s', $wxuser['subscribe_time']) : null,
                'openid' => $wxuser['openid'],
                'nickname' => isset($wxuser['nickname']) ? $wxuser['nickname'] : null,
                'city' => isset($wxuser['city']) ? $wxuser['city'] : null,
                'province' => isset($wxuser['province']) ? $wxuser['province'] : null,
                'country' => isset($wxuser['country']) ? $wxuser['country'] : null,
                'sex' => isset($wxuser['sex']) ? $wxuser['sex'] : null,
                'headimgurl' => isset($wxuser['headimgurl']) ? $wxuser['headimgurl'] : null,
                'subscribe' => isset($wxuser['subscribe']) ? $wxuser['subscribe'] : 0,
            ];
            $userId = $userBlock->saveUserByWeixin($saveUserData);

            if ($userId) {
                $user = $userBlock->userInfo($userId);
                $user->jg_im_userpassword = md5($user->jg_im_username);

                $sessionKey = md5($user->id . uniqid());
                \Cache::put($sessionKey, $user, \Config::get('session.lifetime'));
                $user->auth_token = $sessionKey;

                return response()->json(ResponseMessage::getInstance()->success($user)->response());
            } else {
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        } else {
            $user->jg_im_userpassword = md5($user->jg_im_username);

            $sessionKey = md5($user->id . md5($user->nick_name));
            \Cache::put($sessionKey, $user, \Config::get('session.lifetime'));
            $user->auth_token = $sessionKey;
            return response()->json(ResponseMessage::getInstance()->success($user)->response());
        }

    }

    /**
     * @api {get} /api/0.0.1/user/bind/wx 绑定微信
     * @apiName bindWx
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 绑定微信
     * @apiParam {String} code app通过sdk获取的code
     * @apiParam {String} [token] 登录标识
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function bindWx(Request $request)
    {
        global $g_uid;
        $wechat = Factory::officialAccount([
            'app_id' => env('CH_APP_KEY'),
            'secret' => env('CH_APP_SECRET'),
        ]);

        $wxUser = $wechat->oauth->user();
        $userBlock = new UserBlock();
        $user = $userBlock->getUserByWxOpenid($wxUser->getId());
        if (!$user) {
            $wxuser = $wxUser->getOriginal();

            $saveUserData = [
                'wechat_id' => 0,
                'subscribe_time' => isset($wxuser['subscribe_time']) ? date('Y-m-d H:i:s', $wxuser['subscribe_time']) : null,
                'openid' => $wxuser['openid'],
                'nickname' => isset($wxuser['nickname']) ? $wxuser['nickname'] : null,
                'city' => isset($wxuser['city']) ? $wxuser['city'] : null,
                'province' => isset($wxuser['province']) ? $wxuser['province'] : null,
                'country' => isset($wxuser['country']) ? $wxuser['country'] : null,
                'sex' => isset($wxuser['sex']) ? $wxuser['sex'] : null,
                'headimgurl' => isset($wxuser['headimgurl']) ? $wxuser['headimgurl'] : null,
                'subscribe' => isset($wxuser['subscribe']) ? $wxuser['subscribe'] : 0,
            ];
            $userId = $userBlock->bindWeixin($g_uid, $saveUserData);

            if ($userId) {
                return response()->json(ResponseMessage::getInstance()->success()->response());
            } else {
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        } else {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('WXBIND','此微信以绑定其它用户')->failed()->response());
        }

    }

    /**
     * @api {post} /api/0.0.1/user/logout 退出登录
     * @apiName userLogout
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 退出登录
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : []
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function logout(Request $request)
    {
        $token = \Cookie::get(\Config::get('session.sq_cookie'));
        if (!$token) {
            $token = $request->get('token');
        }
        \Cache::forget($token);
        return response()->json(ResponseMessage::getInstance()->success()->response());
    }

    /**
     * @api {post} /api/0.0.1/register/from/mobile 手机号注册
     * @apiName mobileRegister
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 用户通过手机号和验证码进行注册
     * @apiParam {String} mobile 手机号
     * @apiParam {String} password 密码
     * @apiParam {String} nickname 昵称
     * @apiParam {String} validate_code 验证码
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : []
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function mobileRegister(Register $request)
    {
        $mobile = $request->get('mobile');
        $password = $request->get('password');
        $nickname = $request->get('nickname');
        $validateCode = $request->get('validate_code');

        if ($validateCode != \Cache::get($mobile)) {
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }

        $password = md5($password);
        $fileNum = rand(1, 2);
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';

        $saveData = [
            'mobile' => $mobile,
            'nick_name' => $nickname,
            'password' => $password,
            'jg_im_username' => 'chaihe_' . $mobile,
            'headimgurl' => $http_type . $_SERVER['SERVER_NAME'] . "/upload/headImage/head{$fileNum}.png",
            'created_at' => date('Y-m-d H:i:s', time()),
        ];
        $userBlock = new UserBlock();
        if ($userBlock->addUser($saveData)) {
            return response()->json(ResponseMessage::getInstance()->success()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
        }
    }

    /**
     * @api {post} /api/0.0.1/validate/code/form/sms 获取验证码
     * @apiName validateCode
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 通过手机号获取验证码
     * @apiParam {String} mobile 手机号
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'validate_code': 123456     //验证码
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function validateCodeFromSms(SmsCode $request)
    {


        $mobile = $request->get('mobile');
        $code = getSalt(6, 1);

        $testMobile = [
            '18684916858',
            '18874130125',
            '18874130126',
            '18874130127',
            '18874130128',
            '18874130129',
            '18874130130',
            '18874130131',
            '18874130132',
            '18874130133',
            '18874130134',
            '18877777777',
            '18888888888',
            '18899999999',
            '15511111111',
            '15522222222',
            '15533333333',
            '15544444444',
            '15555555555',
            '15566666666',
            '15577777777',
            '15588888888',
            '15599999999',
            '18811111111',
            '18822222222',
            '18833333333',
            '18844444444',
            '18855555555',
            '18866666666',
            '18877777777',
            '18508499941',

        ];

        if (in_array($mobile, $testMobile)) {
            $code = '123456';
            \Cache::put($mobile, $code, 1);
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        \Cache::put($mobile, $code, 1);

        $response = \App\Libs\Aliyun\Sms::VerCode($mobile, $code);
        \Log::debug(json_encode($response));
        if ($response->Code === 'OK') {
            \Cache::put($mobile, $code, 1);
            \Log::debug('validateCodeFromSms：'.$code);
            return response()->json(ResponseMessage::getInstance()->success()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * @api {get} /api/0.0.1/user/my/info 获取自己的信息
     * @apiName myInfo
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 获取自己的信息
     * @apiParam {String} token 登录状态
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'id' : 1,                               //用户ID
     *          'mobile' : '18800001111',               //用户手机号
     *          'nick_name' : '张三',                    //用户昵称
     *          'headimgurl' : 'http://xxxx.png',       //用户头像
     *          'crowd_num' : 1,                        //群数量
     *          'user_cert_status' : null               //是否实名；null未实名;0已提交;1通过;2未通过
     *          'wx_openid': 'xx'                       //用户微信openid
     *          'jg_im_username': 'xx'                  //极光IM的用户名
     *          'crowd_num': 'xx'                       //用户所创建的群数
     *          'crowd_money': [                        //用户群收益
     *              {
     *                  id: 'xx',                       //群ID
     *                  name: 'xxx',                    //群名
     *                  income: 'xx'                    //群收益
     *              }
     *              ...
     *          ]
     *          money: 'xx'                             //用户总收益
     *          monthIcom: 'xx'                         //月收益
     *          yesterdayIcom: 'xx'                     //昨日收益
     *          auth_token: 'xx'                        //用户token
     *          jg_im_userpassword: 'xx'
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function getMyInfo(Request $request)
    {
        global $g_uid;

        $userBlock = new UserBlock();
        $user = $userBlock->userInfo($g_uid);
        if ($user and empty($user->headimgurl)) {
            $user->headimgurl = env('APP_URL') . '/upload/108.png';
        }

        $lib = new JMessage();
        $lib->getUserInfo($user->jg_im_username);

        $user->auth_token = $request->get('token');
        $user->jg_im_userpassword = md5($user->jg_im_username);

        return response()->json(ResponseMessage::getInstance()->success($user)->response());
    }

    /**
     * @api {post} /api/0.0.1/user/update/info 更新用户信息
     * @apiName userUpdateInfo
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 更新用户信息
     * @apiParam {String} token 登录状态
     * @apiParam {String} headimgurl 用户头像
     * @apiParam {String} nick_name 昵称
     * @apiParam {String} password 密码
     * @apiParam {String} signature 签名
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function updateMyInfo(Request $request)
    {
        global $g_uid;

        $updateField = ['headimgurl', 'nick_name', 'password', 'signature'];
        $updateData = [];

        foreach ($updateField as $field) {
            if ($request->has($field)) {
                if($field == 'password'){
                    $updateData[$field] = md5($request->get($field));
                }else{
                    $updateData[$field] = $request->get($field);
                }
            }
        }

        $user = Users::where('id', $g_uid)->first();
        if (!$user || count($updateData) <= 0) {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        //不知道极光为什么不返回用户头像
        //所以用了极光名称命名的图片，方便前端显示
        if (isset($updateData['headimgurl'])) {
            $disk = QiniuStorage::disk('qiniu');
            $disk->put('headimgurl/' . $user->jg_im_username, file_get_contents($updateData['headimgurl']));
        }
        $res = Users::where('id', $g_uid)->update($updateData);

        if ($res) {
            $jMessage = new JMessage();
            $jgData = [];
            if (isset($updateData['nick_name'])) {
                $jgData['nickname'] = $updateData['nick_name'];
            }
            if (isset($updateData['signature'])) {
                $jgData['signature'] = $updateData['signature'];
            }
            if (isset($updateData['headimgurl'])) {
                $pathinfo = pathinfo($updateData['headimgurl']);
                $extension = isset($pathinfo['extension']) ? $pathinfo['extension'] : 'png';
                file_put_contents("/tmp/{$user->jg_im_username}.{$extension}", file_get_contents($updateData['headimgurl']));
                $jgData['avatar'] = $jMessage->reUpload('image', "/tmp/{$user->jg_im_username}.{$extension}");
            }
            $jMessage->userUpdate($user->jg_im_username, $jgData);
            return response()->json(ResponseMessage::getInstance()->success($user)->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * @api {get} /api/0.0.1/user/crowd/join/list 我加入的群
     * @apiName getMyCrowd
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 获取我加入的群
     * @apiParam {Number=1,2} [type=1] 我的群类型;1加入的群 2收藏的群
     * @apiParam {String} [search] 根据名字搜索
     * @apiParam {Number} [page=1] 分页
     * @apiParam {Number} [pageSize=20] 单页数量
     * @apiParam {String} token 登录状态
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [{
     *          'id' : 1,                        //群ID
     *          'uid' : '0',                     //群主ID
     *          'name' : '汽车',                  //群名称
     *          'introduce' : '群简介',           //群简介
     *          'headimgurl' : 'http://xxxxx',   //群列表小图,
     *          'classify_id': 2,                //群分类ID 分类的二级id
     *          'tag_ids' : '1,2,3,4,5',         //群标签ID
     *          'grade' : 12,                    //群等级,
     *          'member' : 234,                  //群成员数量,
     *          'goods' : 4343,                  //群商品数量,
     *          'is_free': 1,                    //进去是否收费 1不收费，0收费
     *          'is_private': 1,                 //是否是私密群 1是，0不是
     *          'classify_name': '分类',          //分类名称,
     *          'nick_name' : '辉仔',             //群主昵称,
     *      }...]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */

    public function myJoinCrowd(Request $request)
    {
        global $g_uid;
        $page = $request->get('page', 1);
        $pageSize = $request->get('pageSize', 20);

        $searchKey = ['type', 'search'];
        $search = [];
        foreach ($searchKey as $key) {
            if ($request->has($key)) {
                $search[$key] = $request->get($key);
            }
        }

        $crowdBlock = new CrowdBlock();

        $list = $crowdBlock->getUserCrowdList($g_uid, $search, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success($list)->response());
    }

    /**
     * @api {post} /api/0.0.1/user/collect/crowd 收藏群
     * @apiName collectCrowd
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 获取群列表
     * @apiParam {Number} [crowd_id] 群ID
     * @apiParam {String} token 登录状态
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */

    public function collectCrowd(Request $request)
    {
        global $g_uid;
        if (!$request->has('crowd_id')) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR', '缺少群ID')->response());
        }
        $crowdId = $request->get('crowd_id');

        $crowdBlock = new CrowdBlock();
        if ($crowdBlock->collectCrowd($g_uid, $crowdId)) {
            return response()->json(ResponseMessage::getInstance()->success()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * @api {post} /api/0.0.1/user/join/crowd 加入群
     * @apiName joinCrowd
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 加入群
     * @apiParam {Number} [crowd_id] 群ID
     * @apiParam {String} token 登录状态
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'charge' : 10.01 //需要支付的费用
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function joinCrowd(Request $request)
    {
        global $g_uid;
        if (!$request->has('crowd_id')) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR', '缺少群ID')->response());
        }
        $crowdId = $request->get('crowd_id');

        $crowdBlock = new CrowdBlock();

        $charge = $crowdBlock->joinCrowd($g_uid, $crowdId);
        if ($charge === false) {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->success($charge)->response());
        }
    }

    /**
     * @api {post} /api/0.0.1/user/out/crowd 退出群
     * @apiName outCrowd
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 退出群
     * @apiParam {Number} [crowd_id] 群ID
     * @apiParam {String} token 登录状态
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function outCrowd(Request $request)
    {
        global $g_uid;
        if (!$request->has('crowd_id')) {
            return response()->json(ResponseMessage::getInstance()->setFailedCode('PARAM_ERROR', '缺少群ID')->response());
        }
        $crowdId = $request->get('crowd_id');

        $crowdBlock = new CrowdBlock();

        if ($crowdBlock->outCrowd($g_uid, $crowdId) === false) {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }
    }

    /**
     * @api {get} /api/0.0.1/user/topic 获取用户动态列表
     * @apiName getUserTopic
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 获取群动态列表
     * @apiParam {Number} [page=1] 分页
     * @apiParam {Number} [pageSize=20] 单页数量
     * @apiParam {String} token 登录状态
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [{
     *                  'id' : 1                         //动态ID
     *                  'uid'      : 1,                  //用户ID
     *                  'nick_name': '张三',              //用户昵称
     *                  'content'  : 'xxxxx',            //动态内容
     *                  'useful'   : 123,                //点赞的数量
     *                  'comment'  : 123,                //评论的数量
     *                  'imgs'     : [                   //图片
     *                      {
     *                          'id' : 1,                       //图片ID
     *                          'imgurl' : 'http://xxxx.png',   //图片地址
     *                      },
     *                      ...
     *                  ]
     *          }...]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function myTopic(Request $request)
    {
        global $g_uid;
        $page = $request->get('page', 1);
        $pageSize = $request->get('pageSize', 20);

        $topicBlock = new TopicBlock();
        $topic = $topicBlock->getTopicList(['user_id' => $g_uid], $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success($topic)->response());
    }

    /**
     * @api {post} /api/0.0.1/user/cert 提交实名资料
     * @apiName userCert
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 提交实名资料
     * @apiParam {String} real_name 真实姓名
     * @apiParam {String} mobile 联系电话
     * @apiParam {String} id_card 身份证号
     * @apiParam {String} id_card_img_left 身份证正面照
     * @apiParam {String} id_card_img_right 身份证反面照
     * @apiParam {String} token 登录状态
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function userCert(Cert $request)
    {
        global $g_uid;
        $data = [
            'real_name' => $request->get('real_name'),
            'mobile' => $request->get('mobile'),
            'id_card' => $request->get('id_card'),
            'id_card_img_left' => $request->get('id_card_img_left'),
            'id_card_img_right' => $request->get('id_card_img_right'),
            'user_id' => $g_uid,
            'created_at' => date('Y-m-d H:i:s', time()),
        ];

        $userCertModel = new UserCert();
        if ($userCertModel->saveData($data)) {
            return response()->json(ResponseMessage::getInstance()->success()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * @api {get} /api/0.0.1/crowd/list 我创建的群
     * @apiName getMyCrowdList
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 我创建的群
     * @apiParam {String} [search] 根据名字搜索
     * @apiParam {Number} [page=1] 分页
     * @apiParam {Number} [pageSize=20] 单页数量
     * @apiParam {String} token 登录状态
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [{
     *          'id' : 1,                        //群ID
     *          'uid' : '0',                     //群主ID
     *          'name' : '汽车',                  //群名称
     *          'introduce' : '群简介',           //群简介
     *          'headimgurl' : 'http://xxxxx',   //群列表小图,
     *          'classify_id': 2,                //群分类ID 分类的二级id
     *          'tag_ids' : '1,2,3,4,5',         //群标签ID
     *          'grade' : 12,                    //群等级,
     *          'member' : 234,                  //群成员数量,
     *          'goods' : 4343,                  //群商品数量,
     *          'is_free': 1,                    //进去是否收费 1不收费，0收费
     *          'is_private': 1,                 //是否是私密群 1是，0不是
     *          'classify_name': '分类',          //分类名称,
     *          'nick_name' : '辉仔',             //群主昵称,
     *      }...]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */

    public function myCrowd(Request $request)
    {
        global $g_uid;
        $page = $request->get('page', 1);
        $pageSize = $request->get('pageSize', 20);

        $searchKey = ['type', 'search'];
        $search = [
            'user_id' => $g_uid,
        ];
        foreach ($searchKey as $key) {
            if ($request->has($key)) {
                $search[$key] = $request->get($key);
            }
        }

        $crowdBlock = new CrowdBlock();

        $list = $crowdBlock->getCrowdList($search, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success($list)->response());
    }

    /**
     * @api {post} /api/0.0.1/user/bind/mobile 绑定手机号
     * @apiName bindMobile
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 绑定手机号
     * @apiParam {String} mobile 手机号
     * @apiParam {String} password 手机号
     * @apiParam {String} validate_code 验证码
     * @apiParam {String} token 登录标识
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : []
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */

    public function bindMobile(Request $request)
    {
        global $g_uid;
        $mobile = $request->get('mobile');
        $password = $request->get('password');
        $validateCode = $request->get('validate_code');

        if ($validateCode != \Cache::get($mobile)) {
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }

        if (Users::where('mobile', $mobile)->first()) {
            return response()->json(ResponseMessage::getInstance()->failed('MOBILE_EXIST')->response());
        }

        $password = md5($password);

        $saveData = [
            'mobile' => $mobile,
            'password' => $password,
        ];

        if (Users::where('id', $g_uid)->update($saveData)) {
            return response()->json(ResponseMessage::getInstance()->success()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
        }
    }


    /**
     * @api {post} /api/0.0.1/user/bind/mobile/update 修改绑定的手机号
     * @apiName updateBindMobile
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 修改绑定的手机号
     * @apiParam {String} mobile 手机号
     * @apiParam {String} validate_code 验证码
     * @apiParam {String} token 登录标识
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : []
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */

    public function updateBindMobile(Request $request)
    {
        global $g_uid;
        $mobile = $request->get('mobile');
        $validateCode = $request->get('validate_code');

        if ($validateCode != \Cache::get($mobile)) {
            return response()->json(ResponseMessage::getInstance()->failed('VALIDATE_CODE_ERROR')->response());
        }

        if (Users::where('mobile', $mobile)->first()) {
            return response()->json(ResponseMessage::getInstance()->failed('MOBILE_EXIST')->response());
        }

        $saveData = [
            'mobile' => $mobile,
        ];

        if (Users::where('id', $g_uid)->update($saveData)) {
            return response()->json(ResponseMessage::getInstance()->success()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
        }
    }

    /**
     * @api {post} /api/0.0.1/user/waste 个人账单
     * @apiName userWaste
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 个人账单
     * @apiParam {String} token 登录标识
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *          "id": xx,                       //用户ID
     *           "balance": xx,                   //余额
     *           "monthIcom": "xx",             //月收入
     *           "yesterdayIcom": xx,           //昨日收入
     *           "waste": {                     //流水详情
     *               "01":[                     //流水月份
     *                  {
     *                       "id":2
     *                       "money": xx,        //金额
     *                       "create_at": xx,    //时间
     *                       "name": "xx",       //备注
     *                       "type": "xx",       //流水类型 0：商品收入 1：加群收入 2：活动收入
     *                       "month": "xx"       //月份
     *                   }
     *           ]
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function UserWaste(Request $request)
    {
        $month = (int) $request->post('month');
        global $g_uid;
        $userBlock = new UserBlock();
        $res = $userBlock->UserWaste($g_uid);
        if ($res) {
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }

    /**
     * @api {post} /api/0.0.1/user/withraw 提现
     * @apiName UserWithRraw
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 提现
     * @apiParam {Number} money 提现金额
     * @apiParam {String} token 登录标识
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : []
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function UserWithRraw(Request $request)
    {

        global $g_uid;
        if (!$request->has('money')) {
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $userBlock = new UserBlock();
        $balance = $userBlock->UserBalance($g_uid);
        $money = (int) $request->post('money');
        if ($money < 100){
            return response()
                ->json(
                    ResponseMessage::getInstance()
                        ->setFailedCode('WITHRRAW','提现金额小于100')
                        ->failed()
                        ->response()
                );
        }
        if ($balance->balance < $money) {
            return response()
                ->json(
                    ResponseMessage::getInstance()
                        ->setFailedCode('WITHRRAW','提现金额大于用户余额')
                        ->failed()
                        ->response()
                );
        }
        
        $res = $userBlock->UserWithRraw($g_uid, $money);
        if ($res) {
            return response()->json(ResponseMessage::getInstance()->success()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
        }
    }
    public function withdrawList(Request $request)
    {
        global $g_uid;
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);

        $search         = ['user_id'=>$g_uid];

        $commentList = new UserBlock();
        $list = $commentList->withdrawList($search, $page, $pageSize);
        $pagination = $commentList->withdrawListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $list, 'pagination' => $pagination])->response());
    }
    /**
     * @api {post} /api/0.0.1/user/balance 个人余额
     * @apiName UserWithBalance
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 个人余额
     * @apiParam {String} token 登录标识
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'balance': 'xx'     //账户余额
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function UserBalance()
    {
        global $g_uid;
        $userBlock = new UserBlock();
        $res = $userBlock->UserBalance($g_uid);
        if ($res) {
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * @api {get} /api/0.0.1/user/friend/req-list 好友请求列表
     * @apiName userFriendReqList
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 好友请求列表
     * @apiParam {String} token 登录标识
     * @apiParam {Number} [page=1] 分页
     * @apiParam {Number} [pageSize=20] 单页数量
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function friendReqList(Request $request){
        global $g_uid;
        $page = $request->get('page', 1);
        $pageSize = $request->get('pageSize', 20);

        $userBlock = new UserBlock();

        $user = $userBlock->getUserFriendReq($g_uid, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success($user)->response());
    }

    /**
     * @api {post} /api/0.0.1/user/friend/req 好友请求
     * @apiName userFriendReq
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 好友请求
     * @apiParam {String} token 登录标识
     * @apiParam {String} friend 登录标识
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'balance': 'xx'     //账户余额
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function friendRequest(Request $request){
        global $g_uid;
        $friendId = $request->get('friend');

        \Log::debug($friendId);

        $friend = User::where('jg_im_username', $friendId)->whereNull('deleted_at')->first();

        \Log::debug($friend);

        if($friend) {
            $data = [
                'user_id' => $g_uid,
                'friend_id' => $friend->id,
                'create_time' => date('Y-m-d H:i:s', time())
            ];

            $userFriendModel = new UserFriends();
            if ($userFriendModel->saveData($data)) {
                return response()->json(ResponseMessage::getInstance()->success()->response());
            } else {
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * @api {post} /api/0.0.1/user/friend/res 好友请求处理
     * @apiName userFriendRes
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 好友请求处理
     * @apiParam {String} token 登录标识
     * @apiParam {String} status 状态
     * @apiParam {String} user_friend_id 好友列表ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function friendRes(Request $request){
        global $g_uid;
        $friendId = $request->get('user_friend_id');
        $status = $request->get('status');

        $userBlock = new UserBlock();

        if($userBlock->userFriendRes($g_uid, $friendId, $status)){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * @api {post} /api/0.0.1/user/group/nodisturb 添加群免打扰
     * @apiName UserGroupNodisturb
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 添加群免打扰
     * @apiParam {String} token 登录标识
     * @apiParam {String} crowd_id 群ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function addGroupNodisturb(Request $request){
        global $g_uid;
        $crowdId = $request->get('crowd_id');

        $crowdBlock = new CrowdBlock();

        $data = $crowdBlock->getCrowdMemberInfo($crowdId, $g_uid);

        \Log::debug(json_encode($data));

        if(!$data){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
        $jMessage = new JMessage();
        if($jMessage->addGroupNodisturb($data->jg_im_username, [$data->jg_im_gid])){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * @api {post} /api/0.0.1/user/group/nodisturb/remove 移出群免打扰
     * @apiName UserGroupNodisturbRemove
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 移出群免打扰
     * @apiParam {String} token 登录标识
     * @apiParam {String} crowd_id 群ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function removeGroupNodisturb(Request $request){
        global $g_uid;
        $crowdId = $request->get('crowd_id');

        $crowdBlock = new CrowdBlock();

        $data = $crowdBlock->getCrowdMemberInfo($crowdId, $g_uid);
        if(!$data){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
        $jMessage = new JMessage();
        if($jMessage->removeGroupNodisturb($data->jg_im_username, [$data->jg_im_gid])){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    public function userList(Request $request){
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $userMsg        = $request->get('userMsg');
//        if (!$userMsg){
//            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
//        }
        $search         = ['userMsg' => $userMsg];
        $commentList = new UserBlock();
        $list = $commentList->userList($search, $page, $pageSize);
        $pagination = $commentList->userListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $list, 'pagination' => $pagination])->response());
    }
}
