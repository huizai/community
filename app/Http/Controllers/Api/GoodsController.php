<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blocks\GoodsBlock;
use App\Libs\ResponseMessage;
use App\Blocks\OrderBlock;
class GoodsController extends Controller{

    /**
     * @api {get} /api/0.0.1/goods/list 群商品列表
     * @apiName goods_list
     * @apiGroup 商品
     * @apiVersion 0.0.1
     * @apiDescription 群商品列表
     * @apiParam {Number} crowd_id   群ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'id' : xx,                  //商品ID
     *          'crowd_id' : 'xx',          //群ID
     *          'small_image' : 'http://xxx.jpg',  //商品图片
     *          'salenum' : 'xx',       //销量
     *          'out_price' : 'xx',    //售价
     *          'name': xx,              //商品名字
     *          'inventory':'xx'        //库存
     *          'on_off_time':'xx'      //上架 下架时间
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function crowdGoods(Request $request){
        if ($request->has('crowd_id')){
            $crowd_id = $request->get('crowd_id');
            $goods = new GoodsBlock();
            $res = $goods->apiGoodList($crowd_id);
            if ($res){
                return response()->json(ResponseMessage::getInstance()->success($res)->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }

        }else{
            return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
        }
    }

    /**
     * @api {get} /api/0.0.1/goods/laser/list 激光群商品列表
     * @apiName goods_laser_list
     * @apiGroup 商品
     * @apiVersion 0.0.1
     * @apiDescription 激光群商品列表
     * @apiParam {Number} jg_im_gid  激光群ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'id' : 1,                  //商品ID
     *          'crowd_id' : '1',          //群ID
     *          'small_image' : 'http://xxx.jpg',  //商品图片
     *          'salenum' : '100',       //销量
     *          'out_price' : '100',    //售价
     *          'name': 1,              //商品名字
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function jg_crowd_goods(Request $request){
        if ($request->has('jg_im_gid')){
            $jg_id = $request->get('jg_im_gid');
            $goods = new GoodsBlock();
            $res = $goods->Api_jg_goods_list($jg_id);
            if ($res){
                return response()->json(ResponseMessage::getInstance()->success($res)->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }

        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
    }

    /**
     * @api {get} /api/0.0.1/goods/details 商品详情
     * @apiName goods_details
     * @apiGroup 商品
     * @apiVersion 0.0.1
     * @apiDescription 商品详情
     * @apiParam {Number} [id]   商品ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data: {
     *           "id": xx,                   //商品ID
     *           "show_title": "xx",         //标题
     *           "c_name": "xxx",            //群名称
     *           "name": "xx",               //商品名字
     *           "producing_area": "xx",     //产地
     *           "unit": "xx",               //规矩
     *           "salenum": xx,               //销量
     *           "slogan": "xx",              //广告语
     *           "illustrate": [                //商品插图
     *                      "xx.jpg",
     *           ],
     *           "now_price": 200,           //现价
     *           "out_price": 200,           //售价
     *           "img": [                     //商品轮播图
     *              {
     *                       "img_url": "https://community.guoxiaoge.cn/upload/p1.jpg"
     *              }
     *           ],
     *           "evalue_content": [
     *               {
     *                       "star": xx,                    //星星数
     *                       "content": "xx",               //评价内容
     *                       "goods_name": "xxx",           //商品名字
     *                       "headimgurl": xx               //用户头像
     *                       "nick_name": xx                //用户昵称
     *                       "create_time": xx              //评论时间
     *                       "img_url": []                  //评论图片
     *               },
     *           ]
     *      }
     *}
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function goodsDetails(Request $request){
        if($request->has('id')){
            $id = $request->get('id');
            $goods = new GoodsBlock();
            return response()->json(ResponseMessage::getInstance()->success($goods->Api_goods_details($id))->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
    }


    /**
     * @api {get} /api/0.0.1/goods/goods_update 商品修改
     * @apiName goods_update
     * @apiGroup 商品
     * @apiVersion 0.0.1
     * @apiDescription 商品修改
     * @apiParam {Number} id                商品ID
     * @apiParam {String} [show_title]      显示的标题
     * @apiParam {String} [name]            商品名称
     * @apiParam {String} [producing_area]  产地
     * @apiParam {String} [slogan]          广告语
     * @apiParam {String} [unit]            规矩
     * @apiParam {Number} [c_id]            分类ID
     * @apiParam {Number} [now_price]       现价
     * @apiParam {Number} [out_price]       售价
     * @apiParam {Number} [in_price]        进价
     * @apiParam {Number} [status]          是否上架 1:上架 , 0:下架
     * @apiParam {Number} [b_id]            品牌ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data: [],
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function goodsUpdate(Request $request){
        if ($request->has('id')) {
            $id = $request->post('id');
            $data['show_title'] = trim($request->post('show_title'));
            $data['name'] = trim($request->post('name'));
            $data['producing_area'] = trim($request->post('producing_area'));
            $data['slogan'] = trim($request->post('slogan'));
            $data['unit'] = trim($request->post('unit'));
            $data['c_id'] = $request->post('c_id');
            $data['illustrate'] = trim($request->post('illustrate'));
            $data['now_price'] = $request->post('now_price');
            $data['out_price'] = $request->post('out_price');
            $data['in_price'] = $request->post('in_price');
            $data['status'] = $request->post('status');
            $data['b_id'] = $request->post('b_id');
            $arr = array();
            foreach ($data as $key => $value) {
                if ($value != '') {
                    $arr[$key] = $value;
                }
            }
            $goosmodel = new GoodsBlock();
            $res = $goosmodel->good_update($id, $arr);
            if ($res) {
                return response()->json(ResponseMessage::getInstance()->success()->response());
            } else {
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        } else {
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

    }

    /**
     * 商品评论列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function goodCommentList(Request $request){
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 20);
        $goods_id       = $request->get('goods_id');
        if (!$goods_id){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search         = ['goods_id' => $goods_id];
        $commentList = new OrderBlock();
        $list = $commentList->commentList($search, $page, $pageSize);
        $pagination = $commentList->commentListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $list, 'pagination' => $pagination])->response());
    }


    /**
     * @api {post} /api/0.0.1/goods/goodRecommend 商品推荐
     * @apiName goodRecommend
     * @apiGroup 商品
     * @apiVersion 0.0.1
     * @apiDescription 商品推荐
     * @apiParam {Number} page              当前页
     * @apiParam {Number} pageSize          当前页大小
     * @apiParam {Number} crowd_id          群ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data: {
     *          "list": [
     *               {
     *                   "id": 9,
     *                   "crowd_id": 1,
     *                   "small_image": "http://community.img.guoxiaoge.cn/image_51585e19698427661d2b9e681d845b7f.jpg",
     *                   "salenum": 0,
     *                   "out_price": 19800,
     *                   "name": "男士外套春秋季2019新款修身帅气百搭薄款工装青年休闲运动夹克男",
     *                   "inventory": 0,
     *                   "on_off_time": "2019-04-02 15:16:33",
     *                   "created_at": "2019-04-02 15:16:33"
     *               },
     *          ],
     *          "pagination": {
     *               "total": 8,
     *               "pageSize": 5,
     *               "current": 1
     *           }
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function goodRecommend(Request $request){
        $page           = $request->get('page', 1);
        $pageSize       = $request->get('pageSize', 5);
        $crowd_id       = $request->get('crowd_id');
        if (!$crowd_id){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $search         = ['crowd_id' => $crowd_id];
        $commentList = new GoodsBlock();
        $list = $commentList->commentList($search, $page, $pageSize);
        $pagination = $commentList->commentListPagination($search, $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success(['list' => $list, 'pagination' => $pagination])->response());
    }
}