<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\SystemInformation;
use Illuminate\Http\Request;
use App\Libs\Jiguang\Jpush;

class SystemController extends Controller{

    /**
     * @api {post} /api/0.0.1/system/information 平台消息
     * @apiName information
     * @apiGroup 系统
     * @apiVersion 0.0.1
     * @apiDescription 平台消息
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": [
     *          {
     **             "id": 1,                                //消息ID
     *              "title": "测试系统消息"                  //消息标题
     *              "created_at": "2019-04-25 11:52:45",    //发布时间
     *              "type": 1,                              //0：文本消息  1：图文消息
     *              "img_url": xx.jpg                       //封面图
     *          }
     *       ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function information(Request $request){
        global $g_uid;
        $page = $request->post('page',1);
        $pageSize = $request->post('pageSize', 10);
        $informationModel = new SystemInformation();
        $data = $informationModel->informationTitleList($g_uid, $page, $pageSize);

        foreach ($data as $datum) {
            $datum->created_at = strtotime($datum->created_at);
        }

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * @api {post} /api/0.0.1/system/information/info 平台消息详情
     * @apiName informationInfo
     * @apiGroup 系统
     * @apiVersion 0.0.1
     * @apiDescription 平台消息详情
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": [
     *          {
     **             "id": 1,
     *              "title": "测试系统消息"
     *              "content": "<p>测试系统消息</p>",
     *              "created_at": "2019-04-25 11:52:45",
     *          }
     *       ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function informationInfo(Request $request){
        if (!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = (int)$request->post('id');

        $informationModel = new SystemInformation();
        $data = $informationModel->informationInfo($id);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    public function test(){
        $push = new Jpush();
        $push->pushInformation();
    }
}