<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Libs\Jiguang\Jpush;
use App\Libs\Jiguang\JpushCreateTopic;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;
use zgldh\QiniuStorage\QiniuStorage;

class ToolController extends Controller {
    /**
     * @api {post} /api/0.0.1/tool/upload/img 上传图片
     * @apiName uploadImg
     * @apiGroup 工具
     * @apiVersion 0.0.1
     * @apiDescription 上传图片
     * @apiParam {File} img 图片
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'img_path' : 'http://xxxx.png'
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function uploadImg(Request $request) {

        \Log::debug($request);
        $param = 'img';
        if($request->hasFile($param)){

            $file = $request->file($param);

//            $destinationPath	= public_path()."/upload";
//            $filename			= uniqid();
//
//            $exten = substr($file->getClientOriginalName()  , strpos($file->getClientOriginalName() , '.' ) + 1 );
//
//            if( $request->file($param)->isValid() ){
//                if( $file->move($destinationPath , $filename . '.' . $exten )  ){
//                    $filePath = env('APP_URL')."/upload/".$filename.'.'.$exten;
//                    return response()->json(ResponseMessage::getInstance()->success(['img_path' => $filePath])->response());
//                }
//            }

            // 初始化
            $disk = QiniuStorage::disk('qiniu');
            // 重命名文件
            $fileName = md5($file->getClientOriginalName().time().rand()).'.'.$file->getClientOriginalExtension();

            // 上传到七牛
            $bool = $disk->put('image_'.$fileName,file_get_contents($file->getRealPath()));
            // 判断是否上传成功
            if ($bool) {
                $path = $disk->downloadUrl('image_'.$fileName);
                return response()->json(ResponseMessage::getInstance()->success(['img_path' => $path])->response());
            }
        }

        return response()->json(ResponseMessage::getInstance()->failed()->response());
    }


    /**
     * @api {get} /api/0.0.1/tool/app/isupdate app是否更新
     * @apiName appIsupdate
     * @apiGroup 工具
     * @apiVersion 0.0.1
     * @apiDescription app是否更新
     * @apiParam {String='android', 'ios'} system 系统
     * @apiParam {String} version 当前版本号
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'version' : '0.0.1',         //最新版本号
     *          'is_force' : 0               //是否强制更新到新版本
     *          'newest': 1                  //是否是最新版本
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function appIsUpdate(Request $request){
        if(!$request->has('system') || !$request->has('version')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $system = $request->get('system');
        $version = $request->get('version');

        if($system === 'android'){
            $data = [
                'version' => '1.1',
                'is_force' => 0,
                'newest' => 1,
                'download_url' => 'https://api.diandao.net.cn/app-release.1.1.apk'
            ];
        }else{
            $data = [
                'version' => '0.0.1',
                'is_force' => 0,
                'newest' => 1
            ];
        }
        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    public function push(){
        $jpush = new Jpush();
        $jpush->pushAliasToAllPlatform(['1','4','5'], '极光别名推送测试', $extras = [
            'type' => 'CircleDetail',
            'id' => 21,
        ]);

        return response()->json(ResponseMessage::getInstance()->success()->response());
    }
}