<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Blocks\ActivityBlock;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;

class ActivityControllers extends Controller
{

    /**
     * @api {get} /api/0.0.1/activity/list 活动列表
     * @apiName activityList
     * @apiGroup 活动
     * @apiVersion 0.0.1
     * @apiDescription 活动列表
     * @apiParam {String} token       用户登陆标志
     * @apiParam {Number} crowd_id    群ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {[
     *                  'id' : 1,                        //活动ID
     *                  'title': 'xx',                   //活动标题
     *                  'charge' : 'xx',                 //活动费用
     *                  'address' : 'xx',                //活动地址
     *                  'headimgurl' : 234,              //列表图
     *                  'start_time' : 234,              //开始时间
     *                  'end_time' : 432,                //结束时间
     *      ]...}
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function activityList(Request $request){
        if (!$request->has('crowd_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $crowd_id = $request->get('crowd_id');
        $activityBlock = new ActivityBlock();
        $result = $activityBlock->activityList($crowd_id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * @api {get} /api/0.0.1/activity/info 活动详情
     * @apiName activityInfo
     * @apiGroup 活动
     * @apiVersion 0.0.1
     * @apiDescription 活动详情
     * @apiParam {String} token       用户登陆标志
     * @apiParam {Number} id          活动ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *                  'id' : 1,                           //活动ID
     *                  'title': 'xx',                      //活动标题
     *                  'charge' : 'xx',                    //活动费用
     *                  'address' : 'xx',                   //活动地址
     *                  'headimgurl' : xx.xx.jpg,           //列表图
     *                  'take_num': 'xx'                    //最多参与人数
     *                  'start_time' : xx-xx-xx xx:xx:xx,   //开始时间
     *                  'end_time' : xx-xx-xx xx:xx:xx,     //结束时间
     *                  'introduce': 'xx'                   //活动介绍
     *                  'created_at': xx-xx-xx xx:xx:xx     //活动发布时间
     *                  'isTake': 1                         //当前用户是否参与  0：未参与 1：已参与未支付 2：已参与已支付
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function activityInfo(Request $request){
        if (!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $id = $request->get('id');

        global $g_uid;
        $activityBlock = new ActivityBlock();
        $result = $activityBlock->activityInfo($g_uid,$id);
        if ($result){
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * @api {post} /api/0.0.1/activity/take 参与活动
     * @apiName activityTake
     * @apiGroup 活动
     * @apiVersion 0.0.1
     * @apiDescription 参与活动
     * @apiParam {String} token         用户登陆标志
     * @apiParam {Number} activity_id   活动ID
     * @apiParam {String} name          参与人姓名
     * @apiParam {Number} mobile        参与人手机号
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : []
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function takeActivity(Request $request){
        $is_post = ['name','mobile','activity_id'];
        foreach ($is_post as $item) {
            if (!$request->has($item)){
                return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
            }
            $data[$item] = $request->post($item);
        }
        $data['created_at'] = date('Y-m-d H:i:s',time());

        global $g_uid;
        $data['user_id'] = $g_uid;
        $data['is_pay'] = 1;
        $activityBlock = new ActivityBlock();
        $charge = $activityBlock->activityIsEnd($data['activity_id']);
        if($charge === false){
            return response()->json(ResponseMessage::getInstance()->failed('ACTIVITY_END')->response());
        }
        $data['pay_money'] = $charge;
        if ($charge>0){
            $data['is_pay'] = 0;
        }
        $result = $activityBlock->takeActivity($data);
        if (!$result){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        if ($result != 'take'){
            return response()->json(ResponseMessage::getInstance()->success(['order_id' => $result])->response());

        }else{
            return response()->json(ResponseMessage::getInstance()->failed('ALREADY_PARTICIPATED')->response());
        }

    }


    /**
     * @api {post} /api/0.0.1/activity/myactivity 我的活动
     * @apiName myactivity
     * @apiGroup 活动
     * @apiVersion 0.0.1
     * @apiDescription 我的活动
     * @apiParam {String} token         用户登陆标志
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {[
     *                  'name': 'xx'                     //群名
     *                  'id' : 1,                        //活动ID
     *                  'title': 'xx',                   //活动标题
     *                  'charge' : 'xx',                 //活动费用
     *                  'address' : 'xx',                //活动地址
     *                  'headimgurl' : 234,              //列表图
     *                  'start_time' : 234,              //开始时间
     *                  'end_time' : 432,                //结束时间
     *                  'is_pay': 0                      //0 未付费参与 1 已付费参与
     *                  ]...}
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function myActivity(){
        $activityBlock =  new ActivityBlock();
        global $g_uid;
        $list = $activityBlock->myActivity($g_uid);

        return response()->json(ResponseMessage::getInstance()->success($list)->response());
    }



}