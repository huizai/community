<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Blocks\GoodsBlock;
use Illuminate\Http\Request;
class CateController extends Controller
{
    /**
     * @api {get} /api/0.0.1/crowd/cate 群商品分类
     * @apiName crowd_cate
     * @apiGroup 商品
     * @apiVersion 0.0.1
     * @apiDescription 群商品分类
     * @apiParam {Number} crowd_id   群ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'id' : 'xxx',              //商品ID
     *          'crowd_id' : 'xxx',        //群ID
     *          'name' : 'xx',             //分类名称
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function cateList(Request $request){
        if ($request->has('crowd_id')){
            $id = $request->get('crowd_id');
            $cate = new GoodsBlock();
            $res = $cate->crowd_cate($id);
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
    }


    /**
     * @api {get} /api/0.0.1/crowd/cate/goods 群商品分类商品
     * @apiName crowd_cate_goods
     * @apiGroup 商品
     * @apiVersion 0.0.1
     * @apiDescription 群商品分类商品
     * @apiParam {Number} cate_id  群商品分类ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'id' : 'xxx',              //商品ID
     *          'crowd_id' : 'xxx',        //群ID
     *          'name' : 'xx',             //商品名称
     *          'small_image' : 'xx',      //商品图片
     *          'out_price' : 'xx',        //售价
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function crowdCateGoodsList(Request $request){
        if ($request->has('cate_id')){
            $id = $request->get('cate_id');
            $cate = new GoodsBlock();
            $res = $cate->crowd_cate_goods($id);
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
    }

}