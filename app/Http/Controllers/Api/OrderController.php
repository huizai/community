<?php
namespace App\Http\Controllers\Api;

use App\Blocks\OrderBlock;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
use App\Models\UserAddress;
class OrderController extends Controller{

    /**
     * @api {post} /api/0.0.1/orders/myorder 获取订单列表
     * @apiName myorder
     * @apiGroup 订单
     * @apiVersion 0.0.1
     * @apiDescription 获取订单列表
     * @apiParam {String} token    登陆标志
     * @apiParam {Number} [status] 订单状态
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *              'id',               //订单ID
     *              'crowd_id',         //群ID
     *              'c_name',             //群名
     *              'freight',          //运费
     *              'pay_total',        //总共需支付的总价
     *              'order_num',        //订单号
     *              'service_charge',   //服务费
     *              'status',           //订单状态 0未付款 1已支付 2已发货 3已收货 4已评价 5退款中 6已退款 7已取消
     *              'message',           //订单状态 0未付款 1已支付 2已发货 3已收货 4已评价 5退款中 6已退款 7已取消
     *              'nick_name',        //购买人
     *              'b_name',           //购买商品品牌名称
     *              'name',             //购买商品名称
     *              'small_img'         //商品列表图
     *              'buy_num',          //购买数量
     *              'now_price',        //购买商品现价
     *              'province',         //购买人所在省
     *              'city',             //购买人所在市、区
     *              'district',         //购买人所在县
     *              'address',          //详细地址
     *              'created_at'        //订单创建时间
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function myOrder(Request $request){
            $order = new OrderBlock();
            $status = $request->get('status');
            global $g_uid;
            $res = $order->my_orders($g_uid,$status);
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }


    /**
     * @api {post} /api/0.0.1/orders/create 创建订单
     * @apiName order_create
     * @apiGroup 订单
     * @apiVersion 0.0.1
     * @apiDescription 创建订单
     * @apiParam {String} token           用户登陆标志
     * @apiParam {Number} crowd_id        群ID
     * @apiParam {Number} service_charge  服务费
     * @apiParam {Number} freight         运费
     * @apiParam {Number} g_total_price   商品总价
     * @apiParam {Number} pay_total       总共需支付的总价
     * @apiParam {Number} g_id            商品ID
     * @apiParam {Number} buy_num         购买数量
     * @apiParam {Number} a_id            用户填写地址ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : []
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function createOrder(Request $request){

        $data['crowd_id'] = (int)$request->post('crowd_id');
        $data['service_charge'] = (int)$request->post('service_charge');
        $data['freight'] = (int)$request->post('freight');
        $data['g_total_price'] = (int)$request->post('g_total_price');
        $data['pay_total'] = (int)$request->post('pay_total');

        $data['g_id'] = $request->post("g_id");
        $data['buy_num'] = $request->post("buy_num");

        $data['a_id'] = $request->post('a_id');
        $add = UserAddress::where('id',$data['a_id'])->whereNull('deleted_at')->first();
        if (!$add){
            return response()->json(ResponseMessage::getInstance()->failed('CHOOSE_ADDRESS')->response());
        }
        $num = 0;
        foreach ($data as $value){
            if ($value === ''){
                $num = $num+1;
            }
        }
        if ($num>0){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        global $g_uid;
        $data['u_id'] = $g_uid;
        $order = new OrderBlock();
        $res = $order->create_order($data);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }


    }

    /**
     * @api {post} /api/0.0.1/orders/cancel 取消订单、退款
     * @apiName order_cancel
     * @apiGroup 订单
     * @apiVersion 0.0.1
     * @apiDescription 取消订单、退款
     * @apiParam {Number} status        当前订单状态
     * @apiParam {Number} order_id      订单ID
     * @apiParam {String} token         用户登陆标志
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *          "status": 7         //5:退款中 7:已取消
     *          "message": "已取消" //5:退款中 7:已取消
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function cancelOrder(Request $request){
        if (!$request->has('status') || !$request->has('order_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $status = $request->post('status');
        $order_id = $request->post('order_id');
        $statusData = [5,6];
        if (in_array(($status),$statusData)){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        global $g_uid;
        $order = new OrderBlock();
        $res = $order->CancelOrder($g_uid,$status,$order_id);

        if ($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
 * @api {post} /api/0.0.1/order 订单详情
 * @apiName order
 * @apiGroup 订单
 * @apiVersion 0.0.1
 * @apiDescription 订单详情
 * @apiParam {Number} order_id      订单ID
 * @apiSuccessExample {json} 操作成功响应示例
 * {
 *      'status' : 'success',
 *      'failedCode' : '',
 *      'failedMsg' : '',
 *      'data'  : [
 *      ]
 * }
 * @apiErrorExample {json} 操作失败响应示例
 * {
 *      'status' : 'failed',
 *      'failedCode' : 'ERROR CODE',
 *      'failedMsg' : 'ERROR MSG',
 *      'data'  : []
 * }
 */
    public function orderInfo(Request $request){
        $id = $request->get('id');
        $orderBlock = new OrderBlock();

        $order = $orderBlock->my_orders_details($id);

        return response()->json(ResponseMessage::getInstance()->success($order)->response());
    }


    /**
     * @api {post} /api/0.0.1/order 订单详情
     * @apiName order
     * @apiGroup 订单
     * @apiVersion 0.0.1
     * @apiDescription 确认收货
     * @apiParam {Number} order_id      订单ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *          "status": 3         //3:已收货
     *          "message": "已收货" //3:已收货
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     **/
    public function orderTake(Request $request){
        $order_id = $request->post('order_id');
        global $g_uid;
        $order = new OrderBlock();
        $res = $order->TakeOrder($g_uid,$order_id);

        if (is_int($res)){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->setFailedCode('TAKE_ERROR', $res)->response());
        }
    }


    /**
     * @api {post} /api/0.0.1/orders/take 添加商品评论
     * @apiName addGoodComment
     * @apiGroup 订单
     * @apiVersion 0.0.1
     * @apiDescription 添加商品评论
     * @apiParam {Number} order_id      订单ID
     * @apiParam {String} content       评论内容
     * @apiParam {Number} star          评分星数
     * @apiParam {Array}  image         图片
     * @apiParam {Number} goods_id      商品ID
     * @apiParam {String} goods_name    商品名字
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : 1  //新增ID
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     **/
    public function addGoodComment(Request $request){
        global $g_uid;
        $data = $request->only(['order_id','content','star','image','goods_id','goods_name']);
        if (!isset($data['order_id'])||!isset($data['content'])||!isset($data['star'])||!isset($data['goods_id'])||!isset($data['goods_name'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        \Log::debug($data);
        $storeBlock = new OrderBlock();
        $res = $storeBlock->saveGoodComment($g_uid,$data);
        if (is_int($res)){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->setFailedCode('AddComment',$res)->failed()->response());
        }
    }

}