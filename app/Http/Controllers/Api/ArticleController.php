<?php

namespace App\Http\Controllers\Api;
use App\Blocks\ArticleBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\CrowdArticleComment;
use App\Models\CrowdArticleUsefulLog;
use Illuminate\Http\Request;

class ArticleController extends Controller {

    /**
     * @api {get} /api/0.0.1/article/info 文章详情
     * @apiName articleInfo
     * @apiGroup 文章
     * @apiVersion 0.0.1
     * @apiDescription 文章详情
     * @apiParam {String} token 用户登陆标志
     * @apiParam {Number} id    群ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {[
     *                  'id' : 1,                        //文章ID
     *                  'title': 'xxxxx',                //文章标题
     *                  'content' : 'xxxxx',             //文章内容
     *                  'headimgurl' : 'xxxxxx',         //文章小图
     *                  'comment' : 234,                 //评论数量
     *                  'useful' : 234,                  //点赞数量
     *                  'share' : 432,                   //分享次数
     *                  'created_at': '2018-09-21 12:21:12' //创建时间
     *                  'is_useful':'xx'                 //是否点赞 0：未点赞 1：已点赞
     *      ]...}
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function getArticleInfo(Request $request){
        if (!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $id = (int)$request->get('id');
        $token = $request->get('token');
        $userInfo = \Cache::get($token);
        $uid = null;
        if($userInfo){
            $uid = $userInfo->id;
        }
        $articleBlock = new ArticleBlock();

        $article = $articleBlock->getArticleInfo($id,$uid);

        return response()->json(ResponseMessage::getInstance()->success($article)->response());
    }

    /**
     * @api {get} /api/0.0.1/article/comment 文章评论
     * @apiName articleComment
     * @apiGroup 文章
     * @apiVersion 0.0.1
     * @apiDescription 文章评论
     * @apiParam {Number} [article_id] 文章ID
     * @apiParam {Number} [page=1] 分页
     * @apiParam {Number} [pageSize=20] 单页数量
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {[
     *          'topic_id'      : 1,                 //评论ID,
     *          'user_id'       : 1,                 //用户ID,
     *          'nick_name'     : '张三',             //用户昵称,
     *          'headimgurl'    : 'http://xxx.png'   //头像,
     *          'content'       : 'xxxxx'            //内容,
     *          'created_at'    : ''                 //发布时间
     * ]...}
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function getArticleComment(Request $request){
        $page = (int)$request->get('page', 1);
        $pageSize = (int)$request->get('pageSize', 20);

        $articleId = (int)$request->get('article_id');
        $articleBlock = new ArticleBlock();
        $comment = $articleBlock->getArticleComment(['article_id' => $articleId], $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success($comment)->response());
    }

    /**
     * @api {get} /api/0.0.1/article/recommend 推荐文章
     * @apiName articleRecommend
     * @apiGroup 文章
     * @apiVersion 0.0.1
     * @apiDescription 推荐文章
     * @apiParam {Number} [article_id] 当前打开的文章ID
     * @apiParam {Number} [page=1] 分页
     * @apiParam {Number} [pageSize=20] 单页数量
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [{
     *                  'id' : 1,                        //文章ID
     *                  'title': 'xxxxx',                //文章标题
     *                  'content' : 'xxxxx',             //文章内容
     *                  'headimgurl' : 'xxxxxx',         //问题小图
     *                  'comment' : 234,                 //评论数量
     *                  'useful' : 234,                  //点赞数量
     *                  'share' : 432,                   //分享次数
     *                  'created_at': '2018-09-21 12:21:12' //创建时间
     *
     *          }...]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function getRecommendArticle(Request $request){
        $articleBlock = new ArticleBlock();
        $page = (int)$request->get('page', 1);
        $pageSize = (int)$request->get('pageSize', 20);

        if($request->has('article_id')){
            $articleId = $request->get('article_id');
            $page = 0;
            $pageSize = 3;
        }

        $article = $articleBlock->getArticleList(['recommend' => 1], $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success($article)->response());
    }

    /**
     * @api {post} /api/0.0.1/article/create/comment 创建文章评论
     * @apiName createComment
     * @apiGroup 文章
     * @apiVersion 0.0.1
     * @apiDescription 创建文章评论
     * @apiParam {Number} article_id 文章ID
     * @apiParam {String} token 用户登录标识
     * @apiParam {String} content 评论内容
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'article_id': 'xx'                  //文章ID
     *          'user_id': 'xx'                     //评论用户ID
     *          'content': 'xx'                     //用户评论评论
     *          'created_at': 'xx-xx-xx xx:xx:xx    //评论时间
     *          'nick_name': 'xx'                   //评论用户昵称
     *          'headimgurl': 'xx'                  //评论用户头像
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function createComment(Request $request){
        global $g_uid;
        if(!$request->has('article_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        if(!$request->has('content')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $saveData = [
            'user_id'   => $g_uid,
            'article_id'  => $request->get('article_id'),
            'content'   => $request->get('content'),
            'created_at' => date('Y-m-d H:i:s', time())
        ];

        $ArticleBlock = new ArticleBlock();
        $result = $ArticleBlock->createComment($saveData);
        if($result){
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * @api {post} /api/0.0.1/article/useful 点赞
     * @apiName useful
     * @apiGroup 文章
     * @apiVersion 0.0.1
     * @apiDescription 点赞
     * @apiParam {String} token 用户登录标识
     * @apiParam {Number} article_id 动态ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : []
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function useful(Request $request){
        global $g_uid;
        if(!$request->has('article_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $articleId = $request->get('article_id');
        if(CrowdArticleUsefulLog::where('article_id', $articleId)->where('user_id', $g_uid)->first()){
            return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
        }

        $articleBlock = new ArticleBlock();

        if($articleBlock->useful($g_uid, $articleId)){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }
}