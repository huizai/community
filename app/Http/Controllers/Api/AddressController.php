<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Blocks\AddressBlock;
use App\Libs\ResponseMessage;
use App\Models\AmapCityCode;
use Illuminate\Http\Request;

class AddressController extends Controller{

    /**
     * @api {get} /api/0.0.1/address/myaddress 获取收货地址列表
     * @apiName myaddress
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 获取收货地址列表
     * @apiParam {String} token  登陆标志
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          'id': 'xx'                   //地址ID
     *          'name' : 'xxx',              //收货人
     *          'mobile' : 'xxx',            //收获电话
     *          'province' : 'xx',           //省
     *          'city' : 'xx',               //市
     *          'district' : 'xx',           //区
     *          'address': 'xx',             //详细地址
     *          'useing' : '0'               //默认地址，1默认，0不默认
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function myAddress(){
        global $g_uid;
        $address = new AddressBlock();
        $res = $address->my_address($g_uid);
        return response()->json(ResponseMessage::getInstance()->success($res)->response());

    }

    /**
     * @api {post} /api/0.0.1/address/add 添加收货地址
     * @apiName address_add
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 添加收货地址
     * @apiParam {String} token       登陆标志
     * @apiParam {String} name        收货人姓名
     * @apiParam {Number} mobile      收货人电话
     * @apiParam {String} province    省份
     * @apiParam {String} city        市
     * @apiParam {String} district    区、县
     * @apiParam {String} address     详细地址
     * @apiParam {Number} useing      是否默认地址 1：默认 0不默认
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : []
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function myAddressInsert(Request $request){
        global $g_uid;
        $data['u_id'] = $g_uid;
        $data['name'] = trim($request->post('name'));
        $data['mobile'] = (int)$request->post('mobile');
        $data['province'] = trim($request->post('province'));
        $data['city'] = trim($request->post('city'));
        $data['district'] = trim($request->post('district'));
        $data['address'] = trim($request->post('address'));
        $data['useing'] = trim($request->post('useing',0));
        $num = 0;
        foreach ($data as $value){
            if ($value == ''){
                $num = $num+1;
            }
        }
        if ($num>0){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $address = new AddressBlock();
        $res = $address->my_address_save($data);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * @api {post} /api/0.0.1/address/save 修改收货地址
     * @apiName address_save
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 修改收货地址
     * @apiParam {String} token         登陆标志
     * @apiParam {Number} id            收货地址ID
     * @apiParam {Number} [name]        收货人姓名
     * @apiParam {Number} [mobile]      收货人电话
     * @apiParam {Number} [province]    省份
     * @apiParam {Number} [city]        市
     * @apiParam {Number} [district]    区、县
     * @apiParam {Number} [address]     详细地址
     * @apiParam {Number} useing      是否默认地址 1：默认 0不默认
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : []
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function myAddressUpdate(Request $request){
        global $g_uid;
        if($request->has('id')){
            $data['id'] = $request->post('id');
            $data['name'] = trim($request->post('name'));
            $data['mobile'] = $request->post('mobile');
            $data['province'] = trim($request->post('province'));
            $data['city'] = trim($request->post('city'));
            $data['district'] = trim($request->post('district'));
            $data['address'] = trim($request->post('address'));
            $data['u_id'] = $g_uid;
            $data['useing'] = trim($request->post('useing',0));

            foreach ($data as $key => $value) {
                if ($value != '') {
                    $arr[$key] = $value;
                }
            }
            $address = new AddressBlock();
            $res = $address->my_address_update($arr);
            if ($res){
                return response()->json(ResponseMessage::getInstance()->success()->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

    }


    /**
     * @api {post} /api/0.0.1/address/del 删除收货地址
     * @apiName del
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 删除收货地址
     * @apiParam {String} token       登陆标志
     * @apiParam {Number} id          收获地址ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : []
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function addressDel(Request $request){
        if ($request->has('id')){
            $data['id'] = (int)$request->post('id');
            $address = new AddressBlock();
            $res = $address->my_address_del($data);
            if ($res){
                return response()->json(ResponseMessage::getInstance()->success()->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

    }


    /**
     * @api {post} /api/0.0.1/address/area 获取城市列表
     * @apiName address_area
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 获取城市列表
     * @apiParam {String} token       登陆标志
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      status: "success",
     *      failedCode: "",
     *      failedMsg: "",
     *      data: [
     *          {
     *              xxx:[           //省、直辖市
     *                {
     *                  xxx::[      //区、市
     *                      xxx,    //区、县
     *                  ]
     *                }
     *              ]
     *          }
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function getArea(Request $request){
        $province = AmapCityCode::select('name','id')->orderBy('adcode', 'asc')->where('level','province')->orderBy('city_code', 'asc')->get();
        $city = AmapCityCode::select('name','parent_id','id')->where('level','city')->get();
        $district = AmapCityCode::select('name','parent_id')->where('level','district')->get();
        $province = $this->tree_data($province);
        $city = $this->tree_data($city);
        $district = $this->tree_data($district);
        $res = $this->tree_city($province,$city,$district);
        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }


    private function tree_data($data){
        $data = json_decode(json_encode($data), true);
        return $data;
    }
    private function tree_city($province,$city,$district){
        $province_data=[];
        $city_data=[];
        $district_data=[];
        foreach ($province as $value){
            $province_data[$value['name']] = $value;
        }
        foreach ($province_data as $key => $province_data_list) {
            foreach ($city as $citys){
                if ($citys['parent_id'] == $province_data_list['id'])
                    $city_data[$key][][$citys['name']] = $citys['id'];
            }
        }
        foreach ($city_data as $city_data_key => $city_data_list){
            foreach ($city_data_list as $item_key => $item){
                foreach ($item as $items => $values ){
                    foreach ($district as $key => $value){
                        if ($value['parent_id'] == $values){
                            $district_data[$city_data_key][$item_key][$items][] = $value['name'];
                        }
                    }
                }
            }
        }
        $res = [];
        foreach ($district_data as $key => $value){
            $res[][$key] = $value;
        }
        return $res;
    }
    


}