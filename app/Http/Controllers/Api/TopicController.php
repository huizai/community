<?php

namespace App\Http\Controllers\Api;
use App\Blocks\TopicBlock;
use App\Blocks\TopicDataBlock;
use App\Http\Controllers\Controller;
use App\Http\Requests\Topic;
use App\Jobs\JpushCreateTopic;
use App\Libs\ResponseMessage;
use App\Models\TopicComment;
use App\Models\TopicUsefulLog;
use http\Env\Response;
use Illuminate\Http\Request;

class TopicController extends Controller {

    /**
     * @api {get} /api/0.0.1/topic/list 获取动态列表
     * @apiName getCrowdTopic
     * @apiGroup 动态
     * @apiVersion 0.0.1
     * @apiDescription 获取动态列表
     * @apiParam {Number} [page=1] 分页
     * @apiParam {Number} [pageSize=20] 单页数量
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [{
     *                  'id' : 1                         //动态ID
     *                  'uid'      : 1,                  //用户ID
     *                  'nick_name': '张三',              //用户昵称
     *                  'content'  : 'xxxxx',            //动态内容
     *                  'useful'   : 1,                 //是否点赞，1：已点赞 0：未点赞
     *                  'comment'  : 123,                //评论的数量
     *                  'imgs'     : [                   //图片
     *                      {
     *                          'id' : 1,                       //图片ID
     *                          'imgurl' : 'http://xxxx.png',   //图片地址
     *                      },
     *                      ...
     *                  ]
     *                  comment_content: [                  //评论内容数据
     *                      {
     *                          topic_id: 1,                //动态ID
     *                          user_id: 22,                //评论用户ID
     *                          content: "写的挺好的！",      //评论内容
     *                          created_at: "2019-01-16 15:03:10",  //评论时间
     *                          updated_at: "2019-01-16 15:06:47",
     *                          deleted_at: null,
     *                          nick_name: "H@jh"           //评论用户
     *                      },
     *                      ...
     *                  ],
     *                  UsefulCentent: [                    //点赞数据
     *                      {
     *                          topic_id: 1,                //动态ID
     *                          user_id: 22,                //点赞用户ID
     *                          updated_at: "2019-01-16 15:24:16",
     *                          nick_name: "H@jh"           //点赞用户
     *                      },
     *                      ...
     *                  ]
     *          }...]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function getCrowdTopic(Request $request){
        $page = (int)$request->get('page', 1);
        $pageSize = (int)$request->get('pageSize', 20);
        global $g_uid;
        $topicBlock = new TopicBlock();
        $topic = $topicBlock->getTopicList($g_uid, $page, $pageSize);

        foreach ($topic as $t){
            $t->can_delete = false;
            if($t->uid === $g_uid){
                $t->can_delete = true;
            }
        }
        return response()->json(ResponseMessage::getInstance()->success($topic)->response());
    }
    /**
     * @api {post} /api/0.0.1/topic/create 发布动态
     * @apiName topicCreate
     * @apiGroup 动态
     * @apiVersion 0.0.1
     * @apiDescription 发布动态
     * @apiParam {String} content 内容
     * @apiParam {String} [corwn_id] 群Id
     * @apiParam {String[]} img 图片
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {}
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function createTopic(Topic $request){
        $content = $request->get('content');
        global $g_uid;

        $img = $request->get('img');
        $crowdId = $request->get('crowd_id');

        $topicBlock = new TopicBlock();
        $topicId = $topicBlock->createTopic([
            'content'   => $content,
            'img'       => $img,
            'uid'       => $g_uid,
            'crowd_id'  => $crowdId
        ]);

        if($topicId) {
//            $this->dispatch((new JpushCreateTopic($topicId, '群友发布了新动态'))->onQueue('createTopic'));
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * @api {get} /api/0.0.1/topic/info 动态详情
     * @apiName topicInfo
     * @apiGroup 动态
     * @apiVersion 0.0.1
     * @apiDescription 动态详情
     * @apiParam {Number} id 动态ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [{
     *                  'id' : 1                         //动态ID
     *                  'uid'      : 1,                  //用户ID
     *                  'nick_name': '张三',              //用户昵称
     *                  'content'  : 'xxxxx',            //动态内容
     *                  'useful'   : 1,                 //是否点赞，1：已点赞 0：未点赞
     *                  'comment'  : 123,                //评论的数量,
     *                  'crowd_id' : 1                   //群id,
     *                  'crowd_name' : '测试'             //群名称
     *                  'imgs'     : [                   //图片
     *                      {
     *                          'id' : 1,                       //图片ID
     *                          'imgurl' : 'http://xxxx.png',   //图片地址
     *                      },
     *                      ...
     *                  ]
     *                  comment_content: [                  //评论内容数据
     *                      {
     *                          topic_id: 1,                //动态ID
     *                          user_id: 22,                //评论用户ID
     *                          content: "写的挺好的！",      //评论内容
     *                          created_at: "2019-01-16 15:03:10",  //评论时间
     *                          updated_at: "2019-01-16 15:06:47",
     *                          deleted_at: null,
     *                          nick_name: "H@jh"           //评论用户
     *                      },
     *                      ...
     *                  ],
     *                  UsefulCentent: [                    //点赞数据
     *                      {
     *                          topic_id: 1,                //动态ID
     *                          user_id: 22,                //点赞用户ID
     *                          updated_at: "2019-01-16 15:24:16",
     *                          nick_name: "H@jh"           //点赞用户
     *                      },
     *                      ...
     *                  ]
     *          }...]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function getTopicInfo(Request $request){
        if (!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $id = (int)$request->get('id');
        $topicBlock = new TopicBlock();

        $topic = $topicBlock->getTopicInfo($id);

        return response()->json(ResponseMessage::getInstance()->success($topic)->response());
    }

    /**
     * @api {get} /api/0.0.1/topic/comment 动态评论
     * @apiName topicComment
     * @apiGroup 动态
     * @apiVersion 0.0.1
     * @apiDescription 动态评论
     * @apiParam {Number} [topic_id] 动态ID
     * @apiParam {Number} [page=1] 分页
     * @apiParam {Number} [pageSize=20] 单页数量
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {[
     *          'topic_id'      : 1,                 //评论ID,
     *          'user_id'       : 1,                 //用户ID,
     *          'nick_name'     : '张三',             //用户昵称,
     *          'headimgurl'    : 'http://xxx.png'   //头像,
     *          'content'       : 'xxxxx'            //内容,
     *          'created_at'    : ''                 //发布时间
     * ]...}
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function getTopicComment(Request $request){
        $page = (int)$request->get('page', 1);
        $pageSize = (int)$request->get('pageSize', 20);

        $topicId = (int)$request->get('topic_id');
        $topicBlock = new TopicBlock();
        $comment = $topicBlock->getTopicComment(['topic_id' => $topicId], $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success($comment)->response());
    }


    /**
     * @api {get} /api/0.0.1/topic/news 获取消息列表
     * @apiName getNews
     * @apiGroup 动态
     * @apiVersion 0.0.1
     * @apiDescription 获取消息列表
     * @apiParam {String} token 用户登录标识
     * @apiParam {Number} [page=1] 分页
     * @apiParam {Number} [pageSize=20] 单页数量
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [{
     *                  'id' : 1                         //动态ID
     *                  'uid'      : 1,                  //用户ID
     *                  'nick_name': '张三',              //用户昵称
     *                  'content'  : 'xxxxx',            //动态内容
     *                  'useful'   : 123,                //点赞的数量
     *                  'comment'  : 123,                //评论的数量
     *                  'type'     : 1,                  //消息类型；1动态，2，活动
     *                  'redirect' : 'https://xxxx'     //跳转页面h5
     *                  'imgs'     : [                   //图片
     *                      {
     *                          'id' : 1,                       //图片ID
     *                          'imgurl' : 'http://xxxx.png',   //图片地址
     *                      },
     *                      ...
     *                  ]
     *          }...]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function getNews(Request $request){
        global $g_uid;
        $page = (int)$request->get('page', 1);
        $pageSize = (int)$request->get('pageSize', 20);

        $topicBlock = new TopicBlock();
        $topic = $topicBlock->getJoinCrowdTopicList(['user_id' => $g_uid], $page, $pageSize);
        return response()->json(ResponseMessage::getInstance()->success($topic)->response());
    }

    /**
     * @api {post} /api/0.0.1/topic/create/comment 创建动态评论
     * @apiName createComment
     * @apiGroup 动态
     * @apiVersion 0.0.1
     * @apiDescription 创建动态评论
     * @apiParam {String} token 用户登录标识
     * @apiParam {Number} topic_id 动态ID
     * @apiParam {String} content 评论内容
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *           "id": 'xx',            //评论ID
     *           "topic_id": 'xx',      //动态ID
     *           "user_id": 'xx',       //评论人ID
     *           "content": 'xx',       //评论内容
     *           "created_at": 'xx',    //评论时间
     *           "updated_at": 'xx',    //评论修改时间
     *           "deleted_at": 'xx',    //评论删除时间
     *           "nick_name": 'xx'      //评论人昵称
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function createComment(Request $request){
        global $g_uid;
        if(!$request->has('topic_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        if(!$request->has('content')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $topicId = $request->get('topic_id');
        $comment = $request->get('content');
        $topicBlock = new TopicBlock();
        $res = $topicBlock->createComment($g_uid,$topicId,$comment);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    /**
     * @api {post} /api/0.0.1/topic/useful 点赞
     * @apiName useful
     * @apiGroup 动态
     * @apiVersion 0.0.1
     * @apiDescription 点赞
     * @apiParam {String} token 用户登录标识
     * @apiParam {Number} topic_id 动态ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
                'topic_id': 'xx'        //动态ID
     *          'user_id': 'xx'         //用户ID
     *          'updated_id':'xx'       //点赞时间
     *          'nick_name':'xx'        //点赞用户昵称
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function useful(Request $request){
        global $g_uid;
        if(!$request->has('topic_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $topicId = $request->get('topic_id');
        if(TopicUsefulLog::where('topic_id', $topicId)->where('user_id', $g_uid)->whereNull('deleted_at')->first()){
            return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
        }

        $topicBlock = new TopicBlock();
        $res = $topicBlock->useful($g_uid, $topicId);
        if($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }

    /**
     * @api {post} /api/0.0.1/topic/deltopic 删除动态
     * @apiName topic_del
     * @apiGroup 动态
     * @apiVersion 0.0.1
     * @apiDescription 删除动态
     * @apiParam {Number} id 动态ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function topicDel(Request $request)
    {
        if($request->has('id')){

            $topic = new TopicDataBlock();
            $id = $request->post('id');
            $res = $topic->topic_del($id);
            if ($res){
                return response()->json(ResponseMessage::getInstance()->success()->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed($res)->response());
            }
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
    }

    /**
     * @api {post} /api/0.0.1/topic/cancel/useful 取消点赞
     * @apiName topicCancelUseful
     * @apiGroup 动态
     * @apiVersion 0.0.1
     * @apiDescription 取消点赞
     * @apiParam {Number} topic_id 动态ID
     * @apiParam {String} token    用户登陆标志
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": {
                'user_id':'xx'      //用户ID
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function cancelUseful(Request $request){
        if (!$request->has('topic_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        global $g_uid;


        $topicId = $request->get('topic_id');

        $is_useful = TopicUsefulLog::where('topic_id', $topicId)->where('user_id', $g_uid)->whereNotNull('deleted_at')->first();
        if ($is_useful){
            return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
        }

        $topicBlock = new TopicBlock();
        $res = $topicBlock->cancelUseful($g_uid,$topicId);


        if ($res){
            return response()->json(ResponseMessage::getInstance()->success(['user_id'=>$res])->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * @api {post} /api/0.0.1/topic/mytopic 我的动态
     * @apiName myTopic
     * @apiGroup 动态
     * @apiVersion 0.0.1
     * @apiDescription 我的动态
     * @apiParam {String} token    用户登陆标志
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [{
     *                  'id' : 1                         //动态ID
     *                  'uid'      : 1,                  //用户ID
     *                  'nick_name': '张三',              //用户昵称
     *                  'content'  : 'xxxxx',            //动态内容
     *                  'created_at': 'xxx'              //发布时间
     *                  'imgs'     : [                   //图片
     *                      {
     *                          'id' : 1,                       //图片ID
     *                          'imgurl' : 'http://xxxx.png',   //图片地址
     *                      },
     *                      ...
     *                  ]
     *                  comment_content: [                  //评论内容数据
     *                      {
     *                          id :'xx'                    //评论ID
     *                          topic_id: 1,                //动态ID
     *                          user_id: 22,                //评论用户ID
     *                          content: "写的挺好的！",      //评论内容
     *                          created_at: "2019-01-16 15:03:10",  //评论时间
     *                          updated_at: "2019-01-16 15:06:47",
     *                          deleted_at: null,
     *                          nick_name: "H@jh"           //评论用户
     *                      },
     *                      ...
     *                  ],
     *                  UsefulCentent: [                    //点赞数据
     *                      {
     *                          topic_id: 1,                //动态ID
     *                          user_id: 22,                //点赞用户ID
     *                          updated_at: "2019-01-16 15:24:16",
     *                          nick_name: "H@jh"           //点赞用户
     *                      },
     *                      ...
     *                  ]
     *          }...]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function myTopic(){
        global $g_uid;
        $topicBlock = new TopicBlock();
        $res = $topicBlock->myTopic($g_uid);
        if ($res){
            return response(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * @api {post} /api/0.0.1/topic/unread 未读动态信息
     * @apiName TopicUnread
     * @apiGroup 动态
     * @apiVersion 0.0.1
     * @apiDescription 未读动态信息
     * @apiParam {String} token    用户登陆标志
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *          "message": xx    //未读消息数
     *          "headImg": xx    //用户头像
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function unread(){
        global $g_uid;
        $topicBlock = new TopicBlock();
        $res = $topicBlock->unread($g_uid);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    /**
     * @api {post} /api/0.0.1/topic/unread/list 未读信息列表
     * @apiName TopicUnreadList
     * @apiGroup 动态
     * @apiVersion 0.0.1
     * @apiDescription 未读信息列表
     * @apiParam {String} token    用户登陆标志
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [{
     *                  'id' : 1                         //动态ID
     *                  'uid'      : 1,                  //用户ID
     *                  'nick_name': '张三',              //用户昵称
     *                  'content'  : 'xxxxx',            //动态内容
     *                  'created_at': 'xxx'              //发布时间
     *                  'imgs'     : [                   //图片
     *                      {
     *                          'id' : 1,                       //图片ID
     *                          'imgurl' : 'http://xxxx.png',   //图片地址
     *                      },
     *                      ...
     *                  ]
     *                  comment_content: [                  //评论内容数据
     *                      {
     *                          id :'xx'                    //评论ID
     *                          topic_id: 1,                //动态ID
     *                          user_id: 22,                //评论用户ID
     *                          content: "写的挺好的！",      //评论内容
     *                          created_at: "2019-01-16 15:03:10",  //评论时间
     *                          updated_at: "2019-01-16 15:06:47",
     *                          deleted_at: null,
     *                          nick_name: "H@jh"           //评论用户
     *                      },
     *                      ...
     *                  ],
     *                  UsefulCentent: [                    //点赞数据
     *                      {
     *                          topic_id: 1,                //动态ID
     *                          user_id: 22,                //点赞用户ID
     *                          updated_at: "2019-01-16 15:24:16",
     *                          nick_name: "H@jh"           //点赞用户
     *                      },
     *                      ...
     *                  ]
     *          }...]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function unreadList(){
        global $g_uid;
        $topicBlock = new TopicBlock();
        $res = $topicBlock->unreadList($g_uid);
        if ($res===[] || $res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}