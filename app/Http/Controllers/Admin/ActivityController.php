<?php
namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Blocks\ActivityDataBlock;
use Illuminate\Http\Request;
use App\Libs\ResponseMessage;
class ActivityController extends Controller{

    //活动列表
    public function activityList(Request $request)
    {
        $page = $request->get('page', 1);
        $pageSize = $request->get('pageSize', 5);

        $search=[];
        $search['crowd_id'] = $request->get('crowd_id');
        $search['title'] = $request->get('title');

        $activityBlock = new ActivityDataBlock();
        $res = $activityBlock->activityList($page, $pageSize, $search);
        if ($res) {
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    //修改活动
    public function activityUpdate(Request $request){
        if (!$request->post('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $isPost = ['address','end_time','start_time','imgurl','introduce','take_num','title','charge'];
        foreach ($isPost as $item) {
            if ($request->has($item)){
                $data[$item] = $request->post($item);
            }
        }
        $data['charge'] = $data['charge'] * 100;
        $id = $request->post('id');
        $activityBlock = new ActivityDataBlock();
        $result = $activityBlock->activityUpdate($id,$data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    //删除活动
    public function delActivity(Request $request){
        if (!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->post('id');
        $activityBlock = new ActivityDataBlock();
        $result = $activityBlock->delActivity($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    //添加活动
    public function addActivity(Request $request){
        $isPost = ['crowd_id','title','charge','address','introduce','imgurl','take_num','start_time','end_time'];
        foreach ($isPost as $item) {
            if ($request->has($item)){
                $data[$item] = $request->post($item);
            }
        }
        $data['charge'] = $data['charge'] * 100;
        $data['introduce'] = htmlspecialchars($data['introduce'],true);
        $activityBlock = new ActivityDataBlock();
        $result = $activityBlock->addActivity($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success($result)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }
}