<?php

namespace App\Http\Controllers\Admin;

use Aliyun\Api\Sms\Request\V20170525\SendSmsRequest;
use App\Blocks\TopicDataBlock;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;

class TopicDataController
{


    /**
     * @api {post} /admin/topic/topstick 动态列表
     * @apiName topic_crowd_list
     * @apiGroup 后台-动态
     * @apiVersion 0.0.1
     * @apiDescription 动态列表
     * @apiParam {Number} [title]       动态标题
     * @apiParam {Number} [page=1]      当前页数
     * @apiParam {Number} [pagesize=5]  每页显示数量
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": [
     *          "list":{                    //动态列表
     *              "id": xx,                           //动态ID
     *              "crowd_id"：xx                       //群ID
     *              "name": xxx,                        //动态名称
     *              "nick_name": "xxxx",                //动态发布人
     *              "content": "xxx",                   //动态内容
     *              "useful": xx,                       //点赞数量
     *              "comment": xx,                      //评论数量
     *              "imgurl": "xxxx",                   //动态图片
     *              "created_at": "xx-xx-xx xx:xx:x"    //动态发布时间
     *          }
     *          "pagination": {              //分页列表
     *               "total": 15,                       //数据总数
     *               "pagesize": 5,                     //页面大小
     *               "current": 1                       //当前位置
     *          }
     *          "crowd_data": [              //群数据列表
     *              {
     *                   "name": "xxxx",                //群名称
     *                   "id": xx                       //群ID
     *               },
     *          ]
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function topicStick(Request $request){

            $topic = new TopicDataBlock();
            $data['id'] = $request->post('id');//动态ID
            $data['content'] = $request->post('title');
            $data['page'] = $request->post('page');//当前页数
            $data['pagesize'] = $request->post('pagesize');//当前页数大小
            $res = $topic->topic_stick($data);
            if ($res){
                return response()->json(ResponseMessage::getInstance()->success($res)->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }



    }


    /**
     * @api {post} /admin/topic/deltopic 删除动态
     * @apiName topic_del
     * @apiGroup 后台-动态
     * @apiVersion 0.0.1
     * @apiDescription 删除动态
     * @apiParam {Number} id 动态ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function topicDel(Request $request)
    {
        if($request->has('id')){
            $topic = new TopicDataBlock();
            $id = $request->post('id');
            $res = $topic->topic_del($id);
            if ($res){
                return response()->json(ResponseMessage::getInstance()->success()->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed($res)->response());
            }
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
    }


    /**
     * @api {post} /admin/testadmin/delclass 删除群分类
     * @apiName delClassify
     * @apiGroup 群操作
     * @apiVersion 0.0.1
     * @apiDescription 删除群分类
     * @apiParam {Number} id 分类ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : {
     *      }
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'FAILED',
     *      'failedMsg' : '操作失败',
     *      'data'  : []
     * }
     */
    public function delClassify(Request $request){
        if ($request->has('id')){
            $TopicDataBlockModel = new TopicDataBlock();
            $id = $request->get('id');
            $res = $TopicDataBlockModel->delcrowdClassfity($id);
            if ($res){
                return response()->json(ResponseMessage::getInstance()->success()->response());
            }else{
                return response()->jso(ResponseMessage::getInstance()->failed()->response());
            }
        }else{
            return response()->jso(ResponseMessage::getInstance()->failed('RARAM_ERROR')->response());
        }

    }

}