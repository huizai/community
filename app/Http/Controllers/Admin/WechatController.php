<?php

namespace App\Http\Controllers\Admin;

use App\Libs\ResponseMessage;
use App\Blocks\wechatBlock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use think\Response;

class WechatController extends Controller
{

    /**
     * @api {post} /admin/testadmin/weconf 微信配置信息
     * @apiName getWe_config
     * @apiGroup 系统
     * @apiVersion 0.0.1
     * @apiDescription 微信配置信息
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": [
     *          "id": 1,          //微信ID
     *          "name": "xx",     //公众号名称名称
     *          "base_url": "xx",       //动态发布人
     *          "type": 1,    //公众号类型 1：订阅号
     *          "app_id": "xxxxx",  //微信app_id
     *          "secret": "xxxxx",  //微信密匙
     *          "token":  "xxxxx",  //微信token
     *          "aes_key" "xxxxx".  //微信aes_key
     *          "update_time": "2018-12-05 08:53:39"  //修改时间
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function getWe_config(Request $request)
    {
        $data['name'] = trim($request->get('name'));
        $data['we_type'] = $request->get('we_type');
        $wechatModel = new wechatBlock();
        $res = $wechatModel->we_config($data);
        return response()->json(ResponseMessage::getInstance()->success($res)->response());


    }

    public function We_configAdd(Request $request){
        $data['name'] = trim($request->post('name'));
        $data['base_url'] = trim($request->post('base_url'));
        $data['we_type'] = (int)$request->post('we_type');
        $data['app_id'] = trim($request->post('app_id'));
        $data['secret'] = trim($request->post('secret'));
        $data['token'] = trim($request->post('token'));
        $data['aes_key'] = trim($request->post('aes_key'));
        $wechatmodel = new WechatBlock();
        $num = 0;
        foreach ($data as $value) {
            if (!isset($value)){
                $num = $num+1;
            }
        }
        if ($num>0){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $res = $wechatmodel->wechat_add($data);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


    //修改微信配置
    public function We_Update(Request $request){
        if ($request->has('id')){
            $id = $request->post('id');
            $data['name'] = $request->post('name');
            $data['base_url'] = $request->post('base_url');
            $data['we_type'] = $request->post('we_type');
            $data['app_id'] = $request->post('app_id');
            $data['secret'] = $request->post('secret');
            $data['token'] = $request->post('token');
            $data['aes_key'] = $request->post('aes_key');
            foreach ($data as $value){
                if ($value==''){
                    return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
                }
            }

            $wechatModel= new wechatBlock();
            $res = $wechatModel->we_update($id,$data);
            if ($res){
                return response()->json(ResponseMessage::getInstance()->success()->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
    }


    //删除微信配置
    public function Weconfig_delete(Request $request){
        if ($request->has('id')){
            $id = $request->post('id');
            $WechatConfigModel = new wechatBlock();
            $res = $WechatConfigModel->weconfig_delete($id);
            if ($res){
                return response()->json(ResponseMessage::getInstance()->success()->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
    }

    //微信用户列表
    /**
     * @api {post} /admin/testadmin/weuser 微信用户列表
     * @apiName getWe_user
     * @apiGroup 系统
     * @apiVersion 0.0.1
     * @apiDescription 微信用户列表
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": [
     *          "id": 1,          //微信用户ID
     *          "wechat_id": "1"       //微信ID
     *          "openid": "xxxxx",     //微信用户openid
     *          "nick_name": "xxxxxx",   //微信名称
     *          "city": "xxxxx",    //所在城市
     *          "province": "xxxxx",  //所在省份
     *          "country": "xxxxx",  //所在国家
     *          "sex":  "xxxxx",  //性别
     *          "headimgurl": "xxxxx".  //微信头像
     *          "subscribe": "1"    //是否关注 1：关注
     *          "subscribe_time": "2018-12-05 08:53:39"  //关注时间
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function getWe_user(Request $request)
    {
        $subscribe = $request->get('subscribe');
        $page = (int)$request->get('page');
        $pagesize = (int)$request->get('pagesize');
        $wechatModel = new wechatBlock();
        $res = $wechatModel->we_user($subscribe,$page,$pagesize);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return resposne()->json(ResponseMessage::getInstance()->failed()->response());
        }


    }


}