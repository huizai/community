<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Blocks\GoodsBlock;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;

date_default_timezone_set('PRC');

class GoodsController extends Controller
{

    /**
     * @api {get} /admin/goods/list 商品列表
     * @apiName goods_list
     * @apiGroup 后台-商品
     * @apiVersion 0.0.1
     * @apiDescription 商品列表
     * @apiParam {String} [sorter] 根据名字搜索
     * @apiParam {Number} [currentPage=1] 当前页
     * @apiParam {Number} [pageSize=5] 单页数量
     * @apiParam {String} [unit] 规格
     * @apiParam {String} [title] 标题
     * @apiParam {String} [producing_area] 产地
     * @apiParam {String} [name] 商品名字
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *              'list' : {
     *                  'key' : xx,                          //商品ID
     *                  'show_title' : xx,                   //标题
     *                  'name' : 'xx',                       //商品名称
     *                  'small_image' : 'xx',                //商品列表图
     *                  'producing_area' : 'xx',             //产地
     *                  'unit': xx,                          //规格
     *                  'stock_num' : 'xx',                  //群标签ID
     *                  'slogan' : xx,                       //商品广告语
     *                  'illustrate' : xx,                   //商品图片
     *                  'now_price' : xx,                    //群商品数量
     *                  'out_price': xx,                     //进去是否收费 1不收费，0收费
     *                  'in_price': xx,                      //是否是私密群 1是，0不是
     *                  'salenum': 'xx',                     //分类名称
     *                  'status' : 'xx',                     //群主昵称
     *                  'on_off_time':'xx'                   //创建时间
     *                  'created_at' : xx,                   //群商品数量
     *                  'b_id': x,                           //品牌ID
     *                  'c_id': xx,                          //分类ID
     *                  'd_id': 'x',                         //群ID
     *                  'crowd_name' : 'xx',                 //群主昵称
     *                  'img_url':'xx'                       //商品轮播图
     *              },
     *              'pagination' : {            //分页数据
     *                  'total' : xx,                       //数据总数
     *                  'pageSize' : xx,                    //页面大小
     *                  'current' : xx                      //当前页
     *              }
     *              "crowd_data": [             //群数据
     *                   {
     *                       "name": "xx",                  //群名称
     *                       "id": xx                       //群ID
     *                   },
     *               ],
     *              "brand_data": [             //品牌数据
     *                   {
     *                       "id": xx,                      //品牌ID
     *                       "c_id": xx,                    //品牌分类ID
     *                       "name": "xx"                   //品牌名称
     *                   },
     *              ]
     *              "treeData": [               //分类数据
     *                   {
     *                       "title": "xx",     //分类名
     *                       "key": xx,         //分类ID
     *                       "level": xx,       //分类级别
     *                       "value": xx,       //分类ID
     *                       "father_id": xx,   //分类上级ID
     *                       "children": [      //下级分类分支
     *                           {
     *                               "title": "xx",
     *                               "key": xx,
     *                               "level": xx,
     *                               "value": xx,
     *                               "father_id": xx
     *                           },
     *                      ]
     *                  }
     *              ]
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function goodsList(Request $request)
    {

        $data = array();
        $order = $request->get('sorter');
        $pagination['page'] = $request->get('currentPage');
        $pagination['pagesize'] = $request->get('pageSize');
        $data['unit'] = $request->get('unit');
        $data['title'] = trim($request->get('title'));
        $data['producing_area'] = trim($request->get('producing_area'));
        $data['status'] = $request->get('status');
        $data['name'] = trim($request->get('name'));
        $goodmodel = new GoodsBlock();

        $res = $goodmodel->Goods_list($data, $order, $pagination);
        if ($res) {
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }


    }

    /**
     * @api {post} /admin/goods/add 商品添加
     * @apiName goods_add
     * @apiGroup 后台-商品
     * @apiVersion 0.0.1
     * @apiDescription 商品添加
     * @apiParam {Number} crowd_id 群ID
     * @apiParam {String} show_title 标题
     * @apiParam {String} name 商品名称
     * @apiParam {String} producing_area 产地
     * @apiParam {String} slogan 广告语
     * @apiParam {String} unit 规格
     * @apiParam {Number} c_id 分类ID
     * @apiParam {String} illustrate 详细介绍
     * @apiParam {Number} now_price 现价
     * @apiParam {Number} out_price 售价
     * @apiParam {Number} in_price 进价
     * @apiParam {Number} status 是否上架
     * @apiParam {Number} b_id 品牌ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : []
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function goodsAdd(Request $request)
    {
        $goodsPost = [
            'b_id','c_id','crowd_id','in_price','name','now_price','out_price',
            'producing_area','show_title','slogan','status','unit','small_image'
        ];
        foreach ($goodsPost as $item) {
            if (!$request->has($item)){
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
            $goodsData[$item] = $request->post($item);
        }
        $illustrate = $request->post('illustrate');
        $goodsData['illustrate'] = implode(',',$illustrate);
        $goodsData['in_price'] = $goodsData['in_price'] * 100;
        $goodsData['out_price'] = $goodsData['out_price'] * 100;
        $goodsData['now_price'] = $goodsData['now_price'] * 100;
        $goodsImgUrl = $request->post('img_url');

        $goodsModel = new GoodsBlock();
        $res = $goodsModel->goods_add($goodsData,$goodsImgUrl);

        if ($res) {
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }

    /**
     * @api {post} /admin/goods/del 商品删除
     * @apiName goods_del
     * @apiGroup 后台-商品
     * @apiVersion 0.0.1
     * @apiDescription 商品删除
     * @apiParam {Number} id 商品ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function goodsDel(Request $request)
    {
        if ($request->has('id')) {
            $id = $request->post('id');
            $goodmodel = new GoodsBlock();
            $res = $goodmodel->good_del($id);
            if ($res) {
                return response()->json(ResponseMessage::getInstance()->success()->response());
            } else {
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }

        } else {
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

    }

    /**
     * @api {post} /admin/goods/remove 商品修改
     * @apiName goods_remove
     * @apiGroup 后台-商品
     * @apiVersion 0.0.1
     * @apiDescription 商品修改
     * @apiParam {Number} [key] 商品ID
     * @apiParam {Number} [show_title] 商品标题
     * @apiParam {Number} [name] 商品名称
     * @apiParam {Number} [producing_area] 商品产地
     * @apiParam {Number} [slogan] 商品广告语
     * @apiParam {Number} [unit]  商品规格
     * @apiParam {Number} [c_id] 分类ID
     * @apiParam {Number} [illustrate] 商品介绍
     * @apiParam {Number} [now_price] 商品现价
     * @apiParam {Number} [out_price] 商品售价
     * @apiParam {Number} [in_price] 商品进价
     * @apiParam {Number} [status] 是否上架 1:上架 , 0:下架
     * @apiParam {Number} [b_id] 品牌ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function goodsUpdate(Request $request)
    {
        $goodsPost = [
            'b_id','c_id','key','in_price','name','now_price','out_price','inventory',
            'producing_area','show_title','slogan','status','unit','small_image'
        ];
        foreach ($goodsPost as $item) {
            if (!$request->has($item)){
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
            if ($item=='key'){
                $goodsData['id'] = $request->post($item);
            }else{
                $goodsData[$item] = $request->post($item);
            }

        }
        $illustrate = $request->post('illustrate');
        $goodsData['illustrate'] = implode(',',$illustrate);
        $goodsData['in_price'] = $goodsData['in_price'] * 100;
        $goodsData['now_price'] = $goodsData['now_price'] * 100;
        $goodsData['out_price'] = $goodsData['out_price'] * 100;
        $ImgUrl = $request->post('img_url');
        $ImgUrlData =  [];
        foreach ($ImgUrl as $k => $value) {
            $ImgUrlData[$k]['img_url'] = $value;
            $ImgUrlData[$k]['g_id'] = $goodsData['id'];
            $ImgUrlData[$k]['created_at'] = date('Y-m-d H:i:s',time());
        }

        $goodsModel = new GoodsBlock();
        $res = $goodsModel->good_update($goodsData,$ImgUrlData);

        if ($res) {
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed($res)->response());
        }
    }

    /**
     * @api {get} /admin/brand/list 品牌列表
     * @apiName brand_list
     * @apiGroup 后台-品牌
     * @apiVersion 0.0.1
     * @apiDescription 品牌列表
     * @apiParam {Number} [sorter] 排序
     * @apiParam {Number} [currentPage] 分页当前页
     * @apiParam {Number} [pageSize] 页面大小
     * @apiParam {Number} [name] 品牌名称
     * @apiParam {Number} [c_id] 品牌分类ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *              'list' : {
     *                  'key' : xx,                          //品牌ID
     *                  'name' : xx,                         //品牌名称
     *                  'c_name' : 'xx',                     //所属分类名称
     *                  'created_at' : 'xx',                //创建时间
     *                  'c_id' : 'xx',                      //所属分类ID
     *              },
     *              'pagination' : {            //分页数据
     *                  'total' : xx,                       //数据总数
     *                  'pageSize' : xx,                    //页面大小
     *                  'current' : xx                      //当前页
     *              }
     *              "treeData": [               //分类数据
     *                   {
     *                       "title": "xx",     //分类名
     *                       "key": xx,         //分类ID
     *                       "level": xx,       //分类级别
     *                       "value": xx,       //分类ID
     *                       "father_id": xx,   //分类上级ID
     *                       "children": [      //下级分类分支
     *                           {
     *                               "title": "xx",
     *                               "key": xx,
     *                               "level": xx,
     *                               "value": xx,
     *                               "father_id": xx
     *                           },
     *                      ]
     *                  }
     *              ]
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function brandList(Request $request)
    {
        $order = $request->get('sorter');
        $pagination['page'] = $request->get('currentPage');
        $pagination['pagesize'] = $request->get('pageSize');
        $data['name'] = $request->get('name');
        $data['c_id'] = $request->get('c_id');
        $goodmodel = new GoodsBlock();
        $res = $goodmodel->brand_list($data, $order, $pagination);

        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }

    /**
     * @api {post} /admin/brand/del 品牌删除
     * @apiName brand_del
     * @apiGroup 后台-品牌
     * @apiVersion 0.0.1
     * @apiDescription 品牌删除
     * @apiParam {Number} id 品牌ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function brandDel(Request $request)
    {
        if ($request->has('id')) {
            $id = $request->post('id');
            $brandmodel = new GoodsBlock();
            $res = $brandmodel->brand_del($id);
            if ($res) {
                return response()->json(ResponseMessage::getInstance()->success()->response());
            } else {
                return response()->json(ResponseMessage::getInstance()->failed()->rsepons());
            }
        } else {
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
    }


    /**
     * @api {post} /admin/brand/add 添加品牌
     * @apiName brand_add
     * @apiGroup 后台-品牌
     * @apiVersion 0.0.1
     * @apiDescription 添加品牌
     * @apiParam {Number} c_name 分类ID
     * @apiParam {Number} name  品牌名称
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function brandAdd(Request $request)
    {
        if ($request->has('name') && $request->has('c_name')) {
            $data['name'] = trim($request->post('name'));
            $data['c_id'] = (int)$request->post('c_name');
            $data['created_at'] = date('Y-m-d H:i:s', time());
            $brandmodel = new GoodsBlock();
            $res = $brandmodel->brand_Add($data);
            if ($res) {
                return response()->json(ResponseMessage::getInstance()->success()->response());
            } else {
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        } else {
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

    }

    /**
     * @api {post} /admin/brand/remove 品牌修改
     * @apiName brand_remove
     * @apiGroup 后台-品牌
     * @apiVersion 0.0.1
     * @apiDescription 品牌修改
     * @apiParam {Number} id 品牌ID
     * @apiParam {Number} [c_name] 分类ID
     * @apiParam {Number} [name] 品牌名称
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function brandRemove(Request $request)
    {
        if ($request->has('id')) {
            $id = $request->post('id');
            $data['name'] = $request->post('name');
            $data['c_id'] = $request->post('c_id');
            $brandmodel = new GoodsBlock();
            $res = $brandmodel->brand_update($id, $data);
            if ($res) {
                return response()->json(ResponseMessage::getInstance()->success()->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        } else {
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
    }


    /**
     * @api {post} /admin/cate/list 分类列表
     * @apiName cate_list
     * @apiGroup 后台-分类
     * @apiVersion 0.0.1
     * @apiDescription 分类列表
     * @apiParam {Number} [currentPage] 当前页
     * @apiParam {Number} [pageSize] 页面大小
     * @apiParam {Number} [name] 分类名称
     * @apiParam {Number} [sorter] 排序
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : [
     *              'list' : {
     *                  'key' : xx,                          //分类ID
     *                  'name' : xx,                         //分类名称
     *                  'level' : 'xx',                     //分类等级
     *                  ‘father_id’                         //上级ID
     *                  'created_at' : 'xx',                //创建时间
     *                  'father_name' : 'xx',               //上级名称
     *              },
     *              'pagination' : {            //分页数据
     *                  'total' : xx,                       //数据总数
     *                  'pageSize' : xx,                    //页面大小
     *                  'current' : xx                      //当前页
     *              }
     *              "treeData": [               //分类数据
     *                   {
     *                       "title": "xx",     //分类名
     *                       "key": xx,         //分类ID
     *                       "level": xx,       //分类级别
     *                       "value": xx,       //分类ID
     *                       "father_id": xx,   //分类上级ID
     *                       "children": [      //下级分类分支
     *                           {
     *                               "title": "xx",
     *                               "key": xx,
     *                               "level": xx,
     *                               "value": xx,
     *                               "father_id": xx
     *                           },
     *                      ]
     *                  }
     *              ]
     *      ]
     * }
     */
    public function categoriesList(Request $request){
        $pagination['page'] = $request->get('currentPage');
        $pagination['pagesize'] = $request->get('pageSize');
        $data['name'] = trim($request->get('name'));
        $order = $request->get('sorter');
        $catmodel = new GoodsBlock();
        $res = $catmodel->cat_list($data,$pagination,$order);
        return response()->json(ResponseMessage::getInstance()->success($res)->response());
    }


    /**
     * @api {post} /admin/cate/del 分类删除
     * @apiName cate_del
     * @apiGroup 后台-分类
     * @apiVersion 0.0.1
     * @apiDescription 分类删除
     * @apiParam {Number} id 分类ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function categoriesDel(Request $request){
        if ($request->has('id')){
            $id = $request->post('id');
            $catemodel = new GoodsBlock();
            $res = $catemodel->cate_del($id);
            if ($res){
                return response()->json(ResponseMessage::getInstance()->success()->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
    }

    /**
     * @api {post} /admin/cate/remove 分类修改
     * @apiName cate_remove
     * @apiGroup 后台-分类
     * @apiVersion 0.0.1
     * @apiDescription 分类修改
     * @apiParam {Number} key              分类ID
     * @apiParam {String} name             分类名称
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function categoriesRemove(Request $request){
        if ($request->has('key')){
            $id = (int)$request->post('key');
            $data['name'] = trim($request->post('name'));
            $catemodel = new GoodsBlock();
            $res = $catemodel->cate_update($id,$data);

            if ($res){
                return response()->json(ResponseMessage::getInstance()->success()->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
    }


    /**
     * @api {post} /admin/cate/add 添加分类
     * @apiName cate_add
     * @apiGroup 后台-分类
     * @apiVersion 0.0.1
     * @apiDescription 添加分类
     * @apiParam {Number} key              分类ID
     * @apiParam {String} name             分类名称
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function categoriesAdd(Request $request){
        $catemodel = new GoodsBlock();
        if (!$request->has('name') || !$request->has('c_id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $data['name'] = trim($request->post('name'));
        $data['c_id'] = (int)$request->post('c_id');
        $res = $catemodel->cate_insert($data);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success($res)->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }

}