<?php

namespace App\Http\Controllers\Admin;
use App\Blocks\CrowdBlock;
use App\Blocks\CrowdDataBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;

class CrowdController extends Controller {

    /**
     * @api {get} /admin/crowd/list 获取群列表
     * @apiName getCrowd
     * @apiPrivate
     * @apiGroup 群操作
     * @apiVersion 0.0.1
     * @apiDescription 获取群列表
     * @apiParam {Number} [p_cid] 一级分类id
     * @apiParam {String} [search] 根据名字搜索
     * @apiParam {Number} [page=1] 分页
     * @apiParam {Number} [pageSize=20] 单页数量
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *              'list' : {
     *                  'id' : 1,                        //群ID
     *                  'uid' : '0',                     //群主ID
     *                  'name' : '汽车',                  //群名称
     *                  'introduce' : '群简介',           //群简介
     *                  'headimgurl' : 'http://xxxxx',   //群列表小图,
     *                  'classify_id': 2,                //群分类ID 分类的二级id
     *                  'tag_ids' : '1,2,3,4,5',         //群标签ID
     *                  'grade' : 12,                    //群等级,
     *                  'member' : 234,                  //群成员数量,
     *                  'goods' : 4343,                  //群商品数量,
     *                  'is_free': 1,                    //进去是否收费 1不收费，0收费
     *                  'is_private': 1,                 //是否是私密群 1是，0不是
     *                  'classify_name': '分类',          //分类名称,
     *                  'nick_name' : '辉仔',             //群主昵称,
     *                  'created_at':'2018:10:11'        //创建时间
     *              },
     *              'pagination' : {
     *                  'total' : 20,
     *                  'pageSize' : 2,
     *                  'current' : 2
     *              }
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function getCrowd(Request $request){
        $page = (int)$request->get('page', 1);
        $pageSize = (int)$request->get('pageSize', 5);
        $free = (int)$request->get('is_free');
        $name = $request->post('name');

        $crowdBlock = new CrowdDataBlock();
        $list = $crowdBlock->crowdList($page, $pageSize, $free, $name);

        return response()->json(ResponseMessage::getInstance()->success($list)->response());
    }


    //删除群
    public function delCrowd(Request $request){
        if ($request->has('id')){
            $id = (int)$id = $request->post('id');
            $crowd = new CrowdBlock();
            $res = $crowd->del_crowd($id);
            if ($res){
                return response()->json(ResponseMessage::getInstance()->success()->response());
            }else{
                return response()->json(ResponseMessage::getInstance()->failed()->response());
            }
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR' )->response());
        }
    }

    public function crowdUpdate(Request $request){
        $id = $request->post('id');
        $is_post = ['charge','classify_id','detailed','headimgurl','introduce','is_free','is_private','name'];
        foreach ($is_post as $post){
            if ($request->filled($post)){
                $data[$post] = $request->post($post);
            }
        }
        $data['detailed'] = htmlspecialchars($data['detailed'],true);
        $crowdBlock = new CrowdDataBlock();
        $res = $crowdBlock->crowdUpdate($id,$data);

        if ($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    //群分类列表
    public function crowdClass(){

        $crowdBlock = new CrowdDataBlock();
        $list = $crowdBlock->crowdClass();
        return response()->json(ResponseMessage::getInstance()->success($list)->response());
    }


    //删除分类
    public function crowdClassRemove(Request $request)
    {
        if (!$request->has('key')) {
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = (int)$request->post('key');
        $crowdBlock = new CrowdDataBlock();
        $res = $crowdBlock->crowdClassRemove($id);

        if ($res) {
            return response()->json(ResponseMessage::getInstance()->success('删除成功')->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    public function crowdClassUpdate(Request $request){
        if (!$request->has(['key','pid','title'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = (int)$request->post('key');
        $pid = (int)$request->post('pid');
        $name = $request->post('title');
        $crowdBlock = new CrowdDataBlock();
        $res = $crowdBlock->crowdClassUpdate($id,$pid,$name);

        if ($res) {
            return response()->json(ResponseMessage::getInstance()->success('修改成功')->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

    public function crowdClassAdd(Request $request){
        if (!$request->has(['pid','title'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $data['pid'] = (int)$request->post('pid');
        $data['name'] = $request->post('title');
        $crowdBlock = new CrowdDataBlock();
        $res = $crowdBlock->crowdClassAdd($data);

        if ($res) {
            return response()->json(ResponseMessage::getInstance()->success('添加成功')->response());
        } else {
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }

}