<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use App\Models\SystemInformation;
use Illuminate\Http\Request;
use App\Libs\Jiguang\Jpush;


class SystemController extends Controller{


    /**
     * 后台-消息设置列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function informationList(Request $request){
        $page = $request->post('currentPage',1);
        $pageSize = $request->post('pageSize',10);
        $title = $request->post('title','');

        $systemInformationModel = new SystemInformation();
        $data = $systemInformationModel->informationList($page, $pageSize, $title);

        return response()->json(ResponseMessage::getInstance()->success($data)->response());
    }

    /**
     * 后台-添加系统消息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function informationAdd(Request $request){
        if (!$request->has(['content','title','img_url','cover_content'])){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $data['content'] = htmlspecialchars(trim($request->post('content')),true);
        $data['title'] = trim($request->post('title'));
        $data['updated_at'] = date('Y-m-d H:i:s',time());
        $data['img_url'] = trim($request->post('img_url'));
        $data['cover_content'] = trim($request->post('cover_content'));
        $data['type'] = 1;

        $systemInformationModel = new SystemInformation();
        $result = $systemInformationModel->informationAdd($data);

        if ($result){
            $push = new Jpush();
            $push->pushInformation();
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
    }

    /**
     * 后台-修改系统消息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function informationUpdate(Request $request){
        if (!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $isPost = ['content','img_url','title','cover_content'];

        foreach ($isPost as $item) {
            if ($request->has($item)){
                $data[$item] = trim($request->post($item));
            }
        }

        $data['id'] = (int)$request->post('id');
        $data['content'] = htmlspecialchars($data['content'],true);
        $data['updated_at'] = date('Y-m-d H:i:s',time());

        $systemInformationModel = new SystemInformation();
        $result = $systemInformationModel->informationUpdate($data);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
    }

    /**
     * 后台-删除系统消息
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function informationRemove(Request $request){

        if (!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = (int)$request->post('id');

        $systemInformationModel = new SystemInformation();
        $result = $systemInformationModel->informationRemove($id);

        if ($result){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }

        return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
    }
}