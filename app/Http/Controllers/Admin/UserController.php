<?php

namespace App\Http\Controllers\Admin;
use App\Blocks\UserBlock;
use App\Http\Controllers\Controller;
use App\Libs\ResponseMessage;
use Illuminate\Http\Request;

class UserController extends Controller {
    /**
     * @api {get} /admin/appuser/cert 实名列表
     * @apiName appUserCert
     * @apiPrivate
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 实名列表
     * @apiParam {String} [search] 根据名字搜索
     * @apiParam {Number} [page=1] 分页
     * @apiParam {Number} [pageSize=20] 单页数量
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *          {
     *              'id' : 1,                               //用户ID
     *              'mobile' : '18800001111',               //用户手机号
     *              'real_name' : '张三',                    //用户昵称
     *              'status'    : 1,                        //0未审核 1通过 2不通过
     *              'id_card'   : 'xxxx'                    //身份证号
     *              'id_card_img_left' : 'https://xxx.png'  //身份证正面
     *              'id_card_img_right' : 'https://xxx.png' //身份证反面
     *          }
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */

    public function getUserCert(Request $request){
        $page = (int)$request->get('page', 1);
        $pageSize = (int)$request->get('pageSize', 5);

        $search = [];
        $searchField = ['real_name', 'mobile'];
        foreach ($searchField as $field){
            if($request->has($field)){
                $search[$field] = $request->get($field);
            }
        }

        $userBlock = new UserBlock();
        $list = $userBlock->userCertList($search, $page, $pageSize);
        $pagination = $userBlock->userCertListPagination($search, $page, $pageSize);

        return response()->json(ResponseMessage::getInstance()->success(['list' => $list, 'pagination' => $pagination])->response());

    }

    /**
     * @api {post} /admin/appuser/cert/update/status 更新实名状态
     * @apiName updateUserCertStatus
     * @apiPrivate
     * @apiGroup 用户
     * @apiVersion 0.0.1
     * @apiDescription 更新实名状态
     * @apiParam {Number} id ID
     * @apiParam {Number=1,2} status 状态
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      'data'  : [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */

    public function updateUserCertStatus(Request $request){
        if(!$request->has('id') || !$request->has('status')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $id = (int)$request->get('id');
        $status = (int)$request->get('status');
        if(!in_array($status, [1,2])){
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

        $userBlock = new UserBlock();
        if($userBlock->updateUserCertStatus($id, $status)){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }
    }


}