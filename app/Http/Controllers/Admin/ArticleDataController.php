<?php
namespace App\Http\Controllers\Admin;

use App\Blocks\ArticleDataBlock;
use App\Libs\ResponseMessage;
use App\Http\Controllers\Controller;
use http\Env\Response;
use Illuminate\Http\Request;

class ArticleDataController extends Controller{



    /**
     * @api {GET} /admin/article/article 获取群文章列表
     * @apiName getArticle
     * @apiGroup 后台-文章
     * @apiVersion 0.0.1
     * @apiDescription 获取群文章信息
     * @apiParam {Number} crowd_id 群ID
     * @apiParam {Number} order 排序数,越大排越前，必须和id同时存在或者同时不存在
     * @apiParam {Number} page 分页当前页数，默认为第一页
     * @apiParam {Number} pagesize 分页当前页数大小，默认为每页20条数据
     * @apiParam {String} title 文章标题
     * @apiParam {Number} crowd_id 群ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": [
     *          "id": 2,                   //文章ID
     *          "crowd_id": 1,             //群ID
     *          "uid": 1,                  //用户ID
     *          "is_recommend": 1,        //是否推荐，0不推荐
     *          "title": "xxxxxxxxxxxxx",  //文章标题
     *          "content": "xxxxxxxxxxx",  //文章内容
     *          "headimgurl": "xxxx.png",  //文章图片
     *          "useful": 0,                //赞的数量
     *          "comment": 0,               //评论的数量
     *          "share": 0,                 //分享数量
     *          "views": 0,                 //浏览量
     *          "created_at": "2018-12-05 08:53:39"  //文章发布时间
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function getArticle(Request $request){
                $data['id'] = $request->get('id');//文章ID
                $data['title']= trim($request->get('title'));//文章标题
                $data['crowd_id'] = $request->get('crowd_id');//群ID
                $data['page'] = (int)$request->get('page');//当前页数
                $data['pagesize'] = (int)$request->get('pagesize');//页面数据大小
                $data['order'] = $request->get('order');//排序数
                $siteModel = new ArticleDataBlock();
                $res = $siteModel->get_Article($data);
                if ($res){
                    return response()->json(ResponseMessage::getInstance()->success($res)->response());
                }else{
                    return request()->json(ResponseMessage::getInstance()->failed()->response());
                }




    }

    /**
     * @api {post} /admin/article/savearticle 添加群文章信息
     * @apiName saveArticle
     * @apiGroup 后台-文章
     * @apiVersion 0.0.1
     * @apiDescription 添加群文章信息
     * @apiParam {Number} crowd_id 群ID
     * @apiParam {String} title 文章标题
     * @apiParam {String} content 文章内容
     * @apiParam {String} headimgurl 图片地址
     * @apiParam {Number} is_recommend 是否推荐，默认值：0(不推荐)
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function saveArticle(Request $request){
        $is_post = ['content','crowd_id','title','headimgurl','is_recommend','introduce'];
        foreach ($is_post as $item) {
            if ($request->filled($item)){
                $data[$item] = $request->post($item);
            }
        }
        $data['content'] = htmlspecialchars($data['content'],true);
        $data['created_at'] = date('Y-m-d H:iLs',time());
        $articleModel = new ArticleDataBlock();
        $res = $articleModel->save_Article($data);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed()->response());
        }

    }


    /**
     * @api {post} /admin/article/delarticle 删除群文章信息
     * @apiName delArticle
     * @apiGroup 后台-文章
     * @apiVersion 0.0.1
     * @apiDescription 删除群文章信息
     * @apiParam {Number} id 文章ID
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function delArticle(Request $request){
        if (!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }
        $id = $request->get('id');
        $siteModel = new ArticleDataBlock();
        $res = $siteModel->del_Article($id);
        if ($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
        }
    }


    //修改文章信息
    /**
     * @api {post} /admin/article/uparticle 修改文章信息
     * @apiName upArticle
     * @apiGroup 后台-文章
     * @apiVersion 0.0.1
     * @apiDescription 修改文章信息
     * @apiParam {Number} id 文章ID
     * @apiParam {Number} title 文章标题
     * @apiParam {String} is_recommend 是否推荐
     * @apiParam {Number} views 浏览次数
     * @apiParam {Number} share 分享次数
     * @apiParam {Number} useful 点赞数
     * @apiParam {Number} comment 评论数
     * @apiParam {Number} imgurl 文章图片地址
     * @apiParam {Number} content 文章内容
     * @apiSuccessExample {json} 操作成功响应示例
     * {
     *      'status' : 'success',
     *      'failedCode' : '',
     *      'failedMsg' : '',
     *      "data": [
     *      ]
     * }
     * @apiErrorExample {json} 操作失败响应示例
     * {
     *      'status' : 'failed',
     *      'failedCode' : 'ERROR CODE',
     *      'failedMsg' : 'ERROR MSG',
     *      'data'  : []
     * }
     */
    public function updateArticle(Request $request){
        if(!$request->has('id')){
            return response()->json(ResponseMessage::getInstance()->failed('PARAM_ERROR')->response());
        }

        $id = $request->post('id');
        $is_post = ['content','title','is_recommend','headimgurl'];
        foreach ($is_post as $post){
            if ($request->filled($post)){
                $data[$post] = $request->post($post);
            }
        }
        if (isset($data['content'])){
            $data['content'] = htmlspecialchars($data['content'],true);
        }



        $ArticleModel = new ArticleDataBlock();
        $res = $ArticleModel->up_Article($data,$id);

        if ($res){
            return response()->json(ResponseMessage::getInstance()->success()->response());
        }else{
            return response()->json(ResponseMessage::getInstance()->failed('FAILED')->response());
        }
    }

}