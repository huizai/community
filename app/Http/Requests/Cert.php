<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Cert extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'    => [
                'required',
                'regex:/^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,3,6,7,8]{1}\d{8}$|^18[\d]{9}$/',
                'unique:user_cert,mobile'
            ],
            'real_name' => 'required',
            'id_card' => [
                'required',
                'regex:/^\d{17}[0-9xX]$/'
            ],
            'id_card_img_left'  => 'required',
            'id_card_img_right' => 'required'
        ];
    }

    public function messages(){
        return [
            'mobile.required' => '必须填写手机号码',
            'mobile.regex' => '手机号码格式不正确',
            'mobile.unique' => '手机号码已存在',
            'real_name.required' => '必须填写真实姓名',
            'id_card_img_left.required' => '必须上传身份证正面',
            'id_card_img_right.required' => '必须上传身份证反面',
            'id_card.required' => '必须填写身份证号',
            'id_card.regex' => '身份证号格式不正确'
        ];
    }
}
