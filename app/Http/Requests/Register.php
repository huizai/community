<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Register extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile'    => [
                'required',
                'regex:/^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,3,6,7,8]{1}\d{8}$|^18[\d]{9}$/',
                'unique:users,mobile'
            ],
            'validate_code' => 'required',
            'password' => 'required|min:6',
        ];
    }

    public function messages(){
        return [
            'mobile.required' => '必须填写手机号码',
            'mobile.regex' => '手机号码格式不正确',
            'mobile.unique' => '手机号码已被注册',
            'validate_code.required' => '必须填写验证码',
            'password.required' => '必须填写密码',
            'password.min' => '密码长度不能少于6位'
        ];
    }

}
