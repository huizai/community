<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Crowd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'classify_id' => 'required',
            'headimgurl' => 'required',
            'address' => 'required',
            'lat' => 'required',
            'lon' => 'required',

        ];
    }

    public function messages(){
        return [
            'headimgurl.required' => '必须上传群头像',
            'name.required' => '必须填写群名称',
            'classify_id.required' => '必须选择分类',
            'address.required' => '必须填写地址',
            'lat.required' => '自动获取经纬度失败',
            'lon.required' => '自动获取经纬度失败'
        ];
    }
}
