<?php

namespace App\Http\Middleware;

use App\Libs\ResponseMessage;
use Closure;
use Config , Cookie;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request , Closure $next)
    {
        $token = Cookie::get(Config::get('session.sq_cookie'));
        if(!$token){
            $token = $request->get('token');
        }

        \Log::debug($token);

        global $g_uid;
//        global $g_wx_openid;

        if($token == 'test123'){
            $g_uid = 31;

            $response = $next($request);

            return $response;
        }else {
            $userInfo = \Cache::get($token);
        }

        if(!isset($userInfo->id)){
            return response()->json(ResponseMessage::getInstance()->failed('NO_LOGIN')->response());
        }

        \Cache::put($token, $userInfo, 24*60*365);

        $g_uid = $userInfo->id;
//        $g_wx_openid = $userInfo->wx_openid;

        $response = $next($request);

        return $response;
    }
}
