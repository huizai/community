<?php

namespace App\Http\Middleware;

use App\Libs\ResponseMessage;
use Closure;
use Config , Cookie;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request , Closure $next)
    {
        $token = Cookie::get(Config::get('session.sq_admin_cookie'));
        if(!$token){
            $token = $request->get('token');
        }

        global $g_admin_uid;

        if($token == 'test123'){
            $g_admin_uid = 1;

            $response = $next($request);

            return $response;
        }else {
            $userInfo = \Cache::get($token);
        }

        if(!isset($userInfo->id)){
            return response()->json(ResponseMessage::getInstance()->failed('NO_LOGIN')->response(), 401);
        }

        $g_admin_uid = $userInfo->id;

        $response = $next($request);

        return $response;
    }
}
