<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdActivityComment extends Model{

    protected $table = 'crowd_activity_comment';
    public $timestamps = false;
}
