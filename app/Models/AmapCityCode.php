<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmapCityCode extends Model{

    protected $table = 'amap_city_code';
    public $timestamps = false;
}
