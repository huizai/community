<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdCategories extends Model{

    protected $table = 'crowd_categories';
    public $timestamps = false;
}
