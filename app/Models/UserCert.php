<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCert extends Model{

    protected $table = 'user_cert';
    public $timestamps = false;

    public function saveData($data){
        return \DB::table($this->table)->insertGetId($data);
    }
}
