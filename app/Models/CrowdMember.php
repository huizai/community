<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdMember extends Model{

    protected $table = 'crowd_member';
    public $timestamps = false;
}
