<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdArticle extends Model{

    protected $table = 'crowd_article';
    public $timestamps = false;
}
