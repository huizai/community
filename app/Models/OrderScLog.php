<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderScLog extends Model{

    protected $table = 'order_sc_log';
    public $timestamps = false;
}
