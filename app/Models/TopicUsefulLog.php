<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TopicUsefulLog extends Model{

    protected $table = 'topic_useful_log';
    public $timestamps = false;

    protected $guarded=[];
}
