<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodCommentImage extends Model{

    protected $table = 'good_comment_image';
    public $timestamps = false;
}
