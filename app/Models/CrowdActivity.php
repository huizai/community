<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdActivity extends Model{

    protected $table = 'crowd_activity';
    public $timestamps = false;
}
