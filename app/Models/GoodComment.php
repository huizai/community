<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodComment extends Model{

    protected $table = 'good_comment';
    public $timestamps = false;
}
