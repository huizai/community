<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminPermissions extends Model{

    protected $table = 'admin_permissions';
    public $timestamps = false;
}
