<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminNodes extends Model{

    protected $table = 'admin_nodes';
    public $timestamps = false;
}
