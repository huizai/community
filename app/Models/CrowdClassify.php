<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdClassify extends Model{

    protected $table = 'crowd_classify';
    public $timestamps = false;
}
