<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminPermissionRole extends Model{

    protected $table = 'admin_permission_role';
    public $timestamps = false;
}
