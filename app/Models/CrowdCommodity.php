<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdCommodity extends Model{

    protected $table = 'crowd_commodity';
    public $timestamps = false;
}
