<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminUrls extends Model{

    protected $table = 'admin_urls';
    public $timestamps = false;
}
