<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TopicImgs extends Model{

    protected $table = 'topic_imgs';
    public $timestamps = false;
}
