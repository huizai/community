<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdFlowingWather extends Model{

    protected $table = 'crowd_flowing_wather';
    public $timestamps = false;
}
