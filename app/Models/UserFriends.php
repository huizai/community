<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFriends extends Model{

    protected $table = 'user_friends';
    public $timestamps = false;

    public function saveData($data){
        return \DB::table($this->table)->insertGetId($data);
    }
}
