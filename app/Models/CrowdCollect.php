<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdCollect extends Model{

    protected $table = 'crowd_collect';
    public $timestamps = false;
}
