<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BalanceCenter extends Model{

    protected $table = 'balance_center';
    public $timestamps = false;
}
