<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMoneyLog extends Model{

    protected $table = 'user_money_log';
    public $timestamps = false;
}
