<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdArticleShareLog extends Model{

    protected $table = 'crowd_article_share_log';
    public $timestamps = false;
}
