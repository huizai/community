<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WechatUser extends Model{

    protected $table = 'wechat_user';
    public $timestamps = false;
}
