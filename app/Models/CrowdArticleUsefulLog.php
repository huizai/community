<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdArticleUsefulLog extends Model{

    protected $table = 'crowd_article_useful_log';
    public $timestamps = false;
    protected $guarded = [];
}
