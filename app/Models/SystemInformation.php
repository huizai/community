<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class SystemInformation extends Model{

    protected $table = 'system_information';
    public $timestamps = false;


    /**
     * 后台-消息设置列表
     * @param $page
     * @param $pageSize
     * @param $title
     * @return array
     */
    public function informationList($page, $pageSize, $title){
            $sql = DB::table($this->table)
                ->whereNull('deleted_at');

            if (!empty($title)){
                $sql->where('title','like','%'.$title.'%');
            }

            $count = $sql->count();

            $list = $sql->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();

            foreach ($list as $item) {
                $item->content = htmlspecialchars_decode($item->content,true);
            }

            return [
                'list' => $list,
                'pagination' => [
                    'current' => $page,
                    'pageSize' => $pageSize,
                    'total' => $count
                ]
            ];
    }

    /**
     * 后台-添加系统消息
     * @param $data
     * @return mixed
     */
    public function informationAdd($data){
        return DB::table($this->table)->insert($data);
    }

    /**
     * 后台-修改系统消息
     * @param $data
     * @return mixed
     */
    public function informationUpdate($data){
        return DB::table($this->table)->where('id',$data['id'])->update($data);
    }

    /**
     * 后台-删除系统消息
     * @param $id
     * @return mixed
     */
    public function informationRemove($id){
        return DB::table($this->table)->where('id',$id)->update([
            'deleted_at' => date('Y-m-d H:i:s',time())
        ]);
    }

    /**
     * App-消息列表
     * @return mixed
     */
    public function informationTitleList($uid, $page, $pageSize){

        return DB::table($this->table)->select('id','title','created_at','type','img_url','cover_content')
            ->where('uid',$uid)
            ->orWhere('uid',0)
            ->orderBy('created_at','desc')
            ->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();


    }

    /**
     * App-消息详情
     * @param $id
     * @return mixed
     */
    public function informationInfo($id){
        $data =  DB::table($this->table)->where('id',$id)->first();
        $data->content = htmlspecialchars_decode($data->content,true);
        return $data;
    }
}
