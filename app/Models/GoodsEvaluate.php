<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsEvaluate extends Model{

    protected $table = 'goods_evaluate';
    public $timestamps = false;
}
