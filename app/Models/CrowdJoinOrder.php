<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdJoinOrder extends Model{

    protected $table = 'crowd_join_order';
    public $timestamps = false;
}
