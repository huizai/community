<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoodsImgs extends Model{

    protected $table = 'goods_imgs';
    public $timestamps = false;
}
