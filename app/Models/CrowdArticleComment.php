<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdArticleComment extends Model{

    protected $table = 'crowd_article_comment';
    public $timestamps = false;

    public function saveData($data){
        return \DB::table($this->table)->insert($data);
    }
}
