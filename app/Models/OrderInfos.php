<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderInfos extends Model{

    protected $table = 'order_infos';
    public $timestamps = false;
}
