<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrowdActivityCommentLog extends Model{

    protected $table = 'crowd_activity_comment_log';
    public $timestamps = false;
}
