<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TopicComment extends Model{

    protected $table = 'topic_comment';
    public $timestamps = false;

    public function saveData($data){
        return \DB::table($this->table)->insert($data);
    }
}
