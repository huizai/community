<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWithdrawDeposit extends Model{

    protected $table = 'user_withdraw_deposit';
    public $timestamps = false;
}
