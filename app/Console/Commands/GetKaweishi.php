<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GetKaweishi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:kaweishi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '获取卡卫士的数据';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tokenId = "EF00928F442740A989DA25EE1366768E";

        global $allMerchant;
        $this->getAllMerchant('邓帅','173878', '833E722017CE1DE6AE7D732509FE4444');
        $cellData = "名字\t手机号\t身份\t上级\t注册时间\t实名时间\t总分享人数\t快速消费\t快速还款\t总手续费\t落地消费\t落地还款\t总手续费\t快速还款费率\t落地还款费率\t还款书续费\n";

        foreach ($allMerchant as $key => $value){

            $levelData = json_decode(exec("curl -H 'Host: m.appfu.net' -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'X-Requested-With: XMLHttpRequest' -H 'Accept-Language: zh-cn' -H 'Content-Type: application/json; charset=UTF-8' -H 'Origin: https://m.appfu.net' -H 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/16D57' -H 'Referer: https://m.appfu.net/page/agent/memberInfm.html?tokenId={$tokenId}&merId={$value->merId}&chckValue={$value->chckValue}' -H 'Cookie: tokenId={$tokenId}; appId=170; appSign=FD2F79F5-B612-4FA7-A171-C46EDB171ED7; appType=1; plaId=158' --data-binary '{\"appType\":\"1\",\"appId\":\"170\",\"appSign\":\"FD2F79F5-B612-4FA7-A171-C46EDB171ED7\",\"plaId\":\"158\",\"tokenId\":\"{$tokenId}\",\"data\":{\"merId\":\"{$value->merId}\",\"chckValue\":\"{$value->chckValue}\"}}' --compressed 'https://m.appfu.net/v1/merchant/getothermerchantinfo.json?tokenId={$tokenId}'"));

            $levelDataCount = json_decode(exec("curl -H 'Host: m.appfu.net' -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'X-Requested-With: XMLHttpRequest' -H 'Accept-Language: zh-cn' -H 'Content-Type: application/json; charset=UTF-8' -H 'Origin: https://m.appfu.net' -H 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/16D57' -H 'Referer: https://m.appfu.net/page/agent/memberInfm.html?tokenId={$tokenId}&merId={$value->merId}&chckValue={$value->chckValue}' -H 'Cookie: tokenId={$tokenId}; appId=170; appSign=FD2F79F5-B612-4FA7-A171-C46EDB171ED7; appType=1; plaId=158' --data-binary '{\"appType\":\"1\",\"appId\":\"170\",\"appSign\":\"FD2F79F5-B612-4FA7-A171-C46EDB171ED7\",\"plaId\":\"158\",\"tokenId\":\"{$tokenId}\",\"data\":{\"merId\":\"{$value->merId}\",\"chckValue\":\"{$value->chckValue}\"}}' --compressed 'https://m.appfu.net/v1/merchant/selectSumComStaByMerid.json?tokenId={$tokenId}'"));

            if(isset($levelData->data) && isset($levelDataCount->data)) {
                $cellData .= "{$value->merName}\t{$value->merMobile}\t{$value->typeName}\t{$value->parentName}\t{$levelData->data->merTime}\t{$levelData->data->merRealTime}\t{$levelData->data->merReal}\t{$levelDataCount->data->totalJsXfAmount}\t{$levelDataCount->data->totalJsHkAmount}\t{$levelDataCount->data->totalJsFee}\t{$levelDataCount->data->totalYyXfAmount}\t{$levelDataCount->data->totalYyHkAmount}\t{$levelDataCount->data->totalYyFee}\t{$levelData->data->typeFastRate}\t{$levelData->data->typeRate}\t{$levelData->data->withdrawal}\n";
            }

        }

        file_put_contents(public_path()."/kaiweishi/邓帅卡卫士直推数据.cvs", $cellData,FILE_APPEND);

    }

    public function getAllMerchant($parentName,$merParentId, $chckValue){
        global $allMerchant;
        $data = $this->getAllMerchantByMerId($merParentId, $chckValue);

        if(
            isset($data->code)
            && $data->code == '0000'
            && !empty($data->data->rows)
        ){
            foreach ($data->data->rows as $rows) {
                $rows->parentName = $parentName;

                $allMerchant[] = $rows;
                $data = $this->getAllMerchant($rows->merName,$rows->merId, $rows->chckValue);
                if(!$data){
                    return true;
                }
            }
        }

        return true;
    }

    public function getAllMerchantByMerId($merParentId = null, $chckValue= null){
        $param = [
            "appType" => "1",
            "appId" => "170",
            "appSign" => "FD2F79F5-B612-4FA7-A171-C46EDB171ED7",
            "plaId" => "158",
            "tokenId" => "EF00928F442740A989DA25EE1366768E",
            "data" => [
                "merParentId" => $merParentId,
                "page" => 1,
                "pageSize" => 1000,
                "chckValue" => $chckValue
            ]
        ];

        $jParam = json_encode($param);

        $curl = <<<CURL
            curl \
                -H 'Host: m.appfu.net' \
                -H 'Accept: application/json, text/javascript, */*; q=0.01' \
                -H 'X-Requested-With: XMLHttpRequest' \
                -H 'Accept-Language: zh-cn' \
                -H 'Content-Type: application/json; charset=UTF-8' \
                -H 'Origin: https://m.appfu.net' \
                -H 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/16D57' \
                -H 'Referer: https://m.appfu.net/page/agent/teamMember.html?tokenId={$param['tokenId']}' \
                -H 'Cookie: tokenId={$param['tokenId']}; appId={$param['tokenId']}; appSign={$param['appSign']}; appType={$param['appType']}; plaId={$param['plaId']}' \
                --data-binary '{$jParam}' \
                --compressed 'https://m.appfu.net/v1/merchant/getAllMerchantByMerId.json?tokenId={$param['tokenId']}'
CURL;

        $data = exec($curl);

        $data = json_decode($data);
        return $data;
    }
}
