<?php

namespace App\Blocks;

use App\Models\CrowdActivity;
use App\Models\CrowdActivityComment;
use App\Models\Crowd;
use App\Models\UserMoneyLog;
use App\Models\Users;
use App\User;
use DB;

class ActivityBlock
{

    //群活动列表
    public function activityList($crowd_id){
        $activityModel = new CrowdActivity();
        $list = DB::table($activityModel->getTable())
            ->select(
                'id',
                'title',
                'start_time',
                'end_time',
                DB::raw('round(charge / 100, 2) as charge'),
                'address',
                'imgurl'
            )
            ->where('crowd_id',$crowd_id)
            ->whereNull('deleted_at')
            ->get();

        foreach ($list as $item) {
            $startTime = strtotime($item->start_time);
            $endTime   = strtotime($item->end_time);
            $item->start_time = date('Y年m月d日',$startTime);
            $item->end_time   = date('Y年m月d日',$endTime);
        }

        return $list;
    }

    //活动详情
    public function activityInfo($uid,$id){
        $activityModel = new CrowdActivity();
        $commentModel = new CrowdActivityComment();
        $info = DB::table($activityModel->getTable())
            ->where('id',$id)
            ->whereNull('deleted_at')
            ->first();
        if($info->charge === 0){
            $info->charge = number_format(0, 2, '.', ' ');
        }else{
            $info->charge = $info->charge / 100;
        }

        $info->start_time = date('Y年m月d日',strtotime($info->start_time));
        $info->end_time   = date('Y年m月d日',strtotime($info->end_time));
        $info->introduce = htmlspecialchars_decode($info->introduce);
        $take = DB::table($commentModel->getTable())
            ->where('user_id',$uid)
            ->where('activity_id',$id)
            ->whereNull('deleted_at')
            ->first();
        if ($take){
            if ($take->is_pay == 0 ){
                $info->isTake = 1;
                $info->order_id = $take->id;
            }else{
                $info->isTake = 2;
            }


            $info->name = $take->name;
            $info->mobile = $take->mobile;
        }else{
            $info->isTake = 0;
        }

        return $info;
    }

    /**
     * 活动是否结束
     * @param $activityId
     * @return bool
     */
    public function activityIsEnd($activityId){
        $activityModel = new CrowdActivity();
        $activityInfo = DB::table($activityModel->getTable())
            ->select(
                'end_time',
                'charge'
            )
            ->where('id',$activityId)
            ->first();

        if (time()>=strtotime($activityInfo->end_time)){
            return false;
        }else{
            return $activityInfo->charge;
        }
    }

    //参与活动
    public function takeActivity($data){
        $commentModel = new CrowdActivityComment();
        $is_take = DB::table($commentModel->getTable())
            ->where('activity_id',$data['activity_id'])
            ->where('user_id',$data['user_id'])
            ->where('is_pay',1)
            ->first();

        if ($is_take){
            return 'take';
        }else{
            return DB::table($commentModel->getTable())->insertGetId($data);
        }


    }

    //付费参与活动
    public function takePayActivity($order){

        try{
            DB::beginTransaction();

            $activityCommentModel = new CrowdActivityComment();
            $activityModel = new CrowdActivity();
            $moneyLogModel = new UserMoneyLog();
            $crowdModel = new Crowd();
            $userModel = new Users();

            if(
                !DB::table($activityCommentModel->getTable())->where('id', $order->id)->update([
                    'pay_time' => date('Y-m-d H:i:s', time()),
                    'is_pay' => 1
                ])
            ){
                DB::rollBack();
                return false;
            }



            $uid = DB::table($activityModel->getTable().' as a')
                ->leftJoin($crowdModel->getTable().' as c','a.crowd_id','=','c.id')
                ->leftJoin($userModel->getTable().' as u','c.uid','=','u.id')
                ->where('a.id',$order->activity_id)
                ->value('u.id');

            if (
                !DB::table($userModel->getTable())->where('id',$uid)->increment('balance',$order->pay_money)
            ){
                DB::rollBack();
                return false;
            }

            $name = DB::table($userModel->getTable())
                ->where('id',$order->user_id)
                ->value('nick_name');

            if (
                !DB::table($moneyLogModel->getTable())->insert([
                    'u_id' => $uid,
                    'type' => 2,
                    'order_id' => $order->id,
                    'money' => $order->pay_money,
                    'remake' => $name.'参与群活动'
                ])
            ){
                DB::rollBack();
                return false;
            }

            DB::commit();
            return true;
        }catch (\Exception $exception){
            \Log::error('takePayActivity：'.$exception);
            return false;
        }
    }

    //我的活动
    public function myActivity($uid){
        $activityModel = new CrowdActivity();
        $commentModel = new CrowdActivityComment();
        $crowdModel = new Crowd();
        $list = DB::table($commentModel->getTable().' as c')
            ->select(
                'c.activity_id as id',
                'a.crowd_id',
                'a.title',
                'a.end_time',
                'a.start_time',
                'a.address',
                DB::raw('round(sq_a.charge / 100, 2) as charge'),
                'a.imgurl',
                'cr.name',
                'c.is_pay'
            )
            ->leftJoin($activityModel->getTable().' as a','a.id','=','c.activity_id')
            ->leftJoin($crowdModel->getTable().' as cr','a.crowd_id','=','cr.id')
            ->where('c.user_id',$uid)
            ->whereNull('c.deleted_at')
            ->get();

        foreach ($list as $item) {
            $item->start_time = date('Y年m月d日',strtotime($item->start_time));
            $item->end_time = date('Y年m月d日',strtotime($item->end_time));
        }

        return $list;
    }
}