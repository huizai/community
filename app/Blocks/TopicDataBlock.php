<?php

namespace App\Blocks;

use App\Models\Topic;
use App\Models\Users;
use App\Models\Crowd;
use App\Models\TopicImgs;
use App\Models\CrowdClassify;
use function foo\func;
use Illuminate\Support\Facades\DB;
use http\Env\Request;

class TopicDataBlock
{

    //动态列表，置顶
    public function topic_stick($data)
    {
        if (!isset($data['page'])) {
            $data['page'] = 1;
        }
        if (!isset($data['pagesize'])) {
            $data['pagesize'] = 5;
        }
        $topic = new Topic();
        $topicimgs = new TopicImgs();
        $user = new Users();
        $crowd = new Crowd();
        $sql = DB::table($topic->getTable() . ' as a')
            ->select('a.id', 'c.nick_name as owner', 'a.content', 'a.useful', 'a.comment', 'a.created_at as createdAt', 'a.updated_at as updatedAt', 'c.headimgurl as avatar')
            ->leftJoin($user->getTable() . ' as c', 'a.uid', '=', 'c.id')
            ->whereNull('a.deleted_at');
        $like = $this->Topic_like($sql, $data['content']);
        if ($like == false) {
            $total = $sql->get()->count();
            $list = $sql->offset(($data['page'] - 1) * $data['pagesize'])->limit($data['pagesize'])->get();
        } else {
            $total = $like->get()->count();
            $list = $like->offset(($data['page'] - 1) * $data['pagesize'])->limit($data['pagesize'])->get();
        }

        $arr = array(
            'list' => $list,
            'pagination' => array(
                'total' => $total,
                'pagesize' => $data['pagesize'],
                'current' => $data['page']
            ),
            'crowd_data' => $this->crowd_name()
        );
        return $arr;
    }

    private function Topic_like($sql, $content)
    {
        if (isset($content)) {
            return $sql->where('a.content', 'like', '%' . $content . '%');
        }
    }

    //获取所有的群名称
    private function crowd_name()
    {
        $crowd = new Crowd();
        $res = DB::table($crowd->getTable())->select('name', 'id')->get();
        return $res;
    }


    //删除群动态
    public function topic_del($id)
    {
        $topic = new Topic();
        $topicimgs = new TopicImgs();
        if(DB::table($topic->getTable())->where('id',$id)->count()<1){
            return false;
        }
        try {
            DB::beginTransaction();
            $topResult = DB::table($topic->getTable())
                ->where('id', $id)
                ->update(['deleted_at' => date('Y-m-d H:i:s', time())]);
            if (!$topResult){
                DB::rollBack();
                return false;
            }

            $imgResult = true;
            if (DB::table($topicimgs->getTable())->where('topic_id',$id)->count()>0){
                $imgResult = DB::table($topicimgs->getTable())
                    ->where('topic_id', $id)
                    ->update(['deleted_at' => date('Y-m-d H:i:s', time())]);
            }
            if (!$imgResult){
                DB::rollBack();
                return false;
            }

            DB::commit();
            return true;
        } catch (\Exception $exception) {
            DB::rollBack();
            \Log::error($exception);
            return false;
        }
    }

    //群分类
    public function crowdClassfity()
    {
        $Class = new CrowdClassify();
        $data = json_decode(json_encode(DB::table($Class->getTable())->select('id', 'pid', 'name', 'sort')->whereNull('deleted_at')->get()), true);
        $res = $this->getClassfityTree($data, 8);
        return $res;
    }

    //排列数据
    private function getClassfityTree($data, $pid)
    {
        $tree = array();
        foreach ($data as $k => $v) {
            if ($v['pid'] == $pid) {
                $v['pid'] = $this->getClassfityTree($data, $v['id']);
                $tree[] = $v;
            }
        }
        return $tree;

    }


    //删除群分类
    public function delcrowdClassfity($id)
    {
        $Class = new CrowdClassify();
        $dbdata = DB::table($Class->getTable())->select('pid')->where('id', $id)->first();
        $dbdata = json_decode(json_encode($dbdata), true);
        if ($dbdata['pid'] == 0) {
            $res = DB::table($Class->getTable())->whereColumn(['id', '=', $id], ['pid', '=', $id])
                ->update(['deleted_at' => dae('Y-m-d H:i:s', time())]);
        } else {
            $res = DB::table($Class->getTable())->where('id', $id)->update(['deleted_at' => date('Y-m-d H:i:s', time())]);
        }
        return $res;
    }
}