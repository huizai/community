<?php
namespace app\Blocks;

use DB;
use App\Models\UserWithdrawDeposit;
use App\Models\Users;

class AdminUserBlock
{
    public function userWithdraw($page, $pageSize)
    {
        $withdrowModal = new UserWithdrawDeposit();
        $UserModal = new Users();
        $sql = DB::table( $withdrowModal->getTable().' as w')
        ->select('w.id', 'w.money', 'w.status', 'w.created_at','u.nick_name','u.mobile')
        ->leftJoin($UserModal->getTable().' as u','w.uid','=','u.id')
        ->whereNull('w.deleted_at');
        $total = $sql->count();
        $list = $sql->take($pageSize)->skip(($page - 1) * $pageSize)->get();

        return [
            'list' => $list,
            'pagination' => [
                'current' => $page,
                'pageSize' => $pageSize,
                'total' => $total
            ]
        ];
    }

    public function userWithdrawUpdate($data){
        $withdrowModal = new UserWithdrawDeposit();
        return DB::table($withdrowModal->getTable())->where('id',$data['id'])->update($data);
    }
}
