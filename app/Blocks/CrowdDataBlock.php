<?php

namespace App\Blocks;

use App\Libs\Jiguang\JMessage;
use App\Models\Crowd;
use App\Models\CrowdClassify;
use App\Models\CrowdCollect;
use App\Models\CrowdFlowingWather;
use App\Models\CrowdJoinOrder;
use App\Models\CrowdMember;
use App\Models\Users;
use DB;

class CrowdDataBlock
{

    //群列表
    public function crowdList($page, $pageSize, $free, $name)
    {
        $crowdModel = new Crowd();
        $userModel = new Users();
        $crowdClassModel = new CrowdClassify();
        $sql = DB::table($crowdModel->getTable() . ' as c')
            ->select(
                'c.id',
                'c.name',
                'c.introduce',
                'c.detailed',
                'c.headimgurl',
                'c.classify_id',
                'c.charge',
                'c.is_free',
                'c.is_private',
                'u.nick_name'
            )
            ->leftJoin($userModel->getTable() . ' as u', 'c.uid', '=', 'u.id')
            ->whereNull('c.deleted_at');
        $total = $sql->count();
        if ($free){
            $sql->where('is_free',$free);
        }
        if ($name){
            $sql->where('name','like','%'.$name.'%');
        }
        $list = $sql->take($pageSize)->skip(($page - 1) * $pageSize)->get();
        foreach ($list as $value) {
            $value->detailed = htmlspecialchars_decode($value->detailed);
        }

        $pagination['current'] = $page;
        $pagination['pageSize'] = $pageSize;
        $pagination['total'] = $total;

        $class = DB::table($crowdClassModel->getTable())->select('id as key', 'pid', 'name as title')->get();
        $class = json_decode(json_encode($class), true);
        foreach ($class as &$val) {
            $val['value'] = $val['key'];
        }
        $items = array();
        foreach ($class as $value) {
            $items[$value['key']] = $value;
        }
        $classData = array();
        foreach ($items as $key => $value) {
            if (isset($items[$value['pid']])) {
                $items[$value['pid']]['children'][] = &$items[$key];
            } else {
                $classData[] = &$items[$key];
            }
        }

        return array(
            'list' => $list,
            'pagination' => $pagination,
            'class' => $classData
        );
    }

    //修改群信息
    public function crowdUpdate($id, $data)
    {
        $crowdModel = new Crowd();
        return DB::table($crowdModel->getTable())->where('id', $id)->update($data);
    }

    //群分类
    public function crowdClass()
    {
        $classModal = new CrowdClassify();
        $class = DB::table($classModal->getTable())
            ->select('id as key', 'pid', 'name as title', 'sort')
            ->whereNull('deleted_at')
            ->get();

        $classTree = [];
        foreach ($class as $value) {
            $value->value = $value->key;
            $classTree[$value->key] = $value;
        }

        $classTree = json_decode(json_encode($classTree), true);
        $tree = [];
        foreach ($classTree as $key => $item) {
            if (isset($classTree[$item['pid']])) {
                $classTree[$item['pid']]['children'][] = &$classTree[$key];
            } else {
                $tree[] = &$classTree[$key];
            }
        }
        $selectData = $tree;
        array_unshift($selectData,['key'=>'0','value'=>'0','title'=>'没有上一级']);

        return ['list' => $tree, 'selectData' => $selectData];
    }

    //删除群分类
    public function crowdClassRemove($id)
    {

        $classModal = new CrowdClassify();
        $data = DB::table($classModal->getTable())->select('id', 'pid')->get();
        $data = json_decode(json_encode($data),true);

        $Ids = [$id];
        foreach ($data as $v) {
            $is_in = in_array($v['pid'], $Ids);
            if ($is_in) {
                $Ids[]  = $v['id'];
            }
        }

        $sql = DB::table($classModal->getTable());
        if (count($Ids)==1){
            $sql->where('id',$id);
        }else{
            $sql->whereIn('id',$Ids);
        }
        return $sql->update(['deleted_at'=>date('Y-m-d H:i:s',time())]);


    }

    public function crowdClassUpdate($id,$pid,$name){

        $classModal = new CrowdClassify();
        return DB::table($classModal->getTable())->where('id',$id)
            ->update([
                'pid'=>$pid,
                'name'=>$name
            ]);
    }

    public function crowdClassAdd($data){

        $classModal = new CrowdClassify();
        return DB::table($classModal->getTable())->insert($data);
    }


}