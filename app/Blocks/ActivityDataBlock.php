<?php
namespace App\Blocks;

use App\Models\CrowdActivity;
use App\Models\Crowd;
use App\Models\CrowdActivityComment;
use App\Models\Users;
use App\Models\AmapCityCode;
use DB;
class ActivityDataBlock{

    //活动列表
    public function activityList($page,$pageSize,$search){
        $activityModel = new CrowdActivity();
        $crowdModel = new Crowd();
        $userModel = new Users();
        $sql = DB::table($activityModel->getTable().' as a')
            ->select('a.*','c.name','u.nick_name','u.headimgurl')
            ->leftJoin($crowdModel->getTable().' as c','a.crowd_id','=','c.id')
            ->leftJoin($userModel->getTable().' as u','u.id','=','c.uid')
            ->whereNull('a.deleted_at');

        $search['crowd_id']?$sql->where('crowd_id',$search['crowd_id']):null;

        $search['title']?$sql->where('title','like','%'.$search['title'].'%'):null;

        $total = $sql->count();

        $list  = $sql->orderBy('created_at','desc')
            ->skip(($page - 1) * $pageSize)->take($pageSize)
            ->get();

        foreach ($list as $item) {
            $item->charge = $item->charge / 100;
            $item->introduce = htmlspecialchars_decode($item->introduce);
            $item->center = strip_tags($item->introduce);
        }

        $cityModel = new AmapCityCode();
        $province = DB::table($cityModel->getTable())
            ->select('id','parent_id','name')
            ->where('level','province')->get();

        $city = DB::table($cityModel->getTable())
            ->select('id','parent_id','name')
            ->where('level','city')->get();

        $district = DB::table($cityModel->getTable())
            ->select('id','parent_id','name')
            ->where('level','district')->get();

        $crowd = DB::table($crowdModel->getTable())->select('id','name')->get();

        $citys = [];
        foreach ($city as $item) {
            foreach ($province as $items) {
                if ($item->parent_id == $items->id){
                    $citys[$items->name][] = $item;
                }
            }
        }

        return array(
            'list' => $list,
            'pagination' => array(
                'current' => (int)$page,
                'pageSize' => $pageSize,
                'total' => $total
            ),
            'province' => DB::table($cityModel->getTable())->where('level','province')->pluck('name'),
            'city' => $citys,
            'district' => $district,
            'crowd' => $crowd
        );
    }

    //修改活动
    public function activityUpdate($id,$data){
        $activityModel = new CrowdActivity();
        $res = DB::table($activityModel->getTable())->where('id',$id)->update($data);
        return $res;
    }

    //删除活动
    public function delActivity($id){
        $activityModel = new CrowdActivity();
        $res = DB::table($activityModel->getTable())->where('id',$id)
            ->update(['deleted_at'=>date('Y-m-d H:i:s')],time());
        return $res;
    }

    public function addActivity($data){
        $activityModel = new CrowdActivity();
        $userModel = new Users();
        $crowdModel = new Crowd();

        $data['created_at'] = date('Y-m-d H:i:s',time());
        $id = DB::table($activityModel->getTable())->insertGetId($data);

        $idData = DB::table($activityModel->getTable().' as a')
            ->select('a.*','u.nick_name','u.headimgurl','c.name')
            ->where('a.id',$id)
            ->leftJoin($crowdModel->getTable().' as c','a.crowd_id','=','c.id')
            ->leftJoin($userModel->getTable().' as u','c.uid','=','u.id')
            ->first();
        $idData->introduce = htmlspecialchars_decode($idData->introduce);

        return $idData;
    }
}