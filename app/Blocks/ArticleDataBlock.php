<?php
namespace App\Blocks;

use App\Models\UserSite;
use App\Models\Users;
use App\Models\Crowd;
use App\Models\CrowdArticle;
use DB;

class ArticleDataBlock{

    public function get_Article($data){
        $article = new CrowdArticle();
        $crowd = new Crowd();
        $user = new Users();
        if ($data['page']==''){
            $data['page']=1;
        }
        if ($data['pagesize']==''){
            $data['pagesize']=5;
        }
        $sql = DB::table($article->getTable().' as a')
            ->select(
                'c.nick_name as owner', //发布人员
                'c.headimgurl as avatar', //发布人头像
                'a.id', //文章ID
                'a.introduce',//文章介绍
                'a.crowd_id',  //所在群ID
                'b.name',   //所属群名
                'a.is_recommend',  //是否推荐
                'a.title',  //文章标题
                'a.content',  //文章内容
                'a.headimgurl',  //文章图片
                'a.useful',  //  点赞数
                'a.comment',  //评论数量
                'a.share',   //分享数量
                'a.views',   //查看数量
                'a.created_at as createdAt',  //创建时间
                'a.updated_at as updatedAt'  //修改时间
            )
            ->leftJoin($crowd->getTable().' as b','a.crowd_id','=','b.id')
            ->leftJoin($user->getTable().' as c','b.uid','=','c.id')
            ->whereNull('a.deleted_at');
        $like = $this->Article_like($sql,$data['crowd_id'],$data['title']);
        if ($like==false){
            $total = $sql->get()->count();
            $list = $sql->offset(($data['page'] - 1) * $data['pagesize'])->limit($data['pagesize'])->get();
        }else{
            $total = $like->get()->count();
            $list = $like->offset(($data['page'] - 1) * $data['pagesize'])->limit($data['pagesize'])->get();

        }
        foreach ($list as $value){
            $value->content = htmlspecialchars_decode($value->content);
        }

        $arr=array(
            'list'=>$list,
            'pagination' => array(
                'total' => $total,
                'pagesize' => $data['pagesize'],
                'current' => $data['page']
            ),
            'crowd_data'=>$this->crowd_name()
        );
        return $arr;


    }
    //模糊查询
    public function Article_like($sql,$crowd_id,$title){
        if(!isset($crowd_id) && !isset($title)) {return false;}
        if (isset($crowd_id) && !isset($title)){
            return $sql->where('a.crowd_id',$crowd_id);
        }
        if (!isset($crowd_id) && isset($title)){
            return $sql->where('a.title','like','%'.$title.'%');
        }
        if (isset($crowd_id) && isset($title)){
            return $sql->where([['a.crowd_id',$crowd_id],['a.title','like','%'.$title.'%']]);
        }
    }
    //获取所有的群名称
    public function crowd_name(){
        $crowd=new Crowd();
        $res = DB::table($crowd->getTable())->select('name','id')->get();
        return $res;
    }


    //添加群文章信息
    public function save_Article($data){
        $article = new CrowdArticle();
        $res = DB::table($article->getTable())->insert($data);
        return $res;
    }

    //删除文章信息
    public function del_Article($id){
        $article = new CrowdArticle();
        $res = DB::table($article->getTable())
            ->where('id',$id)
            ->update(['deleted_at'=>date('Y-m-d H:i:s',time())]);
        return $res;
    }

    //修改文章信息
    public function up_Article($res_data,$id){
        $article = new CrowdArticle();
        $data = array();
        foreach ($res_data as $key=>$value){
            if($value!=='') {
                $data[$key]=$value;
            }
        }

        $res = DB::table($article->getTable())
            ->where('id',$id)
            ->update($data);
        return $res;
    }


}