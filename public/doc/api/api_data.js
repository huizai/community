define({ "api": [
  {
    "type": "post",
    "url": "/api/0.0.1/topic/unread",
    "title": "未读动态信息",
    "name": "TopicUnread",
    "group": "动态",
    "version": "0.0.1",
    "description": "<p>未读动态信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登陆标志</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         \"message\": xx    //未读消息数\n         \"headImg\": xx    //用户头像\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/TopicController.php",
    "groupTitle": "动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/topic/unread"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/topic/unread/list",
    "title": "未读信息列表",
    "name": "TopicUnreadList",
    "group": "动态",
    "version": "0.0.1",
    "description": "<p>未读信息列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登陆标志</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [{\n                 'id' : 1                         //动态ID\n                 'uid'      : 1,                  //用户ID\n                 'nick_name': '张三',              //用户昵称\n                 'content'  : 'xxxxx',            //动态内容\n                 'created_at': 'xxx'              //发布时间\n                 'imgs'     : [                   //图片\n                     {\n                         'id' : 1,                       //图片ID\n                         'imgurl' : 'http://xxxx.png',   //图片地址\n                     },\n                     ...\n                 ]\n                 comment_content: [                  //评论内容数据\n                     {\n                         id :'xx'                    //评论ID\n                         topic_id: 1,                //动态ID\n                         user_id: 22,                //评论用户ID\n                         content: \"写的挺好的！\",      //评论内容\n                         created_at: \"2019-01-16 15:03:10\",  //评论时间\n                         updated_at: \"2019-01-16 15:06:47\",\n                         deleted_at: null,\n                         nick_name: \"H@jh\"           //评论用户\n                     },\n                     ...\n                 ],\n                 UsefulCentent: [                    //点赞数据\n                     {\n                         topic_id: 1,                //动态ID\n                         user_id: 22,                //点赞用户ID\n                         updated_at: \"2019-01-16 15:24:16\",\n                         nick_name: \"H@jh\"           //点赞用户\n                     },\n                     ...\n                 ]\n         }...]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/TopicController.php",
    "groupTitle": "动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/topic/unread/list"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/topic/create/comment",
    "title": "创建动态评论",
    "name": "createComment",
    "group": "动态",
    "version": "0.0.1",
    "description": "<p>创建动态评论</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登录标识</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "topic_id",
            "description": "<p>动态ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>评论内容</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n          \"id\": 'xx',            //评论ID\n          \"topic_id\": 'xx',      //动态ID\n          \"user_id\": 'xx',       //评论人ID\n          \"content\": 'xx',       //评论内容\n          \"created_at\": 'xx',    //评论时间\n          \"updated_at\": 'xx',    //评论修改时间\n          \"deleted_at\": 'xx',    //评论删除时间\n          \"nick_name\": 'xx'      //评论人昵称\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/TopicController.php",
    "groupTitle": "动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/topic/create/comment"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/topic/list",
    "title": "获取动态列表",
    "name": "getCrowdTopic",
    "group": "动态",
    "version": "0.0.1",
    "description": "<p>获取动态列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>分页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "20",
            "description": "<p>单页数量</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [{\n                 'id' : 1                         //动态ID\n                 'uid'      : 1,                  //用户ID\n                 'nick_name': '张三',              //用户昵称\n                 'content'  : 'xxxxx',            //动态内容\n                 'useful'   : 1,                 //是否点赞，1：已点赞 0：未点赞\n                 'comment'  : 123,                //评论的数量\n                 'imgs'     : [                   //图片\n                     {\n                         'id' : 1,                       //图片ID\n                         'imgurl' : 'http://xxxx.png',   //图片地址\n                     },\n                     ...\n                 ]\n                 comment_content: [                  //评论内容数据\n                     {\n                         topic_id: 1,                //动态ID\n                         user_id: 22,                //评论用户ID\n                         content: \"写的挺好的！\",      //评论内容\n                         created_at: \"2019-01-16 15:03:10\",  //评论时间\n                         updated_at: \"2019-01-16 15:06:47\",\n                         deleted_at: null,\n                         nick_name: \"H@jh\"           //评论用户\n                     },\n                     ...\n                 ],\n                 UsefulCentent: [                    //点赞数据\n                     {\n                         topic_id: 1,                //动态ID\n                         user_id: 22,                //点赞用户ID\n                         updated_at: \"2019-01-16 15:24:16\",\n                         nick_name: \"H@jh\"           //点赞用户\n                     },\n                     ...\n                 ]\n         }...]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/TopicController.php",
    "groupTitle": "动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/topic/list"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/topic/news",
    "title": "获取消息列表",
    "name": "getNews",
    "group": "动态",
    "version": "0.0.1",
    "description": "<p>获取消息列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登录标识</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>分页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "20",
            "description": "<p>单页数量</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [{\n                 'id' : 1                         //动态ID\n                 'uid'      : 1,                  //用户ID\n                 'nick_name': '张三',              //用户昵称\n                 'content'  : 'xxxxx',            //动态内容\n                 'useful'   : 123,                //点赞的数量\n                 'comment'  : 123,                //评论的数量\n                 'type'     : 1,                  //消息类型；1动态，2，活动\n                 'redirect' : 'https://xxxx'     //跳转页面h5\n                 'imgs'     : [                   //图片\n                     {\n                         'id' : 1,                       //图片ID\n                         'imgurl' : 'http://xxxx.png',   //图片地址\n                     },\n                     ...\n                 ]\n         }...]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/TopicController.php",
    "groupTitle": "动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/topic/news"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/topic/mytopic",
    "title": "我的动态",
    "name": "myTopic",
    "group": "动态",
    "version": "0.0.1",
    "description": "<p>我的动态</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登陆标志</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [{\n                 'id' : 1                         //动态ID\n                 'uid'      : 1,                  //用户ID\n                 'nick_name': '张三',              //用户昵称\n                 'content'  : 'xxxxx',            //动态内容\n                 'created_at': 'xxx'              //发布时间\n                 'imgs'     : [                   //图片\n                     {\n                         'id' : 1,                       //图片ID\n                         'imgurl' : 'http://xxxx.png',   //图片地址\n                     },\n                     ...\n                 ]\n                 comment_content: [                  //评论内容数据\n                     {\n                         id :'xx'                    //评论ID\n                         topic_id: 1,                //动态ID\n                         user_id: 22,                //评论用户ID\n                         content: \"写的挺好的！\",      //评论内容\n                         created_at: \"2019-01-16 15:03:10\",  //评论时间\n                         updated_at: \"2019-01-16 15:06:47\",\n                         deleted_at: null,\n                         nick_name: \"H@jh\"           //评论用户\n                     },\n                     ...\n                 ],\n                 UsefulCentent: [                    //点赞数据\n                     {\n                         topic_id: 1,                //动态ID\n                         user_id: 22,                //点赞用户ID\n                         updated_at: \"2019-01-16 15:24:16\",\n                         nick_name: \"H@jh\"           //点赞用户\n                     },\n                     ...\n                 ]\n         }...]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/TopicController.php",
    "groupTitle": "动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/topic/mytopic"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/topic/cancel/useful",
    "title": "取消点赞",
    "name": "topicCancelUseful",
    "group": "动态",
    "version": "0.0.1",
    "description": "<p>取消点赞</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "topic_id",
            "description": "<p>动态ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登陆标志</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": {\n                'user_id':'xx'      //用户ID\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/TopicController.php",
    "groupTitle": "动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/topic/cancel/useful"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/topic/comment",
    "title": "动态评论",
    "name": "topicComment",
    "group": "动态",
    "version": "0.0.1",
    "description": "<p>动态评论</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "topic_id",
            "description": "<p>动态ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>分页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "20",
            "description": "<p>单页数量</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {[\n         'topic_id'      : 1,                 //评论ID,\n         'user_id'       : 1,                 //用户ID,\n         'nick_name'     : '张三',             //用户昵称,\n         'headimgurl'    : 'http://xxx.png'   //头像,\n         'content'       : 'xxxxx'            //内容,\n         'created_at'    : ''                 //发布时间\n]...}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/TopicController.php",
    "groupTitle": "动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/topic/comment"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/topic/create",
    "title": "发布动态",
    "name": "topicCreate",
    "group": "动态",
    "version": "0.0.1",
    "description": "<p>发布动态</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>内容</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "corwn_id",
            "description": "<p>群Id</p>"
          },
          {
            "group": "Parameter",
            "type": "String[]",
            "optional": false,
            "field": "img",
            "description": "<p>图片</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/TopicController.php",
    "groupTitle": "动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/topic/create"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/topic/info",
    "title": "动态详情",
    "name": "topicInfo",
    "group": "动态",
    "version": "0.0.1",
    "description": "<p>动态详情</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>动态ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [{\n                 'id' : 1                         //动态ID\n                 'uid'      : 1,                  //用户ID\n                 'nick_name': '张三',              //用户昵称\n                 'content'  : 'xxxxx',            //动态内容\n                 'useful'   : 1,                 //是否点赞，1：已点赞 0：未点赞\n                 'comment'  : 123,                //评论的数量,\n                 'crowd_id' : 1                   //群id,\n                 'crowd_name' : '测试'             //群名称\n                 'imgs'     : [                   //图片\n                     {\n                         'id' : 1,                       //图片ID\n                         'imgurl' : 'http://xxxx.png',   //图片地址\n                     },\n                     ...\n                 ]\n                 comment_content: [                  //评论内容数据\n                     {\n                         topic_id: 1,                //动态ID\n                         user_id: 22,                //评论用户ID\n                         content: \"写的挺好的！\",      //评论内容\n                         created_at: \"2019-01-16 15:03:10\",  //评论时间\n                         updated_at: \"2019-01-16 15:06:47\",\n                         deleted_at: null,\n                         nick_name: \"H@jh\"           //评论用户\n                     },\n                     ...\n                 ],\n                 UsefulCentent: [                    //点赞数据\n                     {\n                         topic_id: 1,                //动态ID\n                         user_id: 22,                //点赞用户ID\n                         updated_at: \"2019-01-16 15:24:16\",\n                         nick_name: \"H@jh\"           //点赞用户\n                     },\n                     ...\n                 ]\n         }...]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/TopicController.php",
    "groupTitle": "动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/topic/info"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/topic/deltopic",
    "title": "删除动态",
    "name": "topic_del",
    "group": "动态",
    "version": "0.0.1",
    "description": "<p>删除动态</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>动态ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/TopicController.php",
    "groupTitle": "动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/topic/deltopic"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/topic/useful",
    "title": "点赞",
    "name": "useful",
    "group": "动态",
    "version": "0.0.1",
    "description": "<p>点赞</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登录标识</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "topic_id",
            "description": "<p>动态ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n                'topic_id': 'xx'        //动态ID\n         'user_id': 'xx'         //用户ID\n         'updated_id':'xx'       //点赞时间\n         'nick_name':'xx'        //点赞用户昵称\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/TopicController.php",
    "groupTitle": "动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/topic/useful"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/cate/add",
    "title": "添加分类",
    "name": "cate_add",
    "group": "后台-分类",
    "version": "0.0.1",
    "description": "<p>添加分类</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "key",
            "description": "<p>分类ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>分类名称</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/GoodsController.php",
    "groupTitle": "后台-分类",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/cate/add"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/cate/del",
    "title": "分类删除",
    "name": "cate_del",
    "group": "后台-分类",
    "version": "0.0.1",
    "description": "<p>分类删除</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>分类ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/GoodsController.php",
    "groupTitle": "后台-分类",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/cate/del"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/cate/list",
    "title": "分类列表",
    "name": "cate_list",
    "group": "后台-分类",
    "version": "0.0.1",
    "description": "<p>分类列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "currentPage",
            "description": "<p>当前页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "description": "<p>页面大小</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "name",
            "description": "<p>分类名称</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "sorter",
            "description": "<p>排序</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : [\n             'list' : {\n                 'key' : xx,                          //分类ID\n                 'name' : xx,                         //分类名称\n                 'level' : 'xx',                     //分类等级\n                 ‘father_id’                         //上级ID\n                 'created_at' : 'xx',                //创建时间\n                 'father_name' : 'xx',               //上级名称\n             },\n             'pagination' : {            //分页数据\n                 'total' : xx,                       //数据总数\n                 'pageSize' : xx,                    //页面大小\n                 'current' : xx                      //当前页\n             }\n             \"treeData\": [               //分类数据\n                  {\n                      \"title\": \"xx\",     //分类名\n                      \"key\": xx,         //分类ID\n                      \"level\": xx,       //分类级别\n                      \"value\": xx,       //分类ID\n                      \"father_id\": xx,   //分类上级ID\n                      \"children\": [      //下级分类分支\n                          {\n                              \"title\": \"xx\",\n                              \"key\": xx,\n                              \"level\": xx,\n                              \"value\": xx,\n                              \"father_id\": xx\n                          },\n                     ]\n                 }\n             ]\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/GoodsController.php",
    "groupTitle": "后台-分类",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/cate/list"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/cate/remove",
    "title": "分类修改",
    "name": "cate_remove",
    "group": "后台-分类",
    "version": "0.0.1",
    "description": "<p>分类修改</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "key",
            "description": "<p>分类ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>分类名称</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/GoodsController.php",
    "groupTitle": "后台-分类",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/cate/remove"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/topic/topstick",
    "title": "动态列表",
    "name": "topic_crowd_list",
    "group": "后台-动态",
    "version": "0.0.1",
    "description": "<p>动态列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "title",
            "description": "<p>动态标题</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>当前页数</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pagesize",
            "defaultValue": "5",
            "description": "<p>每页显示数量</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n         \"list\":{                    //动态列表\n             \"id\": xx,                           //动态ID\n             \"crowd_id\"：xx                       //群ID\n             \"name\": xxx,                        //动态名称\n             \"nick_name\": \"xxxx\",                //动态发布人\n             \"content\": \"xxx\",                   //动态内容\n             \"useful\": xx,                       //点赞数量\n             \"comment\": xx,                      //评论数量\n             \"imgurl\": \"xxxx\",                   //动态图片\n             \"created_at\": \"xx-xx-xx xx:xx:x\"    //动态发布时间\n         }\n         \"pagination\": {              //分页列表\n              \"total\": 15,                       //数据总数\n              \"pagesize\": 5,                     //页面大小\n              \"current\": 1                       //当前位置\n         }\n         \"crowd_data\": [              //群数据列表\n             {\n                  \"name\": \"xxxx\",                //群名称\n                  \"id\": xx                       //群ID\n              },\n         ]\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/TopicDataController.php",
    "groupTitle": "后台-动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/topic/topstick"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/topic/deltopic",
    "title": "删除动态",
    "name": "topic_del",
    "group": "后台-动态",
    "version": "0.0.1",
    "description": "<p>删除动态</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>动态ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/TopicDataController.php",
    "groupTitle": "后台-动态",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/topic/deltopic"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/brand/add",
    "title": "添加品牌",
    "name": "brand_add",
    "group": "后台-品牌",
    "version": "0.0.1",
    "description": "<p>添加品牌</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "c_name",
            "description": "<p>分类ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "name",
            "description": "<p>品牌名称</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/GoodsController.php",
    "groupTitle": "后台-品牌",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/brand/add"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/brand/del",
    "title": "品牌删除",
    "name": "brand_del",
    "group": "后台-品牌",
    "version": "0.0.1",
    "description": "<p>品牌删除</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>品牌ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/GoodsController.php",
    "groupTitle": "后台-品牌",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/brand/del"
      }
    ]
  },
  {
    "type": "get",
    "url": "/admin/brand/list",
    "title": "品牌列表",
    "name": "brand_list",
    "group": "后台-品牌",
    "version": "0.0.1",
    "description": "<p>品牌列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "sorter",
            "description": "<p>排序</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "currentPage",
            "description": "<p>分页当前页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "description": "<p>页面大小</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "name",
            "description": "<p>品牌名称</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "c_id",
            "description": "<p>品牌分类ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [\n             'list' : {\n                 'key' : xx,                          //品牌ID\n                 'name' : xx,                         //品牌名称\n                 'c_name' : 'xx',                     //所属分类名称\n                 'created_at' : 'xx',                //创建时间\n                 'c_id' : 'xx',                      //所属分类ID\n             },\n             'pagination' : {            //分页数据\n                 'total' : xx,                       //数据总数\n                 'pageSize' : xx,                    //页面大小\n                 'current' : xx                      //当前页\n             }\n             \"treeData\": [               //分类数据\n                  {\n                      \"title\": \"xx\",     //分类名\n                      \"key\": xx,         //分类ID\n                      \"level\": xx,       //分类级别\n                      \"value\": xx,       //分类ID\n                      \"father_id\": xx,   //分类上级ID\n                      \"children\": [      //下级分类分支\n                          {\n                              \"title\": \"xx\",\n                              \"key\": xx,\n                              \"level\": xx,\n                              \"value\": xx,\n                              \"father_id\": xx\n                          },\n                     ]\n                 }\n             ]\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/GoodsController.php",
    "groupTitle": "后台-品牌",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/brand/list"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/brand/remove",
    "title": "品牌修改",
    "name": "brand_remove",
    "group": "后台-品牌",
    "version": "0.0.1",
    "description": "<p>品牌修改</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>品牌ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "c_name",
            "description": "<p>分类ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "name",
            "description": "<p>品牌名称</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/GoodsController.php",
    "groupTitle": "后台-品牌",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/brand/remove"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/goods/add",
    "title": "商品添加",
    "name": "goods_add",
    "group": "后台-商品",
    "version": "0.0.1",
    "description": "<p>商品添加</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "show_title",
            "description": "<p>标题</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>商品名称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "producing_area",
            "description": "<p>产地</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "slogan",
            "description": "<p>广告语</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "unit",
            "description": "<p>规格</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "c_id",
            "description": "<p>分类ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "illustrate",
            "description": "<p>详细介绍</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "now_price",
            "description": "<p>现价</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "out_price",
            "description": "<p>售价</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "in_price",
            "description": "<p>进价</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>是否上架</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "b_id",
            "description": "<p>品牌ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/GoodsController.php",
    "groupTitle": "后台-商品",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/goods/add"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/goods/del",
    "title": "商品删除",
    "name": "goods_del",
    "group": "后台-商品",
    "version": "0.0.1",
    "description": "<p>商品删除</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>商品ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/GoodsController.php",
    "groupTitle": "后台-商品",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/goods/del"
      }
    ]
  },
  {
    "type": "get",
    "url": "/admin/goods/list",
    "title": "商品列表",
    "name": "goods_list",
    "group": "后台-商品",
    "version": "0.0.1",
    "description": "<p>商品列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "sorter",
            "description": "<p>根据名字搜索</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "currentPage",
            "defaultValue": "1",
            "description": "<p>当前页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "5",
            "description": "<p>单页数量</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "unit",
            "description": "<p>规格</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "title",
            "description": "<p>标题</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "producing_area",
            "description": "<p>产地</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>商品名字</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [\n             'list' : {\n                 'key' : xx,                          //商品ID\n                 'show_title' : xx,                   //标题\n                 'name' : 'xx',                       //商品名称\n                 'small_image' : 'xx',                //商品列表图\n                 'producing_area' : 'xx',             //产地\n                 'unit': xx,                          //规格\n                 'stock_num' : 'xx',                  //群标签ID\n                 'slogan' : xx,                       //商品广告语\n                 'illustrate' : xx,                   //商品图片\n                 'now_price' : xx,                    //群商品数量\n                 'out_price': xx,                     //进去是否收费 1不收费，0收费\n                 'in_price': xx,                      //是否是私密群 1是，0不是\n                 'salenum': 'xx',                     //分类名称\n                 'status' : 'xx',                     //群主昵称\n                 'on_off_time':'xx'                   //创建时间\n                 'created_at' : xx,                   //群商品数量\n                 'b_id': x,                           //品牌ID\n                 'c_id': xx,                          //分类ID\n                 'd_id': 'x',                         //群ID\n                 'crowd_name' : 'xx',                 //群主昵称\n                 'img_url':'xx'                       //商品轮播图\n             },\n             'pagination' : {            //分页数据\n                 'total' : xx,                       //数据总数\n                 'pageSize' : xx,                    //页面大小\n                 'current' : xx                      //当前页\n             }\n             \"crowd_data\": [             //群数据\n                  {\n                      \"name\": \"xx\",                  //群名称\n                      \"id\": xx                       //群ID\n                  },\n              ],\n             \"brand_data\": [             //品牌数据\n                  {\n                      \"id\": xx,                      //品牌ID\n                      \"c_id\": xx,                    //品牌分类ID\n                      \"name\": \"xx\"                   //品牌名称\n                  },\n             ]\n             \"treeData\": [               //分类数据\n                  {\n                      \"title\": \"xx\",     //分类名\n                      \"key\": xx,         //分类ID\n                      \"level\": xx,       //分类级别\n                      \"value\": xx,       //分类ID\n                      \"father_id\": xx,   //分类上级ID\n                      \"children\": [      //下级分类分支\n                          {\n                              \"title\": \"xx\",\n                              \"key\": xx,\n                              \"level\": xx,\n                              \"value\": xx,\n                              \"father_id\": xx\n                          },\n                     ]\n                 }\n             ]\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/GoodsController.php",
    "groupTitle": "后台-商品",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/goods/list"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/goods/remove",
    "title": "商品修改",
    "name": "goods_remove",
    "group": "后台-商品",
    "version": "0.0.1",
    "description": "<p>商品修改</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "key",
            "description": "<p>商品ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "show_title",
            "description": "<p>商品标题</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "name",
            "description": "<p>商品名称</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "producing_area",
            "description": "<p>商品产地</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "slogan",
            "description": "<p>商品广告语</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "unit",
            "description": "<p>商品规格</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "c_id",
            "description": "<p>分类ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "illustrate",
            "description": "<p>商品介绍</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "now_price",
            "description": "<p>商品现价</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "out_price",
            "description": "<p>商品售价</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "in_price",
            "description": "<p>商品进价</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "status",
            "description": "<p>是否上架 1:上架 , 0:下架</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "b_id",
            "description": "<p>品牌ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/GoodsController.php",
    "groupTitle": "后台-商品",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/goods/remove"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/article/delarticle",
    "title": "删除群文章信息",
    "name": "delArticle",
    "group": "后台-文章",
    "version": "0.0.1",
    "description": "<p>删除群文章信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>文章ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/ArticleDataController.php",
    "groupTitle": "后台-文章",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/article/delarticle"
      }
    ]
  },
  {
    "type": "GET",
    "url": "/admin/article/article",
    "title": "获取群文章列表",
    "name": "getArticle",
    "group": "后台-文章",
    "version": "0.0.1",
    "description": "<p>获取群文章信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>排序数,越大排越前，必须和id同时存在或者同时不存在</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>分页当前页数，默认为第一页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pagesize",
            "description": "<p>分页当前页数大小，默认为每页20条数据</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>文章标题</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n         \"id\": 2,                   //文章ID\n         \"crowd_id\": 1,             //群ID\n         \"uid\": 1,                  //用户ID\n         \"is_recommend\": 1,        //是否推荐，0不推荐\n         \"title\": \"xxxxxxxxxxxxx\",  //文章标题\n         \"content\": \"xxxxxxxxxxx\",  //文章内容\n         \"headimgurl\": \"xxxx.png\",  //文章图片\n         \"useful\": 0,                //赞的数量\n         \"comment\": 0,               //评论的数量\n         \"share\": 0,                 //分享数量\n         \"views\": 0,                 //浏览量\n         \"created_at\": \"2018-12-05 08:53:39\"  //文章发布时间\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/ArticleDataController.php",
    "groupTitle": "后台-文章",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/article/article"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/article/savearticle",
    "title": "添加群文章信息",
    "name": "saveArticle",
    "group": "后台-文章",
    "version": "0.0.1",
    "description": "<p>添加群文章信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>文章标题</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>文章内容</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "headimgurl",
            "description": "<p>图片地址</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "is_recommend",
            "description": "<p>是否推荐，默认值：0(不推荐)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/ArticleDataController.php",
    "groupTitle": "后台-文章",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/article/savearticle"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/article/uparticle",
    "title": "修改文章信息",
    "name": "upArticle",
    "group": "后台-文章",
    "version": "0.0.1",
    "description": "<p>修改文章信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>文章ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "title",
            "description": "<p>文章标题</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "is_recommend",
            "description": "<p>是否推荐</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "views",
            "description": "<p>浏览次数</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "share",
            "description": "<p>分享次数</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "useful",
            "description": "<p>点赞数</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "comment",
            "description": "<p>评论数</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "imgurl",
            "description": "<p>文章图片地址</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "content",
            "description": "<p>文章内容</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/ArticleDataController.php",
    "groupTitle": "后台-文章",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/article/uparticle"
      }
    ]
  },
  {
    "type": "get",
    "url": "/admin/authority/Admlist",
    "title": "添加管理员账号",
    "name": "Admlist",
    "group": "后台",
    "version": "0.0.1",
    "description": "<p>添加管理员账号</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "account",
            "description": "<p>账号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "cid",
            "description": "<p>角色ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/AdminUserController.php",
    "groupTitle": "后台",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/authority/Admlist"
      }
    ]
  },
  {
    "type": "get",
    "url": "/admin/testadmin/Admlist",
    "title": "后台管理员列表",
    "name": "Admlist",
    "group": "后台",
    "version": "0.0.1",
    "description": "<p>后台用户列表</p>",
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/AdminUserController.php",
    "groupTitle": "后台",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/testadmin/Admlist"
      }
    ]
  },
  {
    "type": "get",
    "url": "/admin/admin/currentUser",
    "title": "获取当前登录的用户信息",
    "name": "adminCurrentUser",
    "group": "后台",
    "version": "0.0.1",
    "description": "<p>获取当前登录的用户信息</p>",
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/AdminUserController.php",
    "groupTitle": "后台",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/admin/currentUser"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/crowd/cate",
    "title": "群商品分类",
    "name": "crowd_cate",
    "group": "商品",
    "version": "0.0.1",
    "description": "<p>群商品分类</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'id' : 'xxx',              //商品ID\n         'crowd_id' : 'xxx',        //群ID\n         'name' : 'xx',             //分类名称\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CateController.php",
    "groupTitle": "商品",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/crowd/cate"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/crowd/cate/goods",
    "title": "群商品分类商品",
    "name": "crowd_cate_goods",
    "group": "商品",
    "version": "0.0.1",
    "description": "<p>群商品分类商品</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "cate_id",
            "description": "<p>群商品分类ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'id' : 'xxx',              //商品ID\n         'crowd_id' : 'xxx',        //群ID\n         'name' : 'xx',             //商品名称\n         'small_image' : 'xx',      //商品图片\n         'out_price' : 'xx',        //售价\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CateController.php",
    "groupTitle": "商品",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/crowd/cate/goods"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/goods/goodRecommend",
    "title": "商品推荐",
    "name": "goodRecommend",
    "group": "商品",
    "version": "0.0.1",
    "description": "<p>商品推荐</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>当前页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pageSize",
            "description": "<p>当前页大小</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data: {\n         \"list\": [\n              {\n                  \"id\": 9,\n                  \"crowd_id\": 1,\n                  \"small_image\": \"http://community.img.guoxiaoge.cn/image_51585e19698427661d2b9e681d845b7f.jpg\",\n                  \"salenum\": 0,\n                  \"out_price\": 19800,\n                  \"name\": \"男士外套春秋季2019新款修身帅气百搭薄款工装青年休闲运动夹克男\",\n                  \"inventory\": 0,\n                  \"on_off_time\": \"2019-04-02 15:16:33\",\n                  \"created_at\": \"2019-04-02 15:16:33\"\n              },\n         ],\n         \"pagination\": {\n              \"total\": 8,\n              \"pageSize\": 5,\n              \"current\": 1\n          }\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/GoodsController.php",
    "groupTitle": "商品",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/goods/goodRecommend"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/goods/details",
    "title": "商品详情",
    "name": "goods_details",
    "group": "商品",
    "version": "0.0.1",
    "description": "<p>商品详情</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "id",
            "description": "<p>商品ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data: {\n          \"id\": xx,                   //商品ID\n          \"show_title\": \"xx\",         //标题\n          \"c_name\": \"xxx\",            //群名称\n          \"name\": \"xx\",               //商品名字\n          \"producing_area\": \"xx\",     //产地\n          \"unit\": \"xx\",               //规矩\n          \"salenum\": xx,               //销量\n          \"slogan\": \"xx\",              //广告语\n          \"illustrate\": [                //商品插图\n                     \"xx.jpg\",\n          ],\n          \"now_price\": 200,           //现价\n          \"out_price\": 200,           //售价\n          \"img\": [                     //商品轮播图\n             {\n                      \"img_url\": \"https://community.guoxiaoge.cn/upload/p1.jpg\"\n             }\n          ],\n          \"evalue_content\": [\n              {\n                      \"star\": xx,                    //星星数\n                      \"content\": \"xx\",               //评价内容\n                      \"goods_name\": \"xxx\",           //商品名字\n                      \"headimgurl\": xx               //用户头像\n                      \"nick_name\": xx                //用户昵称\n                      \"create_time\": xx              //评论时间\n                      \"img_url\": []                  //评论图片\n              },\n          ]\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/GoodsController.php",
    "groupTitle": "商品",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/goods/details"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/goods/laser/list",
    "title": "激光群商品列表",
    "name": "goods_laser_list",
    "group": "商品",
    "version": "0.0.1",
    "description": "<p>激光群商品列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "jg_im_gid",
            "description": "<p>激光群ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'id' : 1,                  //商品ID\n         'crowd_id' : '1',          //群ID\n         'small_image' : 'http://xxx.jpg',  //商品图片\n         'salenum' : '100',       //销量\n         'out_price' : '100',    //售价\n         'name': 1,              //商品名字\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/GoodsController.php",
    "groupTitle": "商品",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/goods/laser/list"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/goods/list",
    "title": "群商品列表",
    "name": "goods_list",
    "group": "商品",
    "version": "0.0.1",
    "description": "<p>群商品列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'id' : xx,                  //商品ID\n         'crowd_id' : 'xx',          //群ID\n         'small_image' : 'http://xxx.jpg',  //商品图片\n         'salenum' : 'xx',       //销量\n         'out_price' : 'xx',    //售价\n         'name': xx,              //商品名字\n         'inventory':'xx'        //库存\n         'on_off_time':'xx'      //上架 下架时间\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/GoodsController.php",
    "groupTitle": "商品",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/goods/list"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/goods/goods_update",
    "title": "商品修改",
    "name": "goods_update",
    "group": "商品",
    "version": "0.0.1",
    "description": "<p>商品修改</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>商品ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "show_title",
            "description": "<p>显示的标题</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>商品名称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "producing_area",
            "description": "<p>产地</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "slogan",
            "description": "<p>广告语</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "unit",
            "description": "<p>规矩</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "c_id",
            "description": "<p>分类ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "now_price",
            "description": "<p>现价</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "out_price",
            "description": "<p>售价</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "in_price",
            "description": "<p>进价</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "status",
            "description": "<p>是否上架 1:上架 , 0:下架</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "b_id",
            "description": "<p>品牌ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data: [],\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/GoodsController.php",
    "groupTitle": "商品",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/goods/goods_update"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/tool/app/isupdate",
    "title": "app是否更新",
    "name": "appIsupdate",
    "group": "工具",
    "version": "0.0.1",
    "description": "<p>app是否更新</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "'android'",
              "'ios'"
            ],
            "optional": false,
            "field": "system",
            "description": "<p>系统</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "version",
            "description": "<p>当前版本号</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'version' : '0.0.1',         //最新版本号\n         'is_force' : 0               //是否强制更新到新版本\n         'newest': 1                  //是否是最新版本\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ToolController.php",
    "groupTitle": "工具",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/tool/app/isupdate"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/tool/upload/img",
    "title": "上传图片",
    "name": "uploadImg",
    "group": "工具",
    "version": "0.0.1",
    "description": "<p>上传图片</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "img",
            "description": "<p>图片</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'img_path' : 'http://xxxx.png'\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ToolController.php",
    "groupTitle": "工具",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/tool/upload/img"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/article/comment",
    "title": "文章评论",
    "name": "articleComment",
    "group": "文章",
    "version": "0.0.1",
    "description": "<p>文章评论</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "article_id",
            "description": "<p>文章ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>分页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "20",
            "description": "<p>单页数量</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {[\n         'topic_id'      : 1,                 //评论ID,\n         'user_id'       : 1,                 //用户ID,\n         'nick_name'     : '张三',             //用户昵称,\n         'headimgurl'    : 'http://xxx.png'   //头像,\n         'content'       : 'xxxxx'            //内容,\n         'created_at'    : ''                 //发布时间\n]...}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ArticleController.php",
    "groupTitle": "文章",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/article/comment"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/article/info",
    "title": "文章详情",
    "name": "articleInfo",
    "group": "文章",
    "version": "0.0.1",
    "description": "<p>文章详情</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登陆标志</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>群ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {[\n                 'id' : 1,                        //文章ID\n                 'title': 'xxxxx',                //文章标题\n                 'content' : 'xxxxx',             //文章内容\n                 'headimgurl' : 'xxxxxx',         //文章小图\n                 'comment' : 234,                 //评论数量\n                 'useful' : 234,                  //点赞数量\n                 'share' : 432,                   //分享次数\n                 'created_at': '2018-09-21 12:21:12' //创建时间\n                 'is_useful':'xx'                 //是否点赞 0：未点赞 1：已点赞\n     ]...}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ArticleController.php",
    "groupTitle": "文章",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/article/info"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/article/recommend",
    "title": "推荐文章",
    "name": "articleRecommend",
    "group": "文章",
    "version": "0.0.1",
    "description": "<p>推荐文章</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "article_id",
            "description": "<p>当前打开的文章ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>分页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "20",
            "description": "<p>单页数量</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [{\n                 'id' : 1,                        //文章ID\n                 'title': 'xxxxx',                //文章标题\n                 'content' : 'xxxxx',             //文章内容\n                 'headimgurl' : 'xxxxxx',         //问题小图\n                 'comment' : 234,                 //评论数量\n                 'useful' : 234,                  //点赞数量\n                 'share' : 432,                   //分享次数\n                 'created_at': '2018-09-21 12:21:12' //创建时间\n\n         }...]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ArticleController.php",
    "groupTitle": "文章",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/article/recommend"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/article/create/comment",
    "title": "创建文章评论",
    "name": "createComment",
    "group": "文章",
    "version": "0.0.1",
    "description": "<p>创建文章评论</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "article_id",
            "description": "<p>文章ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登录标识</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>评论内容</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'article_id': 'xx'                  //文章ID\n         'user_id': 'xx'                     //评论用户ID\n         'content': 'xx'                     //用户评论评论\n         'created_at': 'xx-xx-xx xx:xx:xx    //评论时间\n         'nick_name': 'xx'                   //评论用户昵称\n         'headimgurl': 'xx'                  //评论用户头像\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ArticleController.php",
    "groupTitle": "文章",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/article/create/comment"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/article/useful",
    "title": "点赞",
    "name": "useful",
    "group": "文章",
    "version": "0.0.1",
    "description": "<p>点赞</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登录标识</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "article_id",
            "description": "<p>动态ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ArticleController.php",
    "groupTitle": "文章",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/article/useful"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/activity/info",
    "title": "活动详情",
    "name": "activityInfo",
    "group": "活动",
    "version": "0.0.1",
    "description": "<p>活动详情</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登陆标志</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>活动ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n                 'id' : 1,                           //活动ID\n                 'title': 'xx',                      //活动标题\n                 'charge' : 'xx',                    //活动费用\n                 'address' : 'xx',                   //活动地址\n                 'headimgurl' : xx.xx.jpg,           //列表图\n                 'take_num': 'xx'                    //最多参与人数\n                 'start_time' : xx-xx-xx xx:xx:xx,   //开始时间\n                 'end_time' : xx-xx-xx xx:xx:xx,     //结束时间\n                 'introduce': 'xx'                   //活动介绍\n                 'created_at': xx-xx-xx xx:xx:xx     //活动发布时间\n                 'isTake': 1                         //当前用户是否参与  0：未参与 1：已参与未支付 2：已参与已支付\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ActivityControllers.php",
    "groupTitle": "活动",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/activity/info"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/activity/list",
    "title": "活动列表",
    "name": "activityList",
    "group": "活动",
    "version": "0.0.1",
    "description": "<p>活动列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登陆标志</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {[\n                 'id' : 1,                        //活动ID\n                 'title': 'xx',                   //活动标题\n                 'charge' : 'xx',                 //活动费用\n                 'address' : 'xx',                //活动地址\n                 'headimgurl' : 234,              //列表图\n                 'start_time' : 234,              //开始时间\n                 'end_time' : 432,                //结束时间\n     ]...}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ActivityControllers.php",
    "groupTitle": "活动",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/activity/list"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/activity/take",
    "title": "参与活动",
    "name": "activityTake",
    "group": "活动",
    "version": "0.0.1",
    "description": "<p>参与活动</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登陆标志</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "activity_id",
            "description": "<p>活动ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>参与人姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "mobile",
            "description": "<p>参与人手机号</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ActivityControllers.php",
    "groupTitle": "活动",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/activity/take"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/activity/myactivity",
    "title": "我的活动",
    "name": "myactivity",
    "group": "活动",
    "version": "0.0.1",
    "description": "<p>我的活动</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登陆标志</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {[\n                 'name': 'xx'                     //群名\n                 'id' : 1,                        //活动ID\n                 'title': 'xx',                   //活动标题\n                 'charge' : 'xx',                 //活动费用\n                 'address' : 'xx',                //活动地址\n                 'headimgurl' : 234,              //列表图\n                 'start_time' : 234,              //开始时间\n                 'end_time' : 432,                //结束时间\n                 'is_pay': 0                      //0 未付费参与 1 已付费参与\n                 ]...}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/ActivityControllers.php",
    "groupTitle": "活动",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/activity/myactivity"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/group/nodisturb",
    "title": "添加群免打扰",
    "name": "UserGroupNodisturb",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>添加群免打扰</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录标识</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/group/nodisturb"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/group/nodisturb/remove",
    "title": "移出群免打扰",
    "name": "UserGroupNodisturbRemove",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>移出群免打扰</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录标识</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/group/nodisturb/remove"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/balance",
    "title": "个人余额",
    "name": "UserWithBalance",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>个人余额</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录标识</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'balance': 'xx'     //账户余额\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/balance"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/withraw",
    "title": "提现",
    "name": "UserWithRraw",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>提现</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "money",
            "description": "<p>提现金额</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录标识</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/withraw"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/address/add",
    "title": "添加收货地址",
    "name": "address_add",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>添加收货地址</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登陆标志</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>收货人姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "mobile",
            "description": "<p>收货人电话</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "province",
            "description": "<p>省份</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>市</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "district",
            "description": "<p>区、县</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>详细地址</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "useing",
            "description": "<p>是否默认地址 1：默认 0不默认</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AddressController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/address/add"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/address/area",
    "title": "获取城市列表",
    "name": "address_area",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>获取城市列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登陆标志</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     status: \"success\",\n     failedCode: \"\",\n     failedMsg: \"\",\n     data: [\n         {\n             xxx:[           //省、直辖市\n               {\n                 xxx::[      //区、市\n                     xxx,    //区、县\n                 ]\n               }\n             ]\n         }\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AddressController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/address/area"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/address/save",
    "title": "修改收货地址",
    "name": "address_save",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>修改收货地址</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登陆标志</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>收货地址ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "name",
            "description": "<p>收货人姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "mobile",
            "description": "<p>收货人电话</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "province",
            "description": "<p>省份</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "city",
            "description": "<p>市</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "district",
            "description": "<p>区、县</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "address",
            "description": "<p>详细地址</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "useing",
            "description": "<p>是否默认地址 1：默认 0不默认</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AddressController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/address/save"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/bind/mobile",
    "title": "绑定手机号",
    "name": "bindMobile",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>绑定手机号</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "validate_code",
            "description": "<p>验证码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录标识</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/bind/mobile"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/user/bind/wx",
    "title": "绑定微信",
    "name": "bindWx",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>绑定微信</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>app通过sdk获取的code</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "token",
            "description": "<p>登录标识</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/bind/wx"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/collect/crowd",
    "title": "收藏群",
    "name": "collectCrowd",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>获取群列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录状态</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/collect/crowd"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/address/del",
    "title": "删除收货地址",
    "name": "del",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>删除收货地址</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登陆标志</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>收获地址ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AddressController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/address/del"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/forget/password",
    "title": "忘记密码",
    "name": "forgetPassword",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>忘记密码</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "validate_code",
            "description": "<p>验证码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/forget/password"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/user/crowd/join/list",
    "title": "我加入的群",
    "name": "getMyCrowd",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>获取我加入的群</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "allowedValues": [
              "1",
              "2"
            ],
            "optional": true,
            "field": "type",
            "defaultValue": "1",
            "description": "<p>我的群类型;1加入的群 2收藏的群</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>根据名字搜索</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>分页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "20",
            "description": "<p>单页数量</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录状态</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [{\n         'id' : 1,                        //群ID\n         'uid' : '0',                     //群主ID\n         'name' : '汽车',                  //群名称\n         'introduce' : '群简介',           //群简介\n         'headimgurl' : 'http://xxxxx',   //群列表小图,\n         'classify_id': 2,                //群分类ID 分类的二级id\n         'tag_ids' : '1,2,3,4,5',         //群标签ID\n         'grade' : 12,                    //群等级,\n         'member' : 234,                  //群成员数量,\n         'goods' : 4343,                  //群商品数量,\n         'is_free': 1,                    //进去是否收费 1不收费，0收费\n         'is_private': 1,                 //是否是私密群 1是，0不是\n         'classify_name': '分类',          //分类名称,\n         'nick_name' : '辉仔',             //群主昵称,\n     }...]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/crowd/join/list"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/crowd/list",
    "title": "我创建的群",
    "name": "getMyCrowdList",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>我创建的群</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>根据名字搜索</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>分页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "20",
            "description": "<p>单页数量</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录状态</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [{\n         'id' : 1,                        //群ID\n         'uid' : '0',                     //群主ID\n         'name' : '汽车',                  //群名称\n         'introduce' : '群简介',           //群简介\n         'headimgurl' : 'http://xxxxx',   //群列表小图,\n         'classify_id': 2,                //群分类ID 分类的二级id\n         'tag_ids' : '1,2,3,4,5',         //群标签ID\n         'grade' : 12,                    //群等级,\n         'member' : 234,                  //群成员数量,\n         'goods' : 4343,                  //群商品数量,\n         'is_free': 1,                    //进去是否收费 1不收费，0收费\n         'is_private': 1,                 //是否是私密群 1是，0不是\n         'classify_name': '分类',          //分类名称,\n         'nick_name' : '辉仔',             //群主昵称,\n     }...]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/crowd/list"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/user/topic",
    "title": "获取用户动态列表",
    "name": "getUserTopic",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>获取群动态列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>分页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "20",
            "description": "<p>单页数量</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录状态</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [{\n                 'id' : 1                         //动态ID\n                 'uid'      : 1,                  //用户ID\n                 'nick_name': '张三',              //用户昵称\n                 'content'  : 'xxxxx',            //动态内容\n                 'useful'   : 123,                //点赞的数量\n                 'comment'  : 123,                //评论的数量\n                 'imgs'     : [                   //图片\n                     {\n                         'id' : 1,                       //图片ID\n                         'imgurl' : 'http://xxxx.png',   //图片地址\n                     },\n                     ...\n                 ]\n         }...]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/topic"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/join/crowd",
    "title": "加入群",
    "name": "joinCrowd",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>加入群</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录状态</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'charge' : 10.01 //需要支付的费用\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/join/crowd"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/login",
    "title": "账号密码登录",
    "name": "login",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>用户通过手机号和密码进行登录</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'id' : 1,                               //用户ID\n         'mobile' : '18800001111',               //用户手机号\n         'nick_name' : '张三',                    //用户昵称\n         'headimgurl' : 'http://xxxx.png',       //用户头像\n         'auth_token' : xxxxxxx                  //判断是否登录的凭证,\n         'user_cert_status' : null               //是否实名；null未实名;0已提交;1通过;2未通过\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/login"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/login/from/mobile",
    "title": "手机号短信登录",
    "name": "mobileLogin",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>用户通过手机号和验证码进行登录</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "validate_code",
            "description": "<p>验证码</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'id' : 1,                               //用户ID\n         'mobile' : '18800001111',               //用户手机号\n         'nick_name' : '张三',                    //用户昵称\n         'headimgurl' : 'http://xxxx.png',       //用户头像\n         'auth_token' : xxxxxxx                  //判断是否登录的凭证,\n         'user_cert_status' : null               //是否实名；null未实名;0已提交;1通过;2未通过\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/login/from/mobile"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/register/from/mobile",
    "title": "手机号注册",
    "name": "mobileRegister",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>用户通过手机号和验证码进行注册</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "validate_code",
            "description": "<p>验证码</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/register/from/mobile"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/user/my/info",
    "title": "获取自己的信息",
    "name": "myInfo",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>获取自己的信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录状态</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'id' : 1,                               //用户ID\n         'mobile' : '18800001111',               //用户手机号\n         'nick_name' : '张三',                    //用户昵称\n         'headimgurl' : 'http://xxxx.png',       //用户头像\n         'crowd_num' : 1,                        //群数量\n         'user_cert_status' : null               //是否实名；null未实名;0已提交;1通过;2未通过\n         'wx_openid': 'xx'                       //用户微信openid\n         'jg_im_username': 'xx'                  //极光IM的用户名\n         'crowd_num': 'xx'                       //用户所创建的群数\n         'crowd_money': [                        //用户群收益\n             {\n                 id: 'xx',                       //群ID\n                 name: 'xxx',                    //群名\n                 income: 'xx'                    //群收益\n             }\n             ...\n         ]\n         money: 'xx'                             //用户总收益\n         monthIcom: 'xx'                         //月收益\n         yesterdayIcom: 'xx'                     //昨日收益\n         auth_token: 'xx'                        //用户token\n         jg_im_userpassword: 'xx'\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/my/info"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/address/myaddress",
    "title": "获取收货地址列表",
    "name": "myaddress",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>获取收货地址列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登陆标志</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'id': 'xx'                   //地址ID\n         'name' : 'xxx',              //收货人\n         'mobile' : 'xxx',            //收获电话\n         'province' : 'xx',           //省\n         'city' : 'xx',               //市\n         'district' : 'xx',           //区\n         'address': 'xx',             //详细地址\n         'useing' : '0'               //默认地址，1默认，0不默认\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/AddressController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/address/myaddress"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/out/crowd",
    "title": "退出群",
    "name": "outCrowd",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>退出群</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录状态</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/out/crowd"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/bind/mobile/update",
    "title": "修改绑定的手机号",
    "name": "updateBindMobile",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>修改绑定的手机号</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "validate_code",
            "description": "<p>验证码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录标识</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/bind/mobile/update"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/cert",
    "title": "提交实名资料",
    "name": "userCert",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>提交实名资料</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "real_name",
            "description": "<p>真实姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>联系电话</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id_card",
            "description": "<p>身份证号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id_card_img_left",
            "description": "<p>身份证正面照</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id_card_img_right",
            "description": "<p>身份证反面照</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录状态</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/cert"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/friend/req",
    "title": "好友请求",
    "name": "userFriendReq",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>好友请求</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录标识</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "friend",
            "description": "<p>登录标识</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'balance': 'xx'     //账户余额\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/friend/req"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/user/friend/req-list",
    "title": "好友请求列表",
    "name": "userFriendReqList",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>好友请求列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录标识</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>分页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "20",
            "description": "<p>单页数量</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/friend/req-list"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/friend/res",
    "title": "好友请求处理",
    "name": "userFriendRes",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>好友请求处理</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录标识</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>状态</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_friend_id",
            "description": "<p>好友列表ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/friend/res"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/logout",
    "title": "退出登录",
    "name": "userLogout",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>退出登录</p>",
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/logout"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/update/info",
    "title": "更新用户信息",
    "name": "userUpdateInfo",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>更新用户信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录状态</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "headimgurl",
            "description": "<p>用户头像</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "nick_name",
            "description": "<p>昵称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "signature",
            "description": "<p>签名</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/update/info"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/user/waste",
    "title": "个人账单",
    "name": "userWaste",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>个人账单</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录标识</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [\n         \"id\": xx,                       //用户ID\n          \"balance\": xx,                   //余额\n          \"monthIcom\": \"xx\",             //月收入\n          \"yesterdayIcom\": xx,           //昨日收入\n          \"waste\": {                     //流水详情\n              \"01\":[                     //流水月份\n                 {\n                      \"id\":2\n                      \"money\": xx,        //金额\n                      \"create_at\": xx,    //时间\n                      \"name\": \"xx\",       //备注\n                      \"type\": \"xx\",       //流水类型 0：商品收入 1：加群收入 2：活动收入\n                      \"month\": \"xx\"       //月份\n                  }\n          ]\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/user/waste"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/validate/code/form/sms",
    "title": "获取验证码",
    "name": "validateCode",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>通过手机号获取验证码</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>手机号</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'validate_code': 123456     //验证码\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/validate/code/form/sms"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/login/from/wx",
    "title": "微信登录",
    "name": "wxLogin",
    "group": "用户",
    "version": "0.0.1",
    "description": "<p>用户微信登录</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>app通过sdk获取的code</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'id' : 1,                               //用户ID\n         'mobile' : '18800001111',               //用户手机号\n         'nick_name' : '张三',                    //用户昵称\n         'headimgurl' : 'http://xxxx.png',       //用户头像\n         'auth_token' : xxxxxxx                  //判断是否登录的凭证,\n         'user_cert_status' : null               //是否实名；null未实名;0已提交;1通过;2未通过\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/UserController.php",
    "groupTitle": "用户",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/login/from/wx"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/testadmin/weconf",
    "title": "微信配置信息",
    "name": "getWe_config",
    "group": "系统",
    "version": "0.0.1",
    "description": "<p>微信配置信息</p>",
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n         \"id\": 1,          //微信ID\n         \"name\": \"xx\",     //公众号名称名称\n         \"base_url\": \"xx\",       //动态发布人\n         \"type\": 1,    //公众号类型 1：订阅号\n         \"app_id\": \"xxxxx\",  //微信app_id\n         \"secret\": \"xxxxx\",  //微信密匙\n         \"token\":  \"xxxxx\",  //微信token\n         \"aes_key\" \"xxxxx\".  //微信aes_key\n         \"update_time\": \"2018-12-05 08:53:39\"  //修改时间\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/WechatController.php",
    "groupTitle": "系统",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/testadmin/weconf"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/testadmin/weuser",
    "title": "微信用户列表",
    "name": "getWe_user",
    "group": "系统",
    "version": "0.0.1",
    "description": "<p>微信用户列表</p>",
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n         \"id\": 1,          //微信用户ID\n         \"wechat_id\": \"1\"       //微信ID\n         \"openid\": \"xxxxx\",     //微信用户openid\n         \"nick_name\": \"xxxxxx\",   //微信名称\n         \"city\": \"xxxxx\",    //所在城市\n         \"province\": \"xxxxx\",  //所在省份\n         \"country\": \"xxxxx\",  //所在国家\n         \"sex\":  \"xxxxx\",  //性别\n         \"headimgurl\": \"xxxxx\".  //微信头像\n         \"subscribe\": \"1\"    //是否关注 1：关注\n         \"subscribe_time\": \"2018-12-05 08:53:39\"  //关注时间\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/WechatController.php",
    "groupTitle": "系统",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/testadmin/weuser"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/system/information",
    "title": "平台消息",
    "name": "information",
    "group": "系统",
    "version": "0.0.1",
    "description": "<p>平台消息</p>",
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n         {\n*             \"id\": 1,                                //消息ID\n             \"title\": \"测试系统消息\"                  //消息标题\n             \"created_at\": \"2019-04-25 11:52:45\",    //发布时间\n             \"type\": 1,                              //0：文本消息  1：图文消息\n             \"img_url\": xx.jpg                       //封面图\n         }\n      ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/SystemController.php",
    "groupTitle": "系统",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/system/information"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/system/information/info",
    "title": "平台消息详情",
    "name": "informationInfo",
    "group": "系统",
    "version": "0.0.1",
    "description": "<p>平台消息详情</p>",
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     \"data\": [\n         {\n*             \"id\": 1,\n             \"title\": \"测试系统消息\"\n             \"content\": \"<p>测试系统消息</p>\",\n             \"created_at\": \"2019-04-25 11:52:45\",\n         }\n      ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/SystemController.php",
    "groupTitle": "系统",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/system/information/info"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/crowd/create",
    "title": "创建群",
    "name": "crowdCreate",
    "group": "群操作",
    "version": "0.0.1",
    "description": "<p>创建群</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>群名称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "headimgurl",
            "description": "<p>群头像</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "classify_id",
            "description": "<p>群分类</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "is_free",
            "defaultValue": "0",
            "description": "<p>是否免费</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "charge",
            "defaultValue": "99.99",
            "description": "<p>费用</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "is_private",
            "defaultValue": "0",
            "description": "<p>公开还是私密</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>地址</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "introduce",
            "description": "<p>简介</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lat",
            "description": "<p>经度</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lon",
            "description": "<p>纬度</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录凭证</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CrowdController.php",
    "groupTitle": "群操作",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/crowd/create"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/crowd/info",
    "title": "获取群详情",
    "name": "crowdInfo",
    "group": "群操作",
    "version": "0.0.1",
    "description": "<p>获取群详情</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>群ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'crowd' : {\n                 'id' : 1,                        //群ID\n                 'uid' : '0',                     //群主ID\n                 'name' : '汽车',                  //群名称\n                 'detailed' : '详细简介',           //详细简介\n                 'headimgurl' : 'http://xxxxx',   //群列表小图,\n                 'classify_id': 2,                //群分类ID 分类的二级id\n                 'tag_ids' : '1,2,3,4,5',         //群标签ID\n                 'grade' : 12,                    //群等级,\n                 'member' : 234,                  //群成员数量,\n                 'goods' : 4343,                  //群商品数量,\n                 'is_free': 1,                    //进去是否收费 1不收费，0收费\n                 'is_private': 1,                 //是否是私密群 1是，0不是\n                 'classify_name': '分类',          //分类名称,\n                 'nick_name' : '辉仔',             //群主昵称,\n                 'created_at':'2018:10:11'        //创建时间\n                 'count': 'xx'                    //加入该群用户数\n                 'articleCount': 'xx'             //文章数量\n                 'login_user_is_admin' : 0        //我是否可以管理这个群\n                 'userData':[                     //群用户信息\n                     {\n                         'headimgurl':[           //用户头像\n                             'http://xx.jpg'\n                             ....\n                          ]\n                         'nick_name':'xx'        //用户昵称\n                     }\n                 ]\n         },\n         'article' : [{\n                 'id' : 1,                        //文章ID\n                 'title': 'xxxxx',                //文章标题\n                 'content' : 'xxxxx',             //文章内容\n                 'headimgurl' : 'xxxxxx',         //问题小图\n                 'comment' : 234,                 //评论数量\n                 'useful' : 234,                  //点赞数量\n                 'share' : 432,                   //分享次数\n                 'views' : 231,                   //浏览量\n                 'created_at': '2018-09-21 12:21:12' //创建时间\n         }...]\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CrowdController.php",
    "groupTitle": "群操作",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/crowd/info"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/crowd/info/jgid",
    "title": "根据极光群ID获取群详情",
    "name": "crowdInfoJgId",
    "group": "群操作",
    "version": "0.0.1",
    "description": "<p>根据极光群ID获取群详情</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "jg_im_gid",
            "description": "<p>群ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'crowd' : {\n                 'id' : 1,                        //群ID\n                 'uid' : '0',                     //群主ID\n                 'name' : '汽车',                  //群名称\n                 'introduce' : '群简介',           //群简介\n                 'headimgurl' : 'http://xxxxx',   //群列表小图,\n                 'classify_id': 2,                //群分类ID 分类的二级id\n                 'tag_ids' : '1,2,3,4,5',         //群标签ID\n                 'grade' : 12,                    //群等级,\n                 'member' : 234,                  //群成员数量,\n                 'goods' : 4343,                  //群商品数量,\n                 'is_free': 1,                    //进去是否收费 1不收费，0收费\n                 'is_private': 1,                 //是否是私密群 1是，0不是\n                 'classify_name': '分类',          //分类名称,\n                 'nick_name' : '辉仔',             //群主昵称,\n                 'created_at':'2018:10:11'        //创建时间\n         },\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CrowdController.php",
    "groupTitle": "群操作",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/crowd/info/jgid"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/crowd/update",
    "title": "更新群",
    "name": "crowdUpdate",
    "group": "群操作",
    "version": "0.0.1",
    "description": "<p>修改群</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>群ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "name",
            "description": "<p>群名称</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "headimgurl",
            "description": "<p>群头像</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "classify_id",
            "description": "<p>群分类</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "is_free",
            "description": "<p>是否免费</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "charge",
            "description": "<p>费用</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "is_private",
            "description": "<p>公开还是私密</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "address",
            "description": "<p>地址</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "lat",
            "description": "<p>经度</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "lon",
            "description": "<p>纬度</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "deleted_at",
            "description": "<p>删除时间</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录凭证</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CrowdController.php",
    "groupTitle": "群操作",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/crowd/update"
      }
    ]
  },
  {
    "type": "post",
    "url": "/admin/testadmin/delclass",
    "title": "删除群分类",
    "name": "delClassify",
    "group": "群操作",
    "version": "0.0.1",
    "description": "<p>删除群分类</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>分类ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'FAILED',\n     'failedMsg' : '操作失败',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Admin/TopicDataController.php",
    "groupTitle": "群操作",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/admin/testadmin/delclass"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/crowd/classify",
    "title": "获取群分类",
    "name": "getClassify",
    "group": "群操作",
    "version": "0.0.1",
    "description": "<p>获取群分类</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pid",
            "defaultValue": "0",
            "description": "<p>分类上级id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'id' : 1,       //分类ID\n         'pid' : '0',    //分类上级ID\n         'name' : '汽车', //分类名称\n         'icon': 'http://community.img.guoxiaoge.cn/qiche.png', //分类icon\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CrowdController.php",
    "groupTitle": "群操作",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/crowd/classify"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/crowd/list",
    "title": "获取群列表",
    "name": "getCrowd",
    "group": "群操作",
    "version": "0.0.1",
    "description": "<p>获取群列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "p_cid",
            "description": "<p>一级分类id</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "s_cid",
            "description": "<p>二级分类id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "search",
            "description": "<p>根据名字搜索</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>分页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "20",
            "description": "<p>单页数量</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n         'id' : 1,                        //群ID\n         'uid' : '0',                     //群主ID\n         'name' : '汽车',                  //群名称\n         'jg_im_gid' : 'xx'               //激光群\n         'introduce' : '群简介',           //群简介\n         'headimgurl' : 'http://xxxxx',   //群列表小图,\n         'classify_id': 2,                //群分类ID 分类的二级id\n         'tag_ids' : '1,2,3,4,5',         //群标签ID\n         'grade' : 12,                    //群等级,\n         'member' : 234,                  //群成员数量,\n         'goods' : 4343,                  //群商品数量,\n         'is_free': 1,                    //进去是否收费 1不收费，0收费\n         'is_private': 1,                 //是否是私密群 1是，0不是\n         'charge' : 'xx'                  //进群费用\n         'classify_name': '分类',          //分类名称,\n         'nick_name' : '辉仔',             //群主昵称,\n         'created_at':'2018:10:11'        //创建时间\n         'isJoin': '0'                   //是否加入该群,0:未加入,1:已加入\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CrowdController.php",
    "groupTitle": "群操作",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/crowd/list"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/crowd/article",
    "title": "获取群文章列表",
    "name": "getCrowdArticle",
    "group": "群操作",
    "version": "0.0.1",
    "description": "<p>获取群文章列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>分页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "20",
            "description": "<p>单页数量</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [{\n                 'id' : 1,                        //文章ID\n                 'title': 'xxxxx',                //文章标题\n                 'content' : 'xxxxx',             //文章内容\n                 'headimgurl' : 'xxxxxx',         //问题小图\n                 'comment' : 234,                 //评论数量\n                 'useful' : 234,                  //点赞数量\n                 'share' : 432,                   //分享次数\n                 'created_at': '2018-09-21 12:21:12' //创建时间\n\n         }...]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CrowdController.php",
    "groupTitle": "群操作",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/crowd/article"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/crowd/member",
    "title": "获取群成员列表",
    "name": "getCrowdMember",
    "group": "群操作",
    "version": "0.0.1",
    "description": "<p>获取群成员列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>分页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "20",
            "description": "<p>单页数量</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [{\n                 'id' : 1,                           //成员ID\n                 'nick_name': 'xxxxx',               //成员昵称\n                 'mobile' : 'xxxxx',                 //成员手机号\n                 'headimgurl' : 'xxxxxx',            //成员头像\n                 'updated_at': '2018-09-21 12:21:12' //创建时间\n\n         }...]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CrowdController.php",
    "groupTitle": "群操作",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/crowd/member"
      }
    ]
  },
  {
    "type": "get",
    "url": "/api/0.0.1/crowd/money/log",
    "title": "获取群收入支出流水",
    "name": "getMoneyLog",
    "group": "群操作",
    "version": "0.0.1",
    "description": "<p>获取群收入支出流水</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "page",
            "defaultValue": "1",
            "description": "<p>分页</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "pageSize",
            "defaultValue": "20",
            "description": "<p>单页数量</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [{\n                 'id' : 1,                           //ID\n                 'money': 'xxxxx',                   //金额\n                 'remark' : 'xxxxx',                 //备注\n                 'created_at' : 'xxxxxx',            //创建时间,\n                 'name' : 'xxxx'                     //群名称\n\n         }...]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CrowdController.php",
    "groupTitle": "群操作",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/crowd/money/log"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/crowd/remove/member",
    "title": "移出群成员",
    "name": "removeMember",
    "group": "群操作",
    "version": "0.0.1",
    "description": "<p>移出群成员</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "crowd_jg_id",
            "description": "<p>群极光ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "member_jg_id",
            "description": "<p>群成员极光ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登录标识</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/CrowdController.php",
    "groupTitle": "群操作",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/crowd/remove/member"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/orders/take",
    "title": "添加商品评论",
    "name": "addGoodComment",
    "group": "订单",
    "version": "0.0.1",
    "description": "<p>添加商品评论</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "order_id",
            "description": "<p>订单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>评论内容</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "star",
            "description": "<p>评分星数</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "image",
            "description": "<p>图片</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "goods_id",
            "description": "<p>商品ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "goods_name",
            "description": "<p>商品名字</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : 1  //新增ID\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/OrderController.php",
    "groupTitle": "订单",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/orders/take"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/orders/myorder",
    "title": "获取订单列表",
    "name": "myorder",
    "group": "订单",
    "version": "0.0.1",
    "description": "<p>获取订单列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>登陆标志</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "status",
            "description": "<p>订单状态</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : {\n             'id',               //订单ID\n             'crowd_id',         //群ID\n             'c_name',             //群名\n             'freight',          //运费\n             'pay_total',        //总共需支付的总价\n             'order_num',        //订单号\n             'service_charge',   //服务费\n             'status',           //订单状态 0未付款 1已支付 2已发货 3已收货 4已评价 5退款中 6已退款 7已取消\n             'message',           //订单状态 0未付款 1已支付 2已发货 3已收货 4已评价 5退款中 6已退款 7已取消\n             'nick_name',        //购买人\n             'b_name',           //购买商品品牌名称\n             'name',             //购买商品名称\n             'small_img'         //商品列表图\n             'buy_num',          //购买数量\n             'now_price',        //购买商品现价\n             'province',         //购买人所在省\n             'city',             //购买人所在市、区\n             'district',         //购买人所在县\n             'address',          //详细地址\n             'created_at'        //订单创建时间\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/OrderController.php",
    "groupTitle": "订单",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/orders/myorder"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/order",
    "title": "订单详情",
    "name": "order",
    "group": "订单",
    "version": "0.0.1",
    "description": "<p>确认收货</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "order_id",
            "description": "<p>订单ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [\n         \"status\": 3         //3:已收货\n         \"message\": \"已收货\" //3:已收货\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/OrderController.php",
    "groupTitle": "订单",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/order"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/order",
    "title": "订单详情",
    "name": "order",
    "group": "订单",
    "version": "0.0.1",
    "description": "<p>订单详情</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "order_id",
            "description": "<p>订单ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/OrderController.php",
    "groupTitle": "订单",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/order"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/orders/cancel",
    "title": "取消订单、退款",
    "name": "order_cancel",
    "group": "订单",
    "version": "0.0.1",
    "description": "<p>取消订单、退款</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>当前订单状态</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "order_id",
            "description": "<p>订单ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登陆标志</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : [\n         \"status\": 7         //5:退款中 7:已取消\n         \"message\": \"已取消\" //5:退款中 7:已取消\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/OrderController.php",
    "groupTitle": "订单",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/orders/cancel"
      }
    ]
  },
  {
    "type": "post",
    "url": "/api/0.0.1/orders/create",
    "title": "创建订单",
    "name": "order_create",
    "group": "订单",
    "version": "0.0.1",
    "description": "<p>创建订单</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户登陆标志</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "crowd_id",
            "description": "<p>群ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "service_charge",
            "description": "<p>服务费</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "freight",
            "description": "<p>运费</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "g_total_price",
            "description": "<p>商品总价</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "pay_total",
            "description": "<p>总共需支付的总价</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "g_id",
            "description": "<p>商品ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "buy_num",
            "description": "<p>购买数量</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "a_id",
            "description": "<p>用户填写地址ID</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "操作成功响应示例",
          "content": "{\n     'status' : 'success',\n     'failedCode' : '',\n     'failedMsg' : '',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "操作失败响应示例",
          "content": "{\n     'status' : 'failed',\n     'failedCode' : 'ERROR CODE',\n     'failedMsg' : 'ERROR MSG',\n     'data'  : []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/Http/Controllers/Api/OrderController.php",
    "groupTitle": "订单",
    "sampleRequest": [
      {
        "url": "https://community.guoxiaoge.cn/api/0.0.1/orders/create"
      }
    ]
  }
] });
