<?php

Route::group(['namespace'=>'Admin'], function () {
    Route::options('/{all}', function(\Illuminate\Http\Request $request) {
        $origin = $request->header('ORIGIN', '*');
        header("Access-Control-Allow-Origin: $origin");
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Headers: Origin, Access-Control-Request-Headers, SERVER_NAME, Access-Control-Allow-Headers, cache-control, token, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie');
        http_response_code(204);
    })->where(['all' => '([a-zA-Z0-9-]|/)+']);

    Route::group(['middleware' => ['ao']], function (){
        Route::post('/login', 'AdminUserController@login');
        Route::group(['middleware' => ['AdminAuth']], function () {
            Route::get('/currentUser', 'AdminUserController@currentUser');

            Route::group(['prefix' => 'crowd'], function (){
                Route::post('/del','CrowdController@delCrowd');
                Route::get('/list', 'CrowdController@getCrowd');
                Route::post('/update','CrowdController@crowdUpdate');

                Route::get('/class', 'CrowdController@crowdClass');
                Route::post('/class/remove', 'CrowdController@crowdClassRemove');
                Route::post('/class/update', 'CrowdController@crowdClassUpdate');
                Route::post('/class/add', 'CrowdController@crowdClassAdd');
            });


            Route::group(['prefix' => 'appuser'], function (){
                Route::get('/cert', 'UserController@getUserCert');
                Route::post('/cert/update/status', 'UserController@updateUserCertStatus');
                Route::get( '/withDraw/list', 'AdminUserController@userWithdraw');
                Route::post('/withDraw/status', 'AdminUserController@userWithdrawUpdate');
            });

            Route::group(['prefix' => 'authority'],function (){
                Route::get('/Admlist','AdminUserController@adminUserList'); //后台账号列表
                Route::post('/Admin_add','AdminUserController@adminUserAdd'); //添加后台账号
                Route::post('/Admin_remove','AdminUserController@adminUserUpdate'); //修改后台账号
                Route::post('/Admin_del','AdminUserController@adminUserDel'); //修改后台账号
                Route::get('/admin/roles','AdminUserController@adminRolesList'); //角色列表
                Route::post('/roles/add','AdminUserController@adminRolesAdd'); //添加角色
                Route::post('/roles/del','AdminUserController@adminRolesRemove'); //删除角色
                Route::post('/roles/remove','AdminUserController@adminRolesUpdate'); //修改角色
                Route::get('/per/list','AdminUserController@adminPerList'); //权限列表
                Route::post('/per/remove','AdminUserController@adminPerUpdate'); //修改权限
                Route::post('/per/del','AdminUserController@adminPerDel'); //删除权限
                Route::post('/per/add','AdminUserController@adminPerAdd'); //删除权限
            });

            Route::group(['prefix' => 'article'],function (){
                    Route::get('/list', 'ArticleDataController@getArticle');//文章列表
                    Route::post('/delarticle', 'ArticleDataController@delArticle');//删除文章
                    ROute::post('/uparticle', 'ArticleDataController@updateArticle');//修改文章
                    Route::post('/savearticle','ArticleDataController@saveArticle');//添加文章
            });

            Route::group(['prefix' => 'topic'],function (){
                Route::post('/topstick','TopicDataController@topicStick');//动态列表
                Route::post('/deltopic','TopicDataController@topicDel');//删除动态
            });

            Route::group(['prefix' => 'activity'],function (){
                Route::get('/list','ActivityController@activityList');//活动列表
                Route::post('/update','ActivityController@activityUpdate');//修改活动
                Route::post('/del','ActivityController@delActivity');//删除活动
                Route::post('/add','ActivityController@addActivity');//添加活动
            });


            Route::group(['prefix' => 'goods'],function (){
                Route::get('/list','GoodsController@goodsList');
                Route::post('/add','GoodsController@goodsAdd');
                Route::post('/del','GoodsController@goodsDel');
                Route::post('/remove','GoodsController@goodsUpdate');
            });

            Route::group(['prefix' => 'brand',],function (){
                Route::get('/list','GoodsController@brandList');
                Route::post('/del','GoodsController@brandDel');
                Route::post('/add','GoodsController@brandAdd');
                Route::post('/remove','GoodsController@brandRemove');
            });


            Route::group(['prefix' => 'cate'],function (){
                Route::get('/list','GoodsController@categoriesList');
                Route::post('/del','GoodsController@categoriesDel');
                Route::post('/remove','GoodsController@categoriesRemove');
                Route::post('/add','GoodsController@categoriesAdd');
            });

            Route::group(['prefix' => 'system'],function (){
                Route::post('/information','SystemController@informationList');
                Route::post('/information/add','SystemController@informationAdd');
                Route::post('/information/update','SystemController@informationUpdate');
                Route::post('/information/remove','SystemController@informationRemove');
                Route::post('/test','SystemController@test');
            });


        });
    });

});
