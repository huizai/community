<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace'=>'Api', 'prefix' => '{version}'], function () {
    Route::get('/login/from/wx', 'UserController@wxLogin');
    Route::any('/wechat/pay/notify/{type}', 'WechatController@notify');
    Route::options('/{all}', function(\Illuminate\Http\Request $request) {
        $origin = $request->header('ORIGIN', '*');
        header("Access-Control-Allow-Origin: $origin");
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
        header('Access-Control-Allow-Headers: Origin, Access-Control-Request-Headers, SERVER_NAME, Access-Control-Allow-Headers, cache-control, token, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie');
        http_response_code(204);
    })->where(['all' => '([a-zA-Z0-9-]|/)+']);

    Route::post('/login', 'UserController@login');
    Route::post('/login/from/mobile', 'UserController@mobileLogin');
    Route::post('/register/from/mobile', 'UserController@mobileRegister');
    Route::post('/validate/code/form/sms', 'UserController@validateCodeFromSms');
    Route::post('/forget/password', 'UserController@forgetPassword');

    Route::group(['prefix' => 'crowd', 'middleware' => ['ao']], function (){
        Route::get('/classify', 'CrowdController@getCrowdClassify');


        Route::get('/article', 'CrowdController@getCrowdArticle');
        Route::get('/member', 'CrowdController@getCrowdMember');
        Route::get('/cate', 'CateController@cate_list');
        Route::get('/cate/goods', 'CateController@crowdCateGoodsList');
        Route::get('/info/share', 'CrowdController@getCrowdInfo');

        Route::group(['middleware' => ['ApiAuth']], function () {
            Route::get('/list', 'CrowdController@getCrowd');
            Route::post('/create', 'CrowdController@createCrowd');
            Route::post('/update', 'CrowdController@updateCrowd');
            Route::post('/remove/member', 'CrowdController@removeMember');
            Route::get('/info', 'CrowdController@getCrowdInfo');
            Route::get('/info/jgid', 'CrowdController@getCrowdInfoByJgId');

        });
    });

    Route::group(['prefix' => 'topic', 'middleware' => ['ao']], function(){
        Route::get('/info', 'TopicController@getTopicInfo');
        Route::get('/comment', 'TopicController@getTopicComment');
        Route::group(['middleware' => ['ApiAuth']], function () {
            Route::post('/mytopic','TopicController@myTopic');
            Route::post('/unread','TopicController@unread');
            Route::post('/unread/list','TopicController@unreadList');
            Route::get('/list', 'TopicController@getCrowdTopic');
            Route::post('/create', 'TopicController@createTopic');
            Route::get('/news', 'TopicController@getNews');
            Route::post('/useful', 'TopicController@useful');
            Route::post('/deltopic', 'TopicController@topicDel');
            Route::post('/create/comment', 'TopicController@createComment');
            Route::post('/cancel/useful', 'TopicController@cancelUseful');
        });
    });

    Route::group(['prefix' => 'article', 'middleware' => ['ao']], function(){
        Route::get('/comment', 'ArticleController@getArticleComment');
        Route::get('/info', 'ArticleController@getArticleInfo');
        Route::get('/recommend', 'ArticleController@getRecommendArticle');
        Route::group(['middleware' => ['ApiAuth']], function () {
            Route::post('/create/comment', 'ArticleController@createComment');
            Route::post('/useful', 'ArticleController@useful');
        });
    });

    Route::group(['prefix' => 'tool', 'middleware' => ['ao']], function(){
        Route::post('/upload/img', 'ToolController@uploadImg');
        Route::get('/app/isupdate', 'ToolController@appIsUpdate');
        Route::get('/push', 'ToolController@push');
    });

    Route::group(['prefix' => 'user', 'middleware' => ['ao','ApiAuth']], function (){
        Route::get('/crowd/join/list', 'UserController@myJoinCrowd');
        Route::get('/crowd/list', 'UserController@myCrowd');
        Route::get('/topic', 'UserController@myTopic');
        Route::post('/collect/crowd', 'UserController@collectCrowd');
        Route::post('/join/crowd', 'UserController@joinCrowd');
        Route::post('/out/crowd', 'UserController@outCrowd');
        Route::post('/cert', 'UserController@userCert');
        Route::get('/my/info', 'UserController@getMyInfo');
        Route::post('/update/info', 'UserController@updateMyInfo');
        Route::post('/bind/mobile', 'UserController@bindMobile');
        Route::post('/bind/mobile/update', 'UserController@updateBindMobile');
        Route::get('/bind/wx', 'UserController@bindWx');
        Route::post('/waste', 'UserController@UserWaste');
        Route::post('logout', 'UserController@logout');
        Route::post('/withraw', 'UserController@UserWithRraw');
        Route::post('/balance', 'UserController@UserBalance');
        Route::post('/withdrawList', 'UserController@withdrawList');
        Route::post('/userList', 'UserController@userList');

        Route::post('/friend/req', 'UserController@friendRequest');
        Route::get('/friend/req-list', 'UserController@friendReqList');
        Route::post('/friend/res', 'UserController@friendRes');

        Route::post('/group/nodisturb', 'UserController@addGroupNodisturb');
        Route::post('/group/nodisturb/remove', 'UserController@removeGroupNodisturb');
    });

    Route::group(['prefix' => 'site', 'middleware' => 'ao'],function (){
        Route::post('/addres', 'SiteController@getAddres');

        Route::get('/test' , 'SiteController@test');
    });

    Route::group(['prefix' => 'goods', 'middleware' => ['ao','ApiAuth']], function (){
        Route::get('/list', 'GoodsController@crowdGoods');
        Route::get('/laser/list', 'GoodsController@jg_crowd_goods');
        Route::get('/details', 'GoodsController@goodsDetails');
        Route::post('/goods_update', 'GoodsController@goodsUpdate');
        Route::post('/goodCommentList', 'GoodsController@goodCommentList');
        Route::post('/goodRecommend', 'GoodsController@goodRecommend');
    });

    Route::group(['prefix' => 'orders', 'middleware' => ['ao','ApiAuth']], function (){
        Route::post('/myorder', 'OrderController@myOrder');
        Route::post('/create', 'OrderController@createOrder');
        Route::post('/cancel', 'OrderController@cancelOrder');
        Route::get('/info', 'OrderController@orderInfo');
        Route::post('/take', 'OrderController@orderTake');
        Route::post('/addGoodComment', 'OrderController@addGoodComment');
    });

    Route::group(['prefix' => 'address', 'middleware' => ['ao','ApiAuth']], function (){
        Route::get('/area', 'AddressController@getArea');
        Route::get('/myaddress', 'AddressController@myAddress');
        Route::post('/add', 'AddressController@myAddressInsert');
        Route::post('/save', 'AddressController@myAddressUpdate');
        Route::post('/del', 'AddressController@addressDel');
    });

    Route::group(['prefix' => 'wechat', 'middleware' => ['ao','ApiAuth']], function (){
        Route::post('/pay', 'WechatController@weChatPay');
    });

    Route::group(['prefix' => 'activity', 'middleware' => ['ao','ApiAuth']], function (){
        Route::get('/list', 'ActivityControllers@activityList');
        Route::get('/info', 'ActivityControllers@activityInfo');
        Route::post('/take', 'ActivityControllers@takeActivity');
        Route::post('/myactivity', 'ActivityControllers@myActivity');
    });

    Route::group(['prefix' => 'system', 'middleware' => ['ao','ApiAuth']], function (){
        Route::post('/information', 'SystemController@information');
        Route::post('/information/info', 'SystemController@informationInfo');
        Route::post('/test', 'SystemController@test');
    });

});
